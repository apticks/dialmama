$(document).ready(function() {
		/*Employee page Multiselect*/
        $('#example-getting-started').multiselect({
        	 buttonWidth: '565px',
        	 includeSelectAllOption: true,
        	 enableFiltering: true,
        	 buttonClass: 'btn buttons-pdf',
        	 filterPlaceholder: 'Search Role'
        });

        $('#services_multiselect').multiselect({
       	 buttonWidth: '300px',
       	 includeSelectAllOption: true,
       	 enableFiltering: true,
       	 buttonClass: 'btn buttons-pdf',
       	 filterPlaceholder: 'Search Role',
       	 maxHeight: 250
       });
        $('#brands_multiselect').multiselect({
          	 buttonWidth: '300px',
          	 includeSelectAllOption: true,
          	 enableFiltering: true,
          	 buttonClass: 'btn buttons-pdf',
          	 filterPlaceholder: 'Search Role',
          	 maxHeight: 250
          });
    });