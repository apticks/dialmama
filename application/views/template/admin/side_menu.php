<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="index.html"> <img alt="image" src="<?php echo base_url()?>assets/img/logo-dialmama.jpg" class="header-logo" /> 
                            <!--<span class="logo-name">Aegis</span>-->
						</a>
					</div>
					<div class="sidebar-user">
						<div class="sidebar-user-picture">
							<img alt="image" src="<?php echo base_url()?>assets/img/userbig.png">
						</div>
						<div class="sidebar-user-details">
							<div class="user-name"><?php echo $user->email;?></div>
							<div class="user-role"><?php echo $user->first_name.''.$user->last_name;?></div>
						</div>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Main</li>
						<li class="dropdown active"><a href="<?php echo base_url('dashboard');?>" class="nav-link "><i
									data-feather="monitor"></i><span>Dashboard</span>
							</a>
							
						</li>
						<li class="dropdown"><a href="<?php echo base_url('sellers/r');?>" class="nav-link "><i
									data-feather="user-check"></i><span>Sellers</span>
							</a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('all_users/r');?>" class="nav-link "><i
									data-feather="user-check"></i><span>All Users</span>
							</a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('meeseva_addresses/r');?>" class="nav-link "><i
									data-feather="user-check"></i><span>Mee seva centers</span>
							</a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('orders/r');?>" class="nav-link "><i
									data-feather="gift"></i><span>Orders</span>
							</a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('coupons/r');?>" class="nav-link "><i
									data-feather="gift"></i><span>Discount Coupon</span>
							</a>
						</li>
						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i
									data-feather="clipboard"></i><span>Payments</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('seller_wallets/list');?>">Seller wallets</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('wallet_transactions/list');?>">Payment request</a></li>
    				            </ul>
    						</li>
    					<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i
    									data-feather="command"></i><span>Catalogue</span></a>
    							<ul class="dropdown-menu">
    									<li><a class="nav-link" href="<?php echo base_url('ecom_product/r');?>">Products</a></li>
        								<li><a class="nav-link" href="<?php echo base_url('ecom_category/r');?>">Categories</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('ecom_sub_category/r');?>">Sub_Categories</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('ecom_brands/r');?>">Brands</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('model/r/0');?>">Models</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('excel_model/r/0');?>">Models Bulk</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('warrenty/r');?>">Warrenties</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('ram/r');?>">Rams</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('storage/r');?>">Storages</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('colour/r');?>">Colours</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('batteries/r');?>">Battery Capacities</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('size/r');?>">Sizes</a></li>
    							</ul>
    						</li>
    						
    						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i
    									data-feather="users"></i><span>Employees</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('employee/r');?>">Add Employee</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('role/r');?>">Add Role</a></li>
    				            </ul>
    						</li>
    						
    						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i
    									data-feather="database"></i><span>Maintenance</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('employee/r');?>">Db backup</a></li>
    				            </ul>
    						</li>
    						
    						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i
    									data-feather="sliders"></i><span>Settings</span></a>
    							<ul class="dropdown-menu">
                                        <li><a class="nav-link" href="<?php echo base_url('state/r');?>">States</a></li>
                                        <li><a class="nav-link" href="<?php echo base_url('district/r');?>">Districts</a></li>
        								<li><a class="nav-link" href="<?php echo base_url('settings/r');?>">Site Settings</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('sliders/r');?>">Manage Sliders</a></li>
    				            </ul>
    						</li>
					</ul>
				</aside>
			</div>