<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
 * Auth
 */
$route['category/(:any)'] = 'admin/master/category/$1';

/*
 *Admin 
 */
$route['dashboard'] = 'admin/dashboard';
$route['meeseva_addresses/(:any)'] = 'admin/dashboard/meeseva_addresses/$1';
$route['sliders/(:any)'] = 'admin/sliders/$1';
$route['advertisements/(:any)'] = 'admin/advertisements/$1';
$route['members/(:any)'] = 'admin/dashboard/members/$1';
$route['members_edit/(:any)'] = 'admin/dashboard/members_edit/$1';
$route['members_update'] = 'admin/dashboard/members_update';
$route['members_delete'] = 'admin/dashboard/members_delete';
$route['sample'] = 'admin/dashboard/sample';
$route['settings/(:any)'] = 'admin/settings/$1';
$route['profile/(:any)'] = 'admin/profile/$1';
/*Catalogue*/
$route['state/(:any)'] = 'admin/master/state/$1';
$route['district/(:any)'] = 'admin/master/district/$1';
$route['model/(:any)/(:any)'] = 'admin/master/model/$1/$2';
$route['excel_model/(:any)/(:any)'] = 'admin/master/excel_model/$1/$2';

$route['warrenty/(:any)'] = 'admin/master/warrenty/$1';
$route['ram/(:any)'] = 'admin/master/ram/$1';
$route['storage/(:any)'] = 'admin/master/storage/$1';
$route['colour/(:any)'] = 'admin/master/colour/$1';
$route['batteries/(:any)'] = 'admin/master/batteries/$1';
$route['size/(:any)'] = 'admin/master/size/$1';
/*Employees*/
$route['employee/(:any)'] = 'admin/employee/$1';
$route['role/(:any)'] = 'admin/role/$1';
$route['emp_list/(:any)'] = 'admin/emp_list/$1';
$route['sellers/(:any)'] = 'admin/sellers/$1';
$route['all_users/(:any)'] = 'admin/all_users/$1';

/*E-Commerece*/
$route['ecom_category/(:any)'] = 'ecom/ecom_category/$1';
$route['ecom_sub_category/(:any)'] = 'ecom/ecom_sub_category/$1';
$route['ecom_sub_sub_category/(:any)'] = 'ecom/ecom_sub_sub_category/$1';
$route['ecom_brands/(:any)'] = 'ecom/ecom_brands/$1';
$route['ecom_orders/(:any)'] = 'ecom/ecom_orders/$1';
$route['ecom_product/(:any)'] = 'ecom/ecom_product/$1';

/* Operations */
$route['orders/(:any)'] = 'admin/operations/orders/$1';
$route['coupons/(:any)'] = 'admin/operations/coupons/$1';

/*Payment*/
$route['wallet_transactions/(:any)'] = 'payment/wallet_transactions/$1';
$route['seller_wallets/(:any)'] = 'payment/seller_wallets/$1';
