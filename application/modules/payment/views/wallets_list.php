<style>
.list {
  display: table;
  border-spacing: 0 10px;
  padding: 0.5em 0;
}

.list > li {
  background-color: #e0e0e1;
  border-radius: 5px;
  color: #6c777f;
  display: table-row;
  width: 100%;
}
.list > li > label {
  border-bottom-left-radius: 5px;
  border-top-left-radius: 5px;
  background-color: #a1aab0;
  color: white;
  display: table-cell;
  min-width: 40%;
  padding: .5em;
  text-transform: capitalize;
}

.list > li > span {
  border-radius: 0 5px 5px 0;
  background-color: #e0e0e1;
  display: table-cell;
  padding: .5em;
}
.modal-backdrop{
	position :relative !important;
}
</style>
<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Sellers </h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>S.no</th>
									<th>Id</th>
									<th>Name</th>
									<th>Wallet</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($sellers)):?>
    							<?php $sno = 1; foreach ($sellers as $seller):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $seller['unique_id'];?></td>
    									<td><?php echo $seller['first_name'];?></td>
    									<td><?php echo $seller['wallet'];?></td>
    									<td>
    										<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal<?php echo $seller['id']?>">Pay</button>
    										<a class="btn btn-secondary buttons-pdf buttons-html5 btn-sm" href="<?php echo base_url()?>/seller_wallets/txns?id=<?php echo $seller['id']?>"><i class="fas fa-arrows-alt-h"></i></a>
    									</td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='8	'><h3><center>No Transactions</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	<?php if(!empty($sellers)):?>
	    <?php $sno = 1; foreach ($sellers as $seller):?>
	    	 <!-- The Modal -->
			  <div class="modal fade" id="myModal<?php echo $seller['id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form action="<?php echo base_url()?>seller_wallets/pay_to_vendor" method="post">
				          <div class="form-group">
				            <label for="recipient-name" class="col-form-label">Amont:</label>
				            <input type="text" name="amount" class="form-control" id="recipient-name">
				          </div>
				          <div class="form-group">
				            <label for="message-text" class="col-form-label">Transaction Id:</label>
				            <input type="text" name="txn_id" class="form-control" id="recipient-name">
				            <input type="hidden" name="user_id" class="form-control" id="recipient-name" value="<?php echo $seller['id']?>">
				          </div>
				        
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Send message</button>
				      </div>
				      </form>
				    </div>
				  </div>
			</div>
	    <?php endforeach;?>
	<?php endif;?>
