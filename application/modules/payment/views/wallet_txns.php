<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Completed Transactions</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport1"
							style="width: 100%;">
							<thead>
								<tr>
									<th>S.no</th>
									<th>Type</th>
									<th>TXN Id</th>
									<th>Amount</th>
									<th>Service Charge</th>
									<th>Final Payment</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($txns)):?>
    							<?php $sno = 1; foreach ($txns as $transaction):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $transaction['type'];?></td>
    									<td><?php echo $transaction['txn_id'];?></td>
    									<td><?php echo $transaction['cash'];?></td>
    									<td><?php echo $transaction['service_charge'];?></td>
    									<td><?php echo $transaction['cash'] - $transaction['service_charge'];?></td>
    									<td>
    										 <select  class="form-control border " disabled id="<?php echo $transaction['id']?>">
                                                <option  disabled>..Select..</option>
                                                <?php if($transaction['status'] == '0'){?>
                                                    <option value="0" selected>Pending</option>
                                                    <option value="1">Success</option>
                                                <?php }else{?>
                                                	<option value="0" >Pending</option>
                                                    <option value="1" selected>Success</option>
                                                <?php }?>
                                            </select>
    									</td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Transactions</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
	