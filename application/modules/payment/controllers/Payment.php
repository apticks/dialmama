<?php

class Payment extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        $this->load->model('wallet_transaction_model');
        $this->load->model('user_model');
        $this->load->model('setting_model');
    }
    
    public function wallet_transactions($type = 'list'){
        if($type == 'list'){
            $this->data['title'] = 'Transactions List';
            $this->data['content'] = 'payment/list';
            $this->data['transactions'] = $this->wallet_transaction_model->order_by('id', 'DESC')->with_bank('fields:user_id, name, bank_name, ac, ifsc', 'where: status = \'1\'')->where(['status'=> 0, 'type' => 'DEBIT'])->get_all();
            if(! empty($this->data['transactions'])){
                for ($i = 0; $i < count($this->data['transactions']) ; $i++){
                	$this->data['transactions'][$i]['unique_id'] = $this->user_model->order_by('id', 'DESC')->get($this->data['transactions'][$i]['user_id'])['unique_id'];
                }
            }
            $this->data['completed_transactions'] = $this->wallet_transaction_model->order_by('id', 'DESC')->with_bank('fields:user_id, name, bank_name, ac, ifsc', 'where: status = \'1\'')->where(['status >'=> 0, 'type' => 'DEBIT'])->get_all();
            if(! empty($this->data['completed_transactions'])){
                for ($i = 0; $i < count($this->data['completed_transactions']) ; $i++){
                	$this->data['completed_transactions'][$i]['unique_id'] = $this->user_model->order_by('id', 'DESC')->get($this->data['completed_transactions'][$i]['user_id'])['unique_id'];
                }
            }
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'change_status'){
        	$amount = $this->input->post('amount');
        	$percent = intval($this->setting_model->where('key','margin')->get()['value']);
        	$service_chanrge = ($percent/100)*$amount;
            echo $this->wallet_transaction_model->update([
                'id' => $this->input->post('id'),
            	'cash' => $amount,
            	'txn_id' => $this->input->post('txn_id'),
            	'service_charge' => $service_chanrge,
                'status' => 1,
            ], 'id');
        }
    }
    
    public function seller_wallets($type = 'list'){
        if($type == 'list'){
            $this->data['title'] = 'Seller wallets';
            $this->data['content'] = 'payment/wallets_list';
            $this->data['sellers'] = $this->user_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'pay_to_vendor'){
        	$amount = $this->input->post('amount');
        	$percent = intval($this->setting_model->where('key','margin')->get()['value']);
        	$service_chanrge = ($percent/100)*$amount;
        	$id = $this->wallet_transaction_model->insert([
        			'user_id' => $this->input->post('user_id'),
        			'type' => 'DEBIT',
        			'cash' => $amount,
        			'txn_id' => $this->input->post('txn_id'),
        			'service_charge' => $service_chanrge,
        			'status' => 1
        	]);
        	if($id){
        		$wallet = $this->user_model->where('id', $this->input->post('user_id'))->fields('wallet')->as_array()->get();
        		$this->user_model->update([
        				'id' => $this->input->post('user_id'),
        				'wallet' =>  floatval($wallet['wallet']) - $amount
        		], 'id');
        	}
        	redirect('seller_wallets/list', 'refresh');
        }elseif ($type == 'txns'){
        	$this->data['title'] = 'Seller wallets';
        	$this->data['content'] = 'payment/wallet_txns';
        	$this->data['txns'] = $this->wallet_transaction_model->order_by('id', 'DESC')->where('user_id', $_GET['id'])->get_all();
        	$this->_render_page($this->template, $this->data);
        }
    }
}

