<html lang="en">
<head>
<title>sri datta</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<style>
.titles h2 {
	text-align: center;
	font-size: 22px;
	font-weight: 800;
	letter-spacing: 2.6px;
}

.titles p {
	text-align: center;
}

.titles h4 {
	text-align: center;
	font-weight: 800;
	font-size: 14px;
	letter-spacing: 1.8;
}

.hed {
	margin-top: 35px;
}

.hed p {
	font-size: 11px;
	font-weight: 800;
}

.hed1 {
	margin-top: 35px;
	float: right;
}

.hed1 p {
	font-size: 11px;
	font-weight: 800;
}

.lab1 input {
	border: none;
	border-bottom: 1px solid #d8d8d8;
	margin-bottom: 14px;
}

.lab1 input:focus {
	border: none !important;
}

label {
	display: inline-block;
	max-width: 100%;
	margin-bottom: 5px;
	font-weight: 700;
	width: 32%;
}

.entry {
	margin-top: 35px;
	border-top: 1px solid #dad8d8;
}

.tit1 p {
	font-size: 17px;
	font-weight: 600;
	padding: 14px 0px;
}

.fr p {
	float: right;
	margin-top: 25px;
}

.tit3 h2 {
	text-align: center;
	font-size: 22px;
	font-weight: 800;
	letter-spacing: 2.6px;
	margin-top: 5%;
}

.tit3 p {
	text-align: center;
}

.tit3 h4 {
	text-align: center;
	font-weight: 800;
	font-size: 12px;
	letter-spacing: 0.4;
	margin-top: -6px;
}

.tit3 h5 {
	text-align: center;
	font-weight: 800;
	font-size: 13px;
}

.content12 p {
	text-align: justify;
	font-weight: 600;
}

.cont1 p {
	text-align: center;
	border: 2px solid gainsboro;
	padding: 8px;
	border-radius: 16px;
}

.palin {
	text-align: center;
	font-size: 11px;
	font-weight: 600;
	margin-top: -6px;
}

.cont1 p span {
	display: block;
	text-align: left;
}

.cont1 label {
	width: 43%;
}

.cont1 span input {
	display: inline;
	border: none;
	border-bottom: 1px solid #d4d3d3;
	width: 50%;
}
</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-12 titles">
				<h2>SRI DATTA SAI NIDHI LTD.</h2>
				<p>karishma textiles complex rajevmarg road,gadwal.</p>
				<h4>DAIL/WEEAKLY/MONTHLY</h4>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="hed">
					<p>HEAD OFFICE : GADWAL</p>
					<p>Branch Code :</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="hed1">
					<p>COUSTOMER ID : <?php echo $user_id;?></p>
					<p>DATE : <?php echo date('yyyy-mm-dd');?></p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="entry">
				<fm class="form-inline" action="/action_page.php">
				<div class="col-md-4">
					<div class="lab1">

						<label for="text">Received From :</label> <input type="text"
							class="form-control" id="text" value="<?php echo $name;?>"
							name="received form"> <label for="text"> W/O:</label> <input
							type="text" class="form-control" id="text" value="<?php echo '____________';?>"
							name="text">
					</div>
				</div>
				<div class="col-md-4">
					<div class="lab1">

						<label for="text">Adders :</label> <input type="text"
							class="form-control" id="text" value="<?php echo $address;?>"
							name="adders"> <label for="text">AADHAR :</label> <input
							type="text" class="form-control" id="text" value="<?php echo $aadhar;?>"
							name="text">
					</div>
				</div>
				<div class="col-md-4">
					<div class="lab1">

						<label for="text">DATE OF BIRTH:</label> <input type="text"
							class="form-control" id="text" value="<?php echo $dob;?>"
							name="adders"> <label for="text">NOMINEE :</label> <input
							type="text" class="form-control" id="text" value="<?php echo $nominee;?>"
							name="text">
					</div>
				</div>
				</fm>

			</div>
		</div>

		<div class="row">
			<div class="col-12 ">
				<div class="tit1">
					<p>Rupes in words :<?php echo rupees_in_words($payment);?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Date of Deposit</th>
						<th>Consideration Amount</th>
						<th>Scheme</th>
						<th>Period</th>
						<th>Rate of Interest</th>
						<th>Date of Maturity</th>
						<th>Maturity Amount Rs</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo date('yyyy-mm-dd');?></td>
						<td><?php echo $payment;?></td>
						<td>DAILY/RS(_______)</td>
						<td>3YEARS</td>
						<td>9.5%</td>
						<td>___________</td>
						<td>___________</td>
					</tr>

				</tbody>
			</table>
		</div>

		<div class="row">
			<div class="entry1">
				<fm class="form-inline" action="/action_page.php">
				<div class="col-md-4">
					<div class="lab1">

						<label for="text">SPONCER ID:</label> <input type="text"
							class="form-control" id="text" value="<?php echo (isset($ref_id))? $ref_id: '_________';?>"
							name="received form">

					</div>
				</div>
				<div class="col-md-4">
					<div class="lab1">

						<label for="text">COUSTOMER ID:</label> <input type="text"
							class="form-control" id="text" value="<?php echo $user_id;?>"
							name="adders">

					</div>
				</div>
				<div class="col-md-4"></div>
				</fm>

			</div>
		</div>
		<div class="row">
			<p class="col-md-offset-4">
				<b>For</b> SRI DATTA SAI NIDHI LTD.
			</p>
		</div>
		<br>
		<br>
		<br>
		<br>
		<div class="row">
			<span class="pull-left"><b>Branch Officer</b></span> <span
				class="pull-right"><b>CEO</b> </span>
		</div>
		<!--2nd page-->
		<div class="row">
			<div class="tit3">
				<h2>SRI DATTA SAI NIDHI LTD.</h2>
				<p>karishma textiles complex rajevmarg road,gadwal</p>
				<h4>Dear Friends !</h4>
				<h4>I am happy to see you amoung the partners of the � NIDHI�
					PRORRAM</h4>
				<h5>SHARE CERTIFICATE</h5>
			</div>
		</div>
		<div class="content12">
			<p>[Pursuant to sub-section (3) of section 46 of the companies
				Act,2013 and rule 5(2) of the Companies (Share Capital and
				Debentures) Rules 2014 This is to certify that the person (s) named
				this Certificate is/are the Registered Holder (s) of The
				within-mentioned Share (s) bearing the distinctive number (s) here
				in specified in the above Company subject to the Memorandum and
				Articles of Association of the Company and that the Amount endorsed
				hereon has been paid up on each such share.</p>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="cont1">
					<p>EQUITY SHARES EACH OF RUPEES:10.00/- (TEN RUPEES ONLY) AMOUNT
						PAIK-UP PER SHARE RUPEES : 10.00/- (TEN RUPEES ONLY)</p>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="cont1">
					<p>
						<span><label for="text">COUSTOMER ID:</label> <input type="text"
							class="form-control" id="text" value="<?php echo '______________';?>"
							name="received form"></span> <span><label for="text">NAME OF THE
								SHAREHOLDER:</label> <input type="text" class="form-control"
							id="text" value="<?php echo '______________';?>" name="received form"></span> <span><label
							for="text">NO OF SHARES:</label> <input type="text"
							class="form-control" id="text" value="<?php echo '______________';?>"
							name="received form"></span>
					</p>
				</div>
				<p class="palin">Given under the Common Seal of the Company</p>
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="hed">
					<p>NOTE: Minimum 50 share</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="hed1">
					<p>CEO</p>
				</div>
			</div>
		</div>

</body>
</html>
