<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of E-Commerce Orders</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Order No</th>
									<th>Address</th>
									<th>Payment_method_id</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($ecom_categories)):?>
    							<?php  $sno = 1; foreach ($ecom_categories as $ecom_category): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $ecom_category['name'];?></td>
									<td><?php echo $ecom_category['desc'];?></td>
									<td><img
										src="<?php echo base_url();?>uploads/ecom_category_image/ecom_category_<?php echo $ecom_category['id'];?>.jpg"
										width="50px"></td>
									<td><a
										href="<?php echo base_url()?>ecom_category/edit?id=<?php echo $ecom_category['id']; ?>"
										class=" mr-2  " type="ecom_category"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $ecom_category['id'] ?>, 'ecom_category')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>No Orders</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>