<?php if($type == 'ecom_category'){?>


    <!--Edit Category -->
    <div class="row">
        <div class="col-12">
            <h4>Edit Category</h4>
            <form class="needs-validation" novalidate=""  action="<?php echo base_url('ecom_category/u');?>" method="post" enctype="multipart/form-data">
           <div class="card-header">
           <div class="form-row">
            <div class="form-group col-md-6">
                            <label>Category Name</label>
                            <input type="text" name="name" class="form-control" required="" value="<?php echo $category['name'];?>">
                            <div class="invalid-feedback">Enter Valid Category Name?</div>
                        </div>
                         <input type="hidden" name="id" value="<?php echo $category['id'] ; ?>">
                         <div class="form-group col-md-4">
                                <label>Brands</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <select class="form-control" id="services_multiselect" name="brand_id[]" required="" multiple>
                                    
                                    <?php foreach ($brands as $brand): if($brand['status'] == 1):?>
                                    	<option value="<?php echo $brand['id'];?>" <?php echo (isset($ecom_categories['brands']) && array_search($brand['id'], array_column($ecom_categories['brands'], 'id')) !== FALSE)? 'selected': '';?>><?php echo $brand['name']?></option>
                                     <?php endif;endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Manufacturer Brands</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <select class="form-control" id="brands_multiselect" name="mf_brand_id[]" required="" multiple>
                                    
                                    <?php foreach ($brands as $brand): if($brand['status'] == 2):?>
                                    	<option value="<?php echo $brand['id'];?>" <?php echo (isset($ecom_categories['brands']) && array_search($brand['id'], array_column($ecom_categories['brands'], 'id')) !== FALSE)? 'selected': '';?>><?php echo $brand['name']?></option>
                                     <?php endif;endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                            </div>
<!--                         <div class="form-group col-md-6"> -->
<!--                             <label>Upload Image</label> 
                            <input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_category_image/ecom_category_<?php echo $category['id']; ?>.jpg">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_category_image/ecom_category_<?php echo $category['id']; ?>.jpg"  style="width: 200px;"/>

<!--                             <img id="blah" src="#" alt="" /> -->

<!--                             <div class="invalid-feedback">Upload Image?</div> -->
<!--                         </div> -->
                        <div class="col-8">
                       <input type='file' name="file" onchange="readURL(this);" id="upload_form"/>
                       <?php echo form_error('file', '<div style="color:red">', '</div>');?>
                          <img id="blah" src="<?php echo base_url(); ?>uploads/ecom_category_image/ecom_category_<?php echo $category['id']; ?>.jpg"" width="180" height="180" alt="your image" />
                      </div>
                         <div class="form-group col-md-12">
                         <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-primary mt-27 ">Update</button> 
<!--                              <button class="btn btn-primary mt-27 ">Update</button> -->
                           
                        </div>
           
            </div>
            </div>
            </form>

        </div>
    </div>
    <?php }elseif ($type == 'ecom_sub_category'){?>
      <!--sub_category Edit-->
        <div class="row">
            <div class="col-12">
                <h4>Edit sub category</h4>
                <form class="needs-validation" novalidate="" action="<?php echo base_url('ecom_sub_category/u');?>" method="post" enctype="multipart/form-data">
                    <div class="card-header">

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>sub_categories</label>
                                <input type="text" class="form-control" name="name" required="" value="<?php echo $sub_categories['name'];?>">
                                <div class="invalid-feedback">Enter valid  Name?</div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $sub_categories['id'] ; ?>">
                            </br>
                            <div class="form-group col-md-4">
                                <label>Category</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <select class="form-control" name="cat_id" required="">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $sub_categories['cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                            </div>
							
                            <div class="form-group col-md-6">
                                <label>Upload Image</label>
                                <input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_sub_category_image/ecom_sub_category_<?php echo $sub_categories['id']; ?>.jpg">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_sub_category_image/ecom_sub_category_<?php echo $sub_categories['id']; ?>.jpg" style="width: 200px;" />
                                
<!--                                 <input type="file" class="form-control" name="file"> -->
                                <div class="invalid-feedback">Upload Image?</div>
                            </div>

<!--                             <div class="form-group col-md-6"> -->
<!--                                 <img src="" width="80px"> -->
<!--                             </div> -->

                            <div class="form-group col-md-12">

                                <button class="btn btn-primary mt-27 ">Update</button>
                            </div>

                        </div>

                    </div>
                </form>

            </div>
        </div>
    <?php }elseif ($type == 'ecom_brands'){?>
        <!--Edit Category -->
    <div class="row">
        <div class="col-12">
            <h4>Edit Brands</h4>
            <form class="needs-validation" novalidate=""  action="<?php echo base_url('ecom_brands/u');?>" method="post" enctype="multipart/form-data">
           <div class="card-header">
           <div class="form-row">
            <div class="form-group col-md-6">
                            <label>Brand Name</label>
                            <input type="text" name="name" class="form-control" required="" value="<?php echo $ecom_brands['name']?>" >
                            <div class="invalid-feedback">Enter Valid Brand Name?</div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $ecom_brands['id'] ; ?>">
                         
                         <div class="form-group mb-0 col-md-6">
                            <label>Description</label>
                            <input type="text" name="desc" class="form-control" required="" value="<?php echo $ecom_brands['desc']?>">
                            <div class="invalid-feedback">Give some Description</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Upload Image</label>
                            <input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_brands_image/ecom_brands_<?php echo $ecom_brands['id']; ?>.jpg" style="width: 200px;">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_brands_image/ecom_brands_<?php echo $ecom_brands['id']; ?>.jpg" style="width: 200px;" />
                             
                            <img id="blah" src="#" alt="" />

                            <div class="invalid-feedback">Upload Image?</div>
                        </div>
                         <div class="form-group col-md-6">
                            <label>Brand Type</label>
                             <select class="form-control" name="status" required="">
                                   
                                   
                                      <option value="<?php echo $ecom_brands['status'];?>" selected disabled><?php echo (($ecom_brands['status']==1))? 'Normal Brand' : 'Manufacturer Brand' ;?></option>
                                       <option value="1" >Normal Brand</option>
                                       <option value="2" >Manufacturer Brand</option>
                                      
                                </select>

                            <div class="invalid-feedback">Upload Image?</div>
                        </div>
                         <div class="form-group col-md-12">
                         <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-primary mt-27 ">Update</button> 
<!--                              <button class="btn btn-primary mt-27 ">Update</button> -->
                           
                        </div>
           
            </div>
            </div>
            </form>

        </div>
    </div>
     <?php }elseif ($type == 'product'){?>
     <div class="row">
	<div class="col-12">
		<h4>Product Page</h4>
		<form class="needs-validation" novalidate="" enctype="multipart/form-data"
			action="<?php echo base_url('product/u'); ?>" method="post" >
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Product Name</label> <input type="text" name="name"
							class="form-control" placeholder="Product Name" required="" value="<?php echo $product['name'];?>">
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
 <input type="hidden" name="id" value="<?php echo $product['id'] ; ?>"><div class="form-group col-md-6">
						<label>Upload Image</label><input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_product_image/ecom_product_<?php echo $product['id']; ?>.jpg">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_product_image/ecom_product_<?php echo $product['id']; ?>.jpg"  style="width: 200px;"/>

                             <img id="blah" src="#" alt=""  style="width: 200px;"/>
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
                    
					<div class="form-group col-md-6">
                      <label class="control-label" for="product-category">Product Category</label>
                      <div class="">
                        <select class="form-control" name="cat_id" required="" id='category' onchange="category_changed()">
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_categories as $category):?>
    								<option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $product['category']['id'])? 'selected' : '';?>><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="col-md-6 control-label" id="product_desc" for="textarea">Product Details</label>
                     
                      <?php echo form_error('desc','<div style="color:red">','</div>')?>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="control-label" for="product-sub-category">Product Sub Category</label>
                      <div class="">

                          <select class="form-control" name="sub_cat_id" id="sub_cat_id" onchange="sub_category_changed()">
								
                                 <option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_sub_categories as $subcat):?>
    								<option value="<?php echo $subcat['id'];?>" <?php echo ($subcat['id'] == $product['sub_category']['id'])? 'selected' : '';?>><?php echo $subcat['name']?></option>
    							<?php endforeach;?>  

                        </select>
						
                      </div>
                    </div>
   
                    
                    
					<div class="form-group col-md-6">
                      <label class="control-label" for="product-brand">Product Brand</label>
                      <div class="">
                         <select class="form-control" name="brand_id"  id="brand_id" onchange="sub_category_changed()"> 
							<option value="0" selected disabled>--select--</option> 
    							<?php foreach ($ecom_brands as $brand):?>
    								<option value="<?php echo $brand['id'];?>" <?php echo ($brand['id'] == $product['brand']['id'])? 'selected' : '';?>><?php echo $brand['name']?></option>
    							<?php endforeach;?>
				</select> 
                      </div>
                    </div>
					
                    <!-- Text input-->
                    <div class="form-group col-md-2">
                      <label class="control-label" for="mrp-price">Qty</label>  
                      <div class="">
                      <input id="mrp-price" name="qty" type="text" placeholder="Quantity" class="form-control input-md" value="<?php echo $product['qty'];?>">
                   
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group col-md-2">
                      <label class="control-label" for="mrp-price">Units</label>  
                      <div class="">
                      <input id="mrp-price" name="units" type="text" placeholder="Units" class="form-control input-md" value="<?php echo $product['units'];?>">
                   
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="col-md-6 control-label" for="marp-price">MRP Price</label>  
                      <div class="">
                      <input id="marp-price" name="mrp" type="text" placeholder="Mrp Price" class="form-control input-md"  value="<?php echo $product['mrp'];?>">
                    
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="col-md-6 control-label" for="offered-price">Offer Price</label>  
                      <div class="">
                      <input id="offered-price" name="offer_price" type="text" placeholder="Offer Price" class="form-control input-md"  value="<?php echo $product['offer_price'];?>">
                    
                      </div>
                    </div>
                    
                    
                    <div class="form-group col-md-6 ">
                      <label class="control-label" for="product-gst-tax">Product GST Tax</label>  
                      <div class="">
                      <input id="product-gst-tax" name="gst" type="text" placeholder="0" class="form-control input-md"  value="<?php echo $product['gst'];?>">
                      </div>
                    </div>
                    
					<div class="form-group col-md-12">
                        <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-success mt-27">Update</button>

        
					</div>
				</div>


			</div>
		</form>
    </div>
</div>
 <?php }elseif ($type == 'ecom_sub_sub_category'){?>

     <div class="row">
	<div class="col-12">
		<h4>Add E-Commerce Sub Sub_Category</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('ecom_sub_sub_category/u');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Sub Sub_Category Name</label> <input type="text"
							class="form-control" name="name" required="" value="<?php echo $ecom_sub_sub_categories['name'];?>">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					 <input type="hidden" name="id" value="<?php echo $ecom_sub_sub_categories['id'] ; ?>">
					<div class="form-group col-md-2">
						<label>Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="cat_id" required="" id="category" onchange="category_changedsub()">
								 <option value="0" selected disabled>--select--</option>

                                                    <?php foreach ($categories as $cat):?>
                                                        <option value="<?php echo $cat['id'];?>" <?php echo ($cat['id'] == $ecom_sub_sub_categories['cat_id'])? 'selected': '';?>><?php echo $cat['name']?></option>
                                                        <?php echo $cat['name']?>
                                                        </option>
                                                    <?php endforeach;?>
                            
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
 	<div class="form-group col-md-2">
						<label>Sub Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="sub_cat_id" required="" id="subcat">
								<option value="0" selected disabled>--select--</option>
    							 <?php foreach ($sub_categories as $cat):?>
                                                        <option value="<?php echo $cat['id'];?>" <?php echo ($cat['id'] == $ecom_sub_sub_categories['sub_cat_id'])? 'selected': '';?>><?php echo $cat['name']?></option>
                                                        <?php echo $cat['name']?>
                                                        </option>
                                                    <?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('sub_cat_id','<div style="color:red>"','</div>');?>
					</div>

					<div class="form-group mb-0 col-md-4">
						<label>Description</label> <textarea type="text" class="form-control"
							name="desc" id="product_desc" data-sample-short><?php echo $ecom_sub_sub_categories['desc'];?></textarea>
						<div class="invalid-feedback">Give some Description</div>
						
					</div>
						
					<div class="form-group col-md-2">
						<label>Upload Image</label> 
						<input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_sub_sub_category_image/ecom_sub_sub_category_<?php echo $ecom_sub_sub_categories['id']; ?>.jpg">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_sub_sub_category_image/ecom_sub_sub_category_<?php echo $ecom_sub_sub_categories['id']; ?>.jpg"  style="width: 200px;"/>

                             <img id="blah" src="#" alt="" style="width: 200px;"/>
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>

					<div class="form-group col-md-12">
                        <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-success mt-27">Update</button>
					</div>


				</div>


			</div>
		</form>
     
  <?php } elseif ($type == 'ecom_product'){?>
  <style>
  th, td {
  padding: 15px;
}
  </style>
  <div class="container">
  	<h1><?php echo $ecom_product['name'];?></h1><hr/>
  		<div class="row justify-content-center">
  			<div class="col-sm-4">
  				<img alt="Product image" src="<?php echo base_url()?>uploads/product_image/product_<?php echo $ecom_product['product_code']?>.jpg" class="rounded" width="350" height="350">
  			</div>
  		</div><hr/>
  		<div class="row">
  			<div class="col-sm-6">
  				<table class="product_details">
  					<thead>
  						<tr>
  							<th colspan="2"><h4>Product Details</h4></th>
  						</tr>
  					</thead>
  					<tbody>
  						<tr>
  							<td>Id</td>
  							<th><?php echo $ecom_product['product_code'];?></th>
  						</tr>
  						<tr>
  							<td>Category</td>
  							<th><?php echo $ecom_product['category']['name'];?></th>
  						</tr>
  						<tr>
  							<td>Sub Category</td>
  							<th><?php echo $ecom_product['sub_category']['name'];?></th>
  						</tr>
  						<tr>
  							<td>Brand</td>
  							<th><?php echo $ecom_product['brand']['name'];?></th>
  						</tr>
  						<tr>
  							<td>Model</td>
  							<th><?php echo $ecom_product['model']['name'];?></th>
  						</tr>
  					</tbody>
  				</table>
  				
  				<table class="seller_details">
  					<thead>
  						<tr>
  							<th colspan="2"><h4>Seller Details</h4></th>
  						</tr>
  					</thead>
  					<tbody>
  						<tr>
  							<td>Seller Id</td>
  							<th><?php echo $ecom_product['user']['unique_id'];?></th>
  						</tr>
  						<tr>
  							<td>Name</td>
  							<th><?php echo $ecom_product['user']['first_name'];?></th>
  						</tr>
  						<tr>
  							<td>Mobile No</td>
  							<th><?php echo $ecom_product['user']['phone'];?></th>
  						</tr>
  						<tr>
  							<td>Email id</td>
  							<th><?php echo $ecom_product['user']['email'];?></th>
  						</tr>
  					</tbody>
  				</table>
  			</div>
  			<div class="col-sm-6">
  				<table class="product_specifications">
  					<thead>
  						<tr>
  							<th colspan="2"><h4>Product Specifications</h4></th>
  						</tr>
  					</thead>
  					<tbody>
  						<tr>
  							<td>Warranty</td>
  							<th><?php echo $ecom_product['warrenty']['time'];?></th>
  						</tr>
  						<tr>
  							<td>Ram</td>
  							<th><?php echo $ecom_product['ram']['size'];?></th>
  						</tr>
  						<tr>
  							<td>Storage</td>
  							<th><?php echo $ecom_product['storage']['size'];?></th>
  						</tr>
  						<tr>
  							<td>Camera (Yes/No)</td>
  							<th><?php echo $ecom_product['is_having_camera'];?></th>
  						</tr>
  						<tr>
  							<td>Features</td>
  							<th><?php echo $ecom_product['mixed_features'];?></th>
  						</tr>
  						<tr>
  							<td>Mixed Compatibility</td>
  							<th><?php echo $ecom_product['mixed_compatibility'];?></th>
  						</tr>
  						<tr>
  							<td>Extra Features</td>
  							<th><?php echo $ecom_product['mixed_extra_features'];?></th>
  						</tr>
  						<tr>
  							<td>Front Camera(in px)</td>
  							<th><?php echo $ecom_product['front_cam'];?></th>
  						</tr>
  						<tr>
  							<td>Back Camera(in px)</td>
  							<th><?php echo $ecom_product['back_cam'];?></th>
  						</tr>
  						<tr>
  							<td>Network</td>
  							<th><?php echo $ecom_product['network'];?></th>
  						</tr>
  						<tr>
  							<td>Condition</td>
  							<th><?php echo $ecom_product['condition'];?></th>
  						</tr>
  						<tr>
  							<td>Touch Screen (Yes/No)</td>
  							<th><?php echo $ecom_product['touch_screen'];?></th>
  						</tr>
  						<tr>
  							<td>Resolution</td>
  							<th><?php echo $ecom_product['resolution'];?></th>
  						</tr>
  						<tr>
  							<td>HDMI Ports</td>
  							<th><?php echo $ecom_product['hdmi'];?></th>
  						</tr>
  						<tr>
  							<td>USB Ports</td>
  							<th><?php echo $ecom_product['usb'];?></th>
  						</tr>
  						<tr>
  							<td>Style</td>
  							<th><?php echo $ecom_product['style'];?></th>
  						</tr>
  						
  						<tr>
  							<td>Bluetooth(in px)</td>
  							<th><?php echo $ecom_product['bluetooth'];?></th>
  						</tr>
  						<tr>
  							<td>Android (Yes/No)</td>
  							<th><?php echo $ecom_product['android'];?></th>
  						</tr>
  						<tr>
  							<td>Compatible for</td>
  							<th><?php echo $ecom_product['compatible_for'];?></th>
  						</tr>
  						<tr>
  							<td>Touch Screen (Yes/No)</td>
  							<th><?php echo $ecom_product['touch_screen'];?></th>
  						</tr>
  						<tr>
  							<td>Resolution</td>
  							<th><?php echo $ecom_product['resolution'];?></th>
  						</tr>
  						<tr>
  							<td>HDMI Ports</td>
  							<th><?php echo $ecom_product['hdmi'];?></th>
  						</tr>
  						<tr>
  							<td>USB Ports</td>
  							<th><?php echo $ecom_product['usb'];?></th>
  						</tr>
  						<tr>
  							<td>Style</td>
  							<th><?php echo $ecom_product['style'];?></th>
  						</tr>
  						<tr>
  							<td>Back Camera(in px)</td>
  							<th><?php echo $ecom_product['back_cam'];?></th>
  						</tr>
  						<tr>
  							<td>Network</td>
  							<th><?php echo $ecom_product['network'];?></th>
  						</tr>
  						<tr>
  							<td>Condition</td>
  							<th><?php echo $ecom_product['condition'];?></th>
  						</tr>
  						<tr>
  							<td>Touch Screen (Yes/No)</td>
  							<th><?php echo $ecom_product['touch_screen'];?></th>
  						</tr>
  						<tr>
  							<td>Resolution</td>
  							<th><?php echo $ecom_product['resolution'];?></th>
  						</tr>
  						<tr>
  							<td>HDMI Ports</td>
  							<th><?php echo $ecom_product['hdmi'];?></th>
  						</tr>
  						<tr>
  							<td>USB Ports</td>
  							<th><?php echo $ecom_product['usb'];?></th>
  						</tr>
  						<tr>
  							<td>Style</td>
  							<th><?php echo $ecom_product['style'];?></th>
  						</tr>
  						<tr>
  							<td>Details</td>
  							<th><?php echo $ecom_product['desc'];?></th>
  						</tr>
  					</tbody>
  				</table>
  			</div>
  		</div>
  		<div calss="row">
  			<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="col-10">List of products</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>SKU</th>
									<th>Model</th>
									<th>Color</th>
									<th>Size</th>
									<th>Price</th>
									<th>Quantity</th>
									<th>MOQ</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($ecom_product['variants'])):?>
    							<?php  $sno = 1; for ($i = 0; $i < count($ecom_product['variants']); $i++){ ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $ecom_product['variants'][$i]['sku'];?></td>
									<td><?php echo $models[array_search($ecom_product['variant_values'][$i]['model'], array_column($models, 'id'))]['name'] ;?></td>
									<td><?php echo $colors[array_search($ecom_product['variant_values'][$i]['color'], array_column($colors, 'id'))]['name'] ;?></td>
									<td><?php echo $sizes[array_search($ecom_product['variant_values'][$i]['size'], array_column($sizes, 'id'))]['size'] ;?></td>
									<td><?php echo $ecom_product['variants'][$i]['price'];?></td>
									<td><?php echo $ecom_product['variants'][$i]['qty'];?></td>
									<td><?php echo $ecom_product['variants'][$i]['moq'];?></td>
								</tr>
    							<?php }?>
							<?php else :?>
							<tr>
									<th colspan='12'><h3>
											<center>Products Not Available</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>	
  		</div>
  	</div>
  <?php }?>
  
                        
                    