<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="col-10">List of products</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Product code</th>
									<th>Name</th>
									<th>Variants</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($ecom_product)):?>
    							<?php  $sno = 1; foreach ($ecom_product as $product): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $product['product_code'];?></td>
									<td><?php echo $product['name'];?></td>
									<td><?php if(! empty($product['variants'])){foreach ($product['variants'] as $variants):?>
										<?php echo $variants['sku'].', ';?>
									<?php endforeach;}?></td>
									<td><a
										href="<?php echo base_url()?>ecom_product/edit?id=<?php echo $product['id']; ?>"
										class=" mr-2  " type="product"> <i class="fas fa-eye"></i>
									</a> 
									<a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $product['id'] ?>, 'ecom_product/d')">
 											<i class="far fa-trash-alt"></i> 
 									</a>
									</td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='12'><h3>
											<center>Products Not Available</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>