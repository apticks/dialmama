<?php
class Ecom extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');
            $this->load->model('category_model');
            $this->load->model('sub_category_model');
            $this->load->model('brand_model');
            $this->load->model('product_model');
            $this->load->model('product_variant_model');
            $this->load->model('size_model');
            $this->load->model('colour_model');
            $this->load->model('model_model');
    }
    
    /**
     * @desc To manage product
     * @author Mehar
     *
     * @param string $type
     */
    public function ecom_product($type = 'r'){
        if ($type == 'r'){
            $this->data['ecom_product'] = $this->product_model
            ->fields('id, product_code, name, desc, model, network, condition, touch_screen, resolution, hdmi, usb, style, 	front_cam, back_cam, bluetooth, android')
            ->order_by('id', 'DESC')
            ->with_user('fields: id, unique_id')
            ->with_category('fields: id, name')
            ->with_sub_category('fields:id, name')
            ->with_brand('fields:id, name')
            ->with_model('fields:id, brand_id, name')
            ->with_warrenty('fields:id, time')
            ->with_ram('fields:id, size')
            ->with_storage('fields:id, size')
            ->with_battery_capacity('fields:id, capacity')
            ->with_screen_size('fields:id, size')
            ->with_variants('fields:id, sku, qty, price, moq')
            ->with_variant_values('fields:variant_id, model, color, size')
            ->with_compatible_for('fields: id, name')
            ->get_all();
            if(! empty($this->data['ecom_product'])){
                for ($i = 0; $i < count($this->data['ecom_product']) ; $i++){
                    $this->data['ecom_product'][$i]['image'] = base_url().'uploads/product_image/product_'.$this->data['ecom_product'][$i]['product_code'].'.jpg';
                }
            }
            $this->data['title'] = 'E-Commerce Products';
            $this->data['content'] = 'ecom/ecom/product';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'edit'){
        	$this->data['ecom_product'] = $this->product_model
        	->fields('id, product_code, name, desc, is_having_camera, mixed_features, mixed_compatibility, mixed_extra_features, front_cam, back_cam, model, network, condition, touch_screen, resolution, hdmi, usb, style, bluetooth, android')
        	->order_by('id', 'DESC')
        	->with_user('fields: id, unique_id, first_name, phone, email')
        	->with_category('fields: id, name')
        	->with_sub_category('fields:id, name')
        	->with_brand('fields:id, name')
        	->with_model('fields:id, brand_id, name')
        	->with_warrenty('fields:id, time')
        	->with_ram('fields:id, size')
        	->with_storage('fields:id, size')
        	->with_battery_capacity('fields:id, capacity')
        	->with_screen_size('fields:id, size')
        	->with_variants('fields:id, sku, qty, price, moq')
        	->with_variant_values('fields:variant_id, model, color, size')
        	->with_compatible_for('fields: id, name')
        	->get($_GET['id']);
        	if(! empty($this->data['ecom_product'])){
        			$this->data['ecom_product']['image'] = base_url().'uploads/product_image/product_'.$this->data['ecom_product']['product_code'].'.jpg';
        	}
        	$this->data['models'] = $this->model_model->get_all();
        	$this->data['sizes'] = $this->size_model->get_all();
        	$this->data['colors'] = $this->colour_model->get_all();
        	$this->data['title'] = 'E-Commerce Products';
        	$this->data['content'] = 'ecom/ecom/edit';
        	$this->data['type'] = 'ecom_product';
        	$this->_render_page($this->template, $this->data);
        }elseif ($type == 'd'){
        	echo $this->product_model->delete(['id' => $this->input->post('id')]);
        }
        
    }
   
    /**
     * @author Trupti
     * @desc To Manage Ecommerce Categories
     * @param string $type
     */
    public function ecom_category($type = 'r'){
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                $this->form_validation->set_rules($this->category_model->rules);
                /* if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecommerce Category Image', 'required');
                } */
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_category('r');
                } else {
                    $id = $this->category_model->insert([
                        'name' => $this->input->post('name'),
                    ]);
                    if(! empty($this->input->post('brand_id'))){foreach ($this->input->post('brand_id') as $sid){
                    	$this->db->insert('categories_brands', ['cat_id' => $id, 'brand_id' => $sid, 'status' => 1]);
                    }}
                    if(! empty($this->input->post('mf_brand_id'))){foreach ($this->input->post('mf_brand_id') as $sid){
                        $this->db->insert('categories_brands', ['cat_id' => $id, 'brand_id' => $sid, 'status' => 2]);
                    }}
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "ecom_category", $id, '', 'no');
                    redirect('ecom_category/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'E-Commerce Category';
                $this->data['content'] = 'ecom/ecom/category';
                $this->data['ecom_categories_brands'] = $this->category_model->with_brands('fields:id, status, name,desc')->get_all();
                $this->data['brands'] = $this->brand_model->order_by('id', 'DESC')->get_all();
              	$this->data['ecom_categories'] = $this->category_model->order_by('id', 'ASCE')->get_all();
                $this->_render_page($this->template, $this->data);
            }  elseif ($type == 'u') {
                $this->form_validation->set_rules($this->category_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->category_model->update([
                        'id' => $this->input->post('id'),
                        'name' => $this->input->post('name'),
                    ], 'id');
                    if(! empty($this->input->post('brand_id'))){
                        $this->db->delete('categories_brands', array('cat_id' => $this->input->post('id'), 'status' => 1));
                        foreach ($this->input->post('brand_id') as $sid){
                            $this->db->insert('categories_brands', ['cat_id' => $this->input->post('id'), 'brand_id' => $sid, 'status' => 1]);
                    }}
                    
                    if(! empty($this->input->post('mf_brand_id'))){
                        $this->db->delete('categories_brands', array('cat_id' => $this->input->post('id'), 'status' => 2));
                        foreach ($this->input->post('mf_brand_id') as $sid){
                            $this->db->insert('categories_brands', ['cat_id' => $this->input->post('id'), 'brand_id' => $sid, 'status' => 2]);
                    }}
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "ecom_category", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_category/r', 'refresh');
                }
            }elseif ($type == 'd') {
                echo $this->category_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-Commerce Category';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_category';
                $this->data['category'] = $this->category_model->where('id',$this->input->get('id'))->get();
                $this->data['i'] = $this->category_model->where('file',$this->input->get('file'))->get();
                $this->data['ecom_categories'] = $this->category_model->order_by('id', 'DESC')->with_brands('fields:id, name')->where('id', $this->input->get('id'))->get();
                $this->data['brands'] = $this->brand_model->order_by('id', 'DESC')->get_all();
                $this->_render_page($this->template, $this->data);
            }
    }
    
    /**
     * E-Commerce Sub_Category crud
     *
     * @author Trupti
     * @desc To Manage Ecommerce Sub Categories
     * @param string $type
     */
    public function ecom_sub_category($type = 'r')
    {
        
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                
                $this->form_validation->set_rules($this->sub_category_model->rules);
                
                /* if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecom sub_category Image', 'required');
                } */
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_sub_category('r');
                } else {
                    $id = $this->sub_category_model->insert([
                        'cat_id' => $this->input->post('cat_id'),
                        'name' => $this->input->post('name'),
                    ]);
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "ecom_sub_category", $id, '', 'no');
                    redirect('ecom_sub_category/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'Ecommerce Sub_Category';
                $this->data['content'] = 'ecom/ecom/subcategory';
                $this->data['ecom_categories'] = $this->category_model->get_all();
                $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
                //print_r($this->data['brands']);exit();
                $this->_render_page($this->template, $this->data);
                //echo json_encode($this->data);
            } elseif ($type == 'u') {
                $this->form_validation->set_rules($this->sub_category_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->sub_category_model->update([
                        'cat_id' => $this->input->post('cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ], $this->input->post('id'));
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "ecom_sub_category", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_sub_category/r', 'refresh');
                }
            } elseif ($type == 'd') {
                $this->sub_category_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-commerce sub_category';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_sub_category';
                $this->data['sub_categories']=$this->sub_category_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
                $this->data['categories'] = $this->category_model->order_by('id', 'DESC')->get_all();
                $this->_render_page($this->template, $this->data);
            }elseif($type == 'list'){
                $data = $this->sub_category_model->fields('id, name, desc')->where(['cat_id' =>$this->input->post('cat_id')])->get_all();
                echo json_encode($data);
            }
    }
    /**
     * E-Commerce Sub Sub_Category crud
     *
     * @author Trupti
     * @desc To Manage Ecommerce Sub Categories
     * @param string $type
     */
    public function ecom_sub_sub_category($type = 'r')
    {
        
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                
                $this->form_validation->set_rules($this->ecom_sub_sub_category_model->rules);
                
                if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecom sub_sub_category Image', 'required');
                }
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_sub_category('r');
                } else {
                    $id = $this->ecom_sub_sub_category_model->insert([
                        'cat_id' => $this->input->post('cat_id'),
                        'sub_cat_id' => $this->input->post('sub_cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ]);
                  
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "ecom_sub_sub_category", $id, '', 'no');
                    redirect('ecom_sub_sub_category/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'Ecommerce Sub Sub_Category';
                $this->data['content'] = 'ecom/ecom/sub_subcategory';
                $this->data['ecom_categories'] = $this->category_model->get_all();
                $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
                $this->data['ecom_sub_sub_categories'] = $this->ecom_sub_sub_category_model->get_all();
                $this->_render_page($this->template, $this->data);
                //echo json_encode($this->data);
            } elseif ($type == 'u') {
                $this->form_validation->set_rules($this->ecom_sub_sub_category_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->ecom_sub_sub_category_model->update([
                        'cat_id' => $this->input->post('cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ], $this->input->post('id'));
                   /*  foreach ($this->input->post('brand_id') as $sid){
                        if(! $this->db->get_where('ecom_subcategories_brands', ['sub_cat_id' => $this->input->post('id'), 'brand_id' => $sid])->row())
                            $this->db->insert('ecom_subcategories_brands', ['sub_cat_id' => $this->input->post('id'), 'brand_id' => $sid]);
                            else
                                $this->db->update('ecom_subcategories_brands', ['sub_cat_id' => $this->input->post('id'), 'brand_id' => $sid]);
                    } */
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "ecom_sub_sub_category", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_sub_sub_category/r', 'refresh');
                }
            } elseif ($type == 'd') {
                $this->ecom_sub_sub_category_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-commerce sub sub_category';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_sub_sub_category';
                $this->data['sub_categories']=$this->sub_category_model->order_by('id', 'DESC')->get_all();
                $this->data['categories'] = $this->category_model->order_by('id', 'DESC')->get_all();
                $this->data['ecom_sub_sub_categories'] = $this->ecom_sub_sub_category_model->with_category('fields: id, name')->with_sub_category('fields: id, name')->where('id',$this->input->get('id'))->get();
                $this->_render_page($this->template, $this->data);
            }elseif($type == 'list'){
                $data = $this->sub_category_model->fields('id, name, desc')->where(['cat_id' =>$this->input->post('cat_id')])->get_all();
                echo json_encode($data);
            }
    }
    
    
    /**
     * E-Commerce brand crud
     *
     * @author Trupti
     * @desc To Manage Ecommerce Sub Categories
     * @param string $type
     */
    public function ecom_brands($type = 'r'){
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                $this->form_validation->set_rules($this->brand_model->rules);
                if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecommerce Brands Image', 'required');
                }
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_brands('r');
                } else {
                    $id = $this->brand_model->insert([
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'status' => $this->input->post('status')
                    ]);
                    
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "ecom_brands", $id, '', 'no');
                    redirect('ecom_brands/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'E-Commerece Brands';
                $this->data['content'] = 'ecom/ecom/brands';
                $this->data['ecom_brands'] = $this->brand_model->order_by('name', 'ASC')->get_all();
                $this->_render_page($this->template, $this->data);
            }  elseif ($type == 'u') {
                $this->form_validation->set_rules($this->brand_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->brand_model->update([
                        'id' => $this->input->post('id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'status' => $this->input->post('status')
                    ], 'id');
                    
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "ecom_brands", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_brands/r', 'refresh');
                }
            }elseif ($type == 'd') {
                echo $this->brand_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-Commerce Brands';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_brands';
                $this->data['i'] = $this->brand_model->where('file',$this->input->get('file'))->get();
                $this->data['ecom_brands'] = $this->brand_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
                $this->_render_page($this->template, $this->data);
            }elseif($type == 'list'){
                $data = $this->sub_category_model->with_brands('fields:id, name, desc')->with_ecom_sub_sub_categories('fields:id, name, desc')->where(['id' =>$this->input->post('sub_cat_id')])->get_all();
                echo json_encode($data);
            }elseif($type == 'brnads_list'){
                $data = $this->brand_model->with_brands('fields:id, name')->where(['status' =>$this->input->post('brand_type')])->get_all();
                echo json_encode($data);
            }
    }  
    
}