<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;
class Meeseva extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('delivery_partner_address_model');
    }
    
    public function meeseva_list_get(){
        $pincode = $this->input->get('pincode');
        if(empty($pincode)){
            return $this->set_response_simple(NUll, 'Invalid input..!', REST_Controller::HTTP_OK, FALSE);
        }
        $pincodes = $this->delivery_partner_address_model->like('pincode', $pincode)->order_by('pincode', 'ASC')->get_all();
        $this->set_response_simple($pincodes, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    public function meeseva_by_id_get(){
        $id = $this->input->get('id');
        if(empty($id)){
            return $this->set_response_simple(NUll, 'Invalid input..!', REST_Controller::HTTP_OK, FALSE);
        }
        $pincodes = $this->delivery_partner_address_model->where('id', $id)->get();
        $this->set_response_simple($pincodes, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    
}

