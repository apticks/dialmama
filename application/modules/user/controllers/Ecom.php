<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class Ecom extends MY_REST_Controller
{
	private $client_id;
	private $client_secret;
	private $url = "https://api.instamojo.com/oauth2/token/";
	private $env = "production";
    public function __construct()
    {
        parent::__construct();
        
        $this->client_id = 'test_1Wy3tlxCFFLPgsvEhogcbKqVnZItlkl4Atf';
        $this->client_secret = 'test_kt1Lp1sHbY8FQ2gsZXtdjQ4QGsPydCF8NpCXK0JS7L2LpzubXn0GFpaq4E9MChAikxtYYYxg1LEl2yGlJwQU42mx7nGmFEM9X7HYepy0ygPKLSs8h6pHUAgufvZ';
        
        $this->load->model('category_model');
        $this->load->model('sub_category_model');
        $this->load->model('brand_model');
        $this->load->model('cart_model');
        $this->load->model('cart_variant_model');
        $this->load->model('model_model');
        $this->load->model('colour_model');
        $this->load->model('size_model');
        $this->load->model('product_model');
        $this->load->model('product_variant_model');
        $this->load->model('product_variant_value_model');
        $this->load->model('users_address_model');
        $this->load->model('order_model');
        $this->load->model('order_details_model');
        $this->load->model('payment_model');
        $this->load->model('coupon_model');
        $this->load->model('product_image_model');
    }
    
    /**
     * @author Mehar
     * @desc To get list of categories and targeted category as well
     * @param string $target
     */
    public function ecom_categories_get($target = '', $brand_id = '') {
        if(empty($target)){
            $data = $this->category_model->fields('id, name, desc')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/ecom_category_image/ecom_category_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif( !empty($target) && empty($brand_id)){
        	$data = $this->category_model->fields('id, name, desc')->with_ecom_sub_categories('fields: name, id')->with_brands('fields: name, id', 'order_by: name, ASC')->where('id', $target)->get();
            if(! empty($data)){
            	for($i = 0; $i < count($data['ecom_sub_categories']) ; $i++){
            		$data['ecom_sub_categories'][$i]['image'] = base_url().'uploads/ecom_sub_category_image/ecom_sub_category_'.$data['ecom_sub_categories'][$i]['id'].'.jpg';
            	}
            	for($i = 0; $i <= max(array_keys($data['brands'])) ; $i++){
            	    if(array_key_exists($i, $data['brands'])){
            	        $data['brands'][$i]['image'] = base_url().'uploads/ecom_brands_image/ecom_brands_'.$data['brands'][$i]['id'].'.jpg';
            	    }
            	}
            	$data['image'] = base_url().'uploads/ecom_category_image/ecom_category_'.$target.'.jpg';
            	$brand_q = $this->db->query("SELECT b.id, b.name, b.status FROM categories_brands as cb join brands as b on b.id = cb.brand_id where cb.cat_id = '".$data['id']."' and b.status=1 and b.deleted_at is null");
            	if($brand_q){
            	   $data['brands'] = $brand_q->result_array();
            	} else{
            	   $data['brands'] = [];
            	}
            	$mf_brand_q = $this->db->query("SELECT b.id, b.name, b.status FROM categories_brands as cb join brands as b on b.id = cb.brand_id where cb.cat_id = '".$data['id']."' and b.status=2 and b.deleted_at is null");
            	if($mf_brand_q){
            	    $data['manufacturer_brands'] = $mf_brand_q->result_array();
            	} else{
            	    $data['manufacturer_brands'] = [];
            	}
        }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
        	$data = $this->brand_model->fields('id, name, desc')->with_models('fields: id, name', 'order_by: id, DESC')->get($brand_id);
        	$this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    /**
     * @author Trupti
     * @desc To get list of sub categories and targeted category as well
     * @param string $target
     */
    public function ecom_sub_categories_get($target = '') {
        if(empty($target)){
            $data = $this->sub_category_model->fields('id, name, desc')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/ecom_sub_category_image/ecom_sub_category_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->sub_category_model->fields('id, name, desc')->where('id', $target)->get();
            if(! empty($data)){
            	$data['image'] = base_url().'uploads/ecom_sub_category_image/ecom_sub_category_'.$target.'.jpg';
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    
    /**
     * @author Mehar
     * @desc To get list of brands
     * @param string $target
     */
    public function ecom_brands_get($target = '') {
        if(empty($target)){
            $data = $this->brand_model->order_by('name', 'ASC')->fields('id, name, desc')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/ecom_brands_image/ecom_brands_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->brand_model->fields('id, name, desc')->where('id', $target)->get_all();
            $data['image'] = base_url().'uploads/ecom_brands_image/ecom_brands_'.$target.'.jpg';
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    
    /**
     * E-Commerce Cart
     *
     * @author Mehar
     * @desc To Manage Ecommerce Cart
     * @param string $type
     */
    public function cart_post($type = 'r', $target = 0){
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
            if ($type == 'c') {
                $_POST = json_decode(file_get_contents("php://input"), TRUE);
                $this->form_validation->set_rules($this->cart_model->rules);
                if ($this->form_validation->run() == false) {
                    $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                } else {
                	$variants = $this->input->post('variants');
                	$cart = $this->cart_model->where(['user_id' => $token_data->id, 'product_id' => $this->input->post('product_id')])->get();
                	if(! empty($cart)){
                		$this->db->where('cart_id', $cart['id']);
                		$this->db->delete('cart_variants');
                		
                		$id = $cart['id'];
                	}else {
                	    $id = 0;
                	}
                	
                	if(! empty($variants)){
                	    if(empty($id)){
                	        $id = $this->cart_model->insert([
                	            'user_id' => $token_data->id,
                	            'product_id' => $this->input->post('product_id'),
                	            'qty' => (! empty($this->input->post('qty')))? $this->input->post('qty') : 0
                	        ]);
                	    }
                	    
                	    foreach ($variants as $variant){
                		$this->cart_variant_model->insert([
                				'cart_id' => $id,
                				'product_id' => $this->input->post('product_id'),
                				'variant_id' => $variant['variant_id'],
                				'qty' => $variant['qty'],
                		]);
                	    }
                	    $is_order_from_exist = $this->db->get_where('order_form', ['product_id' => $this->input->post('product_id'), 'user_id' => $token_data->id])->result_array();
                	    if(! empty($is_order_from_exist)){
                	        $this->db->where('id', $is_order_from_exist['id']);
                	        $this->db->update('order_form', ['total' => (! empty($this->input->post('total')))? $this->input->post('total') : 0]);
                	    }
                	    
                	}else{
                	    $this->db->where(['product_id' => $this->input->post('product_id'), 'user_id' => $token_data->id]);
                	    $this->db->delete('order_form');
                	}
                	
                    $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
                }
            } elseif ($type == 'r') {
            	if(empty($target)){
            		$data = $this->cart_model->fields('id, user_id, product_id, qty')->with_product('fields: id, product_code, name, desc')->with_cart_variants('fields: id, cart_id, variant_id, qty')->where('user_id', $token_data->id)->get_all();
            		if(! empty($data)){foreach ($data as $k => $v){
            			if(! empty($data[$k]['cart_variants'])){
            				foreach  ($data[$k]['cart_variants'] as $key => $val){
            					$data[$k]['cart_variants'][$key]['variant'] = (empty($this->product_variant_model->fields('product_id, sku, price, qty, moq')->where('id', $val['variant_id'])->get()))? NULL: $this->product_variant_model->fields('product_id, sku, price, qty, moq')->where('id', $val['variant_id'])->get();
            					$data[$k]['cart_variants'][$key]['variant_value'] = (empty($this->product_variant_value_model->fields('id, product_id, model, color, size')->where('variant_id', $val['variant_id'])->get()))? NULL: $this->product_variant_value_model->fields('id, product_id, model, color, size')->where('variant_id', $val['variant_id'])->get();
            					if(! empty($data[$k]['cart_variants'][$key]['variant_value'])){
            						$data[$k]['cart_variants'][$key]['variant_value']['model'] = (empty($this->model_model->fields('id, brand_id, name')->where('id', $data[$k]['cart_variants'][$key]['variant_value']['model'])->get()))? NULL: $this->model_model->fields('id, brand_id, name')->where('id', $data[$k]['cart_variants'][$key]['variant_value']['model'])->get();
            						$data[$k]['cart_variants'][$key]['variant_value']['color'] = (empty($this->colour_model->fields('id, color_code, name')->where('id', $data[$k]['cart_variants'][$key]['variant_value']['color'])->get()))? NULL: $this->colour_model->fields('id, color_code, name')->where('id', $data[$k]['cart_variants'][$key]['variant_value']['color'])->get();
            						$data[$k]['cart_variants'][$key]['variant_value']['size'] = (empty($this->size_model->fields('id, size')->where('id', $data[$k]['cart_variants'][$key]['variant_value']['size'])->get()))? NULL: $this->size_model->fields('id, size')->where('id', $data[$k]['cart_variants'][$key]['variant_value']['size'])->get();
            					}
            				}
            			}
            			if(! empty($data)){foreach ($data as $k => $v){
            			    $product_images = $this->product_image_model->where('product_id', $data[$k]['product']['id'])->get();
            			    if(! empty($product_images)){
            			        $data[$k]['product']['image'] = base_url().'uploads/product_image/product_'.$product_images['id'].'.jpg?'.time();
            			    }else{
            			        $data[$k]['product']['image'] = base_url().'uploads/product_image/product_'.$data[$k]['product']['product_code'].'.jpg';
            			    }
            				
            			}}
            		}}
            	}else{
            		$data = $this->cart_model->fields('id, user_id, product_id, qty')->with_product('fields: id, product_code, name, desc')->with_cart_variants('fields: id, cart_id, variant_id, qty')->where(['user_id' => $token_data->id, 'id' => $target])->get();
            			if(! empty($data['cart_variants'])){
            				foreach  ($data['cart_variants'] as $key => $val){
            					$data['cart_variants'][$key]['variant'] = (empty($this->product_variant_model->fields('product_id, sku, price, qty, moq')->where('id', $val['variant_id'])->get()))? NULL: $this->product_variant_model->fields('product_id, sku, price, qty, moq')->where('id', $val['variant_id'])->get();
            					$data['cart_variants'][$key]['variant_value'] = (empty($this->product_variant_value_model->fields('id, product_id, model, color, size')->where('variant_id', $val['variant_id'])->get()))? NULL: $this->product_variant_value_model->fields('id, product_id, model, color, size')->where('variant_id', $val['variant_id'])->get();
            					if(! empty($data['cart_variants'][$key]['variant_value'])){
            						$data['cart_variants'][$key]['variant_value']['model'] = (empty($this->model_model->fields('id, brand_id, name')->where('id', $data['cart_variants'][$key]['variant_value']['model'])->get()))? NULL: $this->model_model->fields('id, brand_id, name')->where('id', $data['cart_variants'][$key]['variant_value']['model'])->get();
            						$data['cart_variants'][$key]['variant_value']['color'] = (empty($this->colour_model->fields('id, color_code, name')->where('id', $data['cart_variants'][$key]['variant_value']['color'])->get()))? NULL: $this->colour_model->fields('id, color_code, name')->where('id', $data['cart_variants'][$key]['variant_value']['color'])->get();
            						$data['cart_variants'][$key]['variant_value']['size'] = (empty($this->size_model->fields('id, size')->where('id', $data['cart_variants'][$key]['variant_value']['size'])->get()))? NULL: $this->size_model->fields('id, size')->where('id', $data['cart_variants'][$key]['variant_value']['size'])->get();
            					}
            				}
            			}
            			$product_images = $this->product_image_model->where('product_id', $data['product']['id'])->get();
            			if(! empty($product_images)){
            			    $data['product']['image'] = base_url().'uploads/product_image/product_'.$product_images['id'].'.jpg?'.time();
            			}else{
            			    $data['product']['image'] = base_url().'uploads/product_image/product_'.$data['product']['product_code'].'.jpg';
            			}
            	}
            	
                $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }  elseif ($type == 'u') {
                $_POST = json_decode(file_get_contents("php://input"), TRUE);
                $this->form_validation->set_rules($this->cart_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                } else {
                    $this->cart_model->update([
                        'id' => $this->input->post('id'),
                        'user_id' => $token_data->id,
                        'product_id' => $this->input->post('product_id'),
                    	'variant_id' => $this->input->post('variant_id'),
                        'qty' => $this->input->post('qty'),
                    ], 'id');
                    $this ->db ->where('user_id', $token_data->id);
                    $this ->db-> delete('order_form');
                    $this->set_response_simple(NULL, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
                }
            }elseif ($type == 'd') {
                $_POST = json_decode(file_get_contents("php://input"), TRUE);
                $cart = $this->cart_model->where(['id' => $this->input->post('id')])->get();
                if(! empty($cart)){
                	$this->cart_variant_model->where('cart_id', $cart['id'])->delete();
                	$this->cart_model->where(['id' => $cart['id']])->delete();
                }
                $this ->db ->where(['product_id' => $cart['product_id'], 'user_id' => $token_data->id]);
                $this ->db-> delete('order_form');
                $this->set_response_simple(NULL, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
            }
    }
    
    /**
     * E-Commerce Checkout
     *
     * @author Mehar
     * @desc To Manage Ecommerce Check out
     * @param string $type
     */
    public function ecom_checkout_post(){
            $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->users_address_model->rules);
            $this->form_validation->set_rules($this->order_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $address_id = 0;
                $name = '';
                if(empty($_POST['address_id'])){
                    if(! empty($this->input->post('name'))){
                        $address_id = $this->users_address_model->insert([
                            'user_id' => $token_data->id,
                            'name' => $this->input->post('name'),
                            'email' => $this->input->post('email'),
                            'phone' => $this->input->post('mobile'),
                            'address' => $this->input->post('address'),
                            'pincode' => $this->input->post('pincode'),
                        ]);
                        $name = $this->input->post('name');
                    }else {
                        $address_id = 0;
                    }
                    
                }  else {
                    $address_id = $_POST['address_id'];
                    $name = $this->users_address_model->where('id', $address_id)->get()['name'];
                }
            	$order_code = "DMO-".time().'-'.mt_rand();
                $order_id = $this->order_model->insert([
                    'user_id' => $token_data->id,
            		'order_no' => $order_code,
                    'customer_name' => (! empty($this->input->post('name')))? $this->input->post('name') : NULL,
                    'mobile_number' => (! empty($this->input->post('mobile')))? $this->input->post('mobile') : NULL,
                    'discount' => (! empty($this->input->post('discount')))? $this->input->post('discount') : 0,
                    'tax' => (! empty($this->input->post('tax')))? $this->input->post('tax') : 0,
                    'delivery_partner_address_id' => (! empty($this->input->post('delivery_partner_address_id')))? $this->input->post('delivery_partner_address_id') : NULL,
                    'total' => $this->input->post('total'),
                    'address_id' => $address_id,
                    'payment_method_id' => $this->input->post('payment_method_id')
                ]);
                if(! empty($order_id)){
                    if(! empty($_POST['coupons'])){foreach($_POST['coupons'] as $coupon){
            	        $this->db->insert('order_coupons',[
            	            'order_id' => $order_id,
            	            'coupon_id' => $coupon['coupon_id']
            	        ]);
                    }}
                	$order_no  = rand(100,100000).time();
                	if(! empty($_POST['products'])){foreach ($_POST['products'] as $product){
                        $this->order_details_model->insert([
                            'order_id' => $order_id,
                        	'vendor' => $this->product_model->where('id', $product['product_id'])->get()['user_id'],
                        		'order_item_code' => $order_no,
                            'product_id' => $product['product_id'],
                            'variant_id' => $product['variant_id'],
                            'qty' => $product['qty'],
                            'price' => $product['price'],
                            "coupon_id" => empty($product['coupon_id'])? NULL : $product['coupon_id'],
                            "discount_percentage" => $product['discount_percentage'],
                            "discount" => $product['discount'],
                            "total" => $product['total'],
                        ]);
                        $variant = $this->product_variant_model->where('id', $product['variant_id'])->get();
                        $reduced_stock = $variant['qty'] - $product['qty'];
                        $this->product_variant_model->update([
                            'id' => $product['variant_id'],
                            'qty' => $reduced_stock
                        ], 'id');
                	}}
                    
                    $cart = $this->cart_model->where(['user_id' => $token_data->id])->get_all();
                    if(! empty($cart)){foreach($cart as $c){
                        $this->cart_variant_model->where('cart_id', $c['id'])->delete();
                        $this->cart_model->where(['id' => $c['id']])->delete();
                    }}
                    $this->db->where('user_id', $token_data->id);
                    $this->db-> delete('order_form');
                }
                $this->send_sms('Thanks for shopping with us! your order ID is '.$order_code.' has been placed successfully. Will be shipped shortly. For more info : 18001234529.', (! empty($this->input->post('mobile')))? $this->input->post('mobile') : $token_data->userdetail->phone, 1307161768665767221);
                if($_POST['payment_method_id'] == 1)
                    $message = 'New order is arrived with id '.$order_code. ', And order total is RS '.$this->input->post('total').'/-'.'. Mode of payment is COD.';  //'New order is arrived with id '.$order_code. ', And order total is RS '.$this->input->post('total').'/-'.'. Mode of payment is COD.';
                else 
                    $message = 'New order is arrived with id '.$order_code. ', And order total is RS '.$this->input->post('total').'/-'.'. Mode of payment is INSTAMOJO.';
                
                $this->send_sms($message, '9666999444' );
                $this->set_response_simple(NULL, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
    }
    
    public function payment_post($type = 1){
    	$token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
    	$_POST = json_decode(file_get_contents("php://input"), TRUE);
    	$payment_id = 0;
    	if($type == 1){
    		$payment_id = $this->main_payment($_POST, $token_data->id, $type, 2);
    	}elseif ($type == 2){
    		$payment_id = $this->main_payment($_POST, $token_data->id, $type, 1);
    	}elseif ($type == 3){
    		$payment_id = $this->main_payment($_POST, $token_data->id, $type, 1);
    	}
    	$this->order_model->update([
    		'id' => $_POST['order_id'],
    		'payment_id' => $payment_id
    	], 'id');
    	$this->set_response_simple($payment_id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
    }
    
    public function main_payment($data = NULL, $user_id = NULL, $type = NULL, $status = NULL){
    	return $this->payment_model->insert([
    			'user_id' => $user_id,
    			'type' => $data['type'],
    			'amount' => $data['amount'],
    			'order_id' => $data['order_id'],
    			'txn_id' => $data['txn_id'], 
    			'payment_method' => $type,
    			'status' => $status
    	]);
    }
    
    public function send_message_get(){
        $status = json_decode($this->send_by_msg91('Hi  Thanks for shopping with us. Your order idas been placed successfully and will be shipped shortly. For more info | 1800 12345 29 |DIALMAMA', '7013907291'));
        if(! empty($status) && $status->type == 'success'){
            $this->send_by_msg91('New order is arrived with id', '7013907291' );
        }
        $this->set_response_simple($status, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    public function is_valid_coupon_get(){
    	$this->coupon_model->were(['code' => $this->input->get('code'), 'validity >=' => date('Y-m-d'), ])->get();
    	$this->set_response_simple($payment_id, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    public function inst_token_get(){
    	if (substr( $this->client_id, 0, 5 ) === "test_") {
    		$this->url = "https://test.instamojo.com/oauth2/token/";
    		$this->env = "test";
    	}
    	$curl = curl_init($this->url);
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
    	curl_setopt($curl, CURLOPT_HEADER, false);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_POST, true);
    	curl_setopt($curl, CURLOPT_POSTFIELDS, rawurldecode(http_build_query(array(
    			'client_id' => $this->client_id,
    			'client_secret' => $this->client_secret,
    			'grant_type' => 'client_credentials'
    	))));
    	$json = json_decode(curl_exec($curl));
    	if(curl_error($curl))
    	{
    		echo 'error:' . curl_error($curl);
    	}
    	if (isset($json->error)) {
    		return "Error: " . $json->error;
    		throw new \Exception("Error: " . $json->error);
    	}
    	$this->token = $json;
    	$this->set_response_simple($this->env . $json->access_token, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
    }
    public function coupons_get($type = 'list'){
        if($type == 'list'){
            $data = $this->coupon_model->where('cat_id', $this->input->get('cat_id'))->get_all();
            $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif ($type == 'validate'){
            if(empty($_GET['cat_id'])){
                $data = $this->db->query("SELECT * FROM `coupons` WHERE cat_id is null AND `code` = '".$this->input->get('code')."' AND validity >= CURRENT_DATE OR validity = NULL AND status = 1 AND deleted_at = NULL")->result_array();
                $this->set_response_simple((empty($data))? FALSE :$data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }else{
                $data = $this->db->query("SELECT * FROM `coupons` WHERE cat_id = ".$this->input->get('cat_id')." AND`code` = '".$this->input->get('code')."' AND validity >= CURRENT_DATE OR validity = NULL AND status = 1 AND deleted_at = NULL")->result_array();
                $this->set_response_simple((empty($data))? FALSE :$data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }
        }
    }
}
