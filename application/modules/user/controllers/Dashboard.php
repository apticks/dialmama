<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;
class Dashboard extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('order_model');
        $this->load->model('order_details_model');
        $this->load->model('user_model');
        $this->load->model('wallet_transaction_model');
        $this->load->model('product_variant_value_model');
        $this->load->model('model_model');
        $this->load->model('colour_model');
        $this->load->model('brand_model');
        $this->load->model('model_model');
        $this->load->model('category_model');
        $this->load->model('category_brand_model');
    }
    
    /**
     * @descvendor orders
     * @author Mehar
     * 
     * @param string $type
     */
    public function orders_get($type = 'today'){
    	$token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
    	if($type == 'today'){
    		$data = $this->order_details_model->with_product('fields:id, product_code, name, status')->with_vendor('fields:id, unique_id, email, phone, first_name')->with_variant('fields:id, sku, price, qty, moq')->where(['vendor' => $token_data->id, 'created_at Like' => '%'.date('Y-m-d').'%'])->get_all();
    		if(! empty($data)){foreach ($data as $k => $val){
    			$data[$k]['variant_values'] = $this->product_variant_value_model->where('variant_id', $val['variant']['id'])->get();
    			$data[$k]['variant_values']['model'] = $this->model_model->where('id', $$val['variant_values']['model'])->get();
    			$data[$k]['variant_values']['color'] = $this->colour_model->where('id', $$val['variant_values']['color'])->get();
    		}}
    		$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}elseif ($type == 'pending'){
    		$data = $this->order_details_model->with_product('fields:id, product_code, name, status')->with_vendor('fields:id, unique_id, email, phone, first_name')->with_variant('fields:id, sku, price, qty, moq')->where(['vendor' => $token_data->id, 'status' => 1])->get_all();
    		if(! empty($data)){foreach ($data as $k => $val){
    			$data[$k]['variant_values'] = $this->product_variant_value_model->where('variant_id', $val['variant']['id'])->get();
    			$data[$k]['variant_values']['model'] = $this->model_model->where('id', $$val['variant_values']['model'])->get();
    			$data[$k]['variant_values']['color'] = $this->colour_model->where('id', $$val['variant_values']['color'])->get();
    		}}
    		$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}elseif ($type == 'completed'){
    		$data = $this->order_details_model->with_product('fields:id, product_code, name, status')->with_vendor('fields:id, unique_id, email, phone, first_name')->with_variant('fields:id, sku, price, qty, moq')->where(['vendor' => $token_data->id, 'status' => 3])->get_all();
    		if(! empty($data)){foreach ($data as $k => $val){
    			$data[$k]['variant_values'] = $this->product_variant_value_model->where('variant_id', $val['variant']['id'])->get();
    			$data[$k]['variant_values']['model'] = $this->model_model->where('id', $$val['variant_values']['model'])->get();
    			$data[$k]['variant_values']['color'] = $this->colour_model->where('id', $$val['variant_values']['color'])->get();
    		}}
    		$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}elseif ($type == 'on_going'){
    		$data = $this->order_details_model->with_product('fields:id, product_code, name, status')->with_vendor('fields:id, unique_id, email, phone, first_name')->with_variant('fields:id, sku, price, qty, moq')->where(['vendor' => $token_data->id, 'status' => 2])->get_all();
    		if(! empty($data)){foreach ($data as $k => $val){
    			$data[$k]['variant_values'] = $this->product_variant_value_model->where('variant_id', $val['variant']['id'])->get();
    			$data[$k]['variant_values']['model'] = $this->model_model->where('id', $$val['variant_values']['model'])->get();
    			$data[$k]['variant_values']['color'] = $this->colour_model->where('id', $$val['variant_values']['color'])->get();
    		}}
    		$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}elseif ($type == 'accept'){
    		$data = $this->order_details_model->update([
    			'id' => $this->input->get('order_id'),
    			'status' => 2
    		], 'id');
    		$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}elseif ($type == 'history'){
    		$data = $this->order_model->where(['user_id' => $token_data->id])->get_all();
    		$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    /**
     * @desc vendor order history
     * @author Mehar
     *
     */
    public function order_history_get(){
    	$token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
    	$data = $this->order_model->with_user('fields: id, first_name, unique_id, email, phone')->with_address('fields: user_id, phone, email, name, address, pincode, status')->where('user_id', $token_data->id)->get_all();
    	$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     * @desc model operations
     * @author Mehar
     *
     */
    public function model_post($method = 'r', $target = NULL){
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
        if($method == 'c'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->model_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            }else{
                $id = $this->model_model->insert([
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'brand_id' => $this->input->post('brand_id')
                ]);
                $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
        }elseif ($method == 'r'){
            if(empty($target)){
            	$data = $this->model_model->order_by('id', 'DESC')->where('user_id', $token_data->id)->get_all();
            }else{
                $data = $this->model_model->with_brands('fields:id,name')->where('id', $target)->get_all();
            }
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif($method == 'd'){
            //$this->model_model->where('id',$target)->delete();
            $data = $this->db->delete('models', array('id' => $target));
            $this->set_response_simple(($data == FALSE)?FALSE: $data, 'Deleted..', REST_Controller::HTTP_OK,TRUE);
        }elseif ($method == 'u'){
            $_POST = json_decode(file_get_contents("php://input"),TRUE);
            $this->form_validation->set_rules($this->model_model->rules);
            if($this->form_validation->run()==false){
                $this->set_response_simple(validation_errors(),"Internal Server Error Occured", REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION,FALSE);
            }else{
                $data = $this->model_model->update([
                    "id" => $target,
                    "name" => $this->input->post("name"),
                    "brand_id" => $this->input->post("brand_id")
                ],'id');
            }
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Updated..!', REST_Controller::HTTP_ACCEPTED, TRUE);
        }
    }
    public function brand_post($method = 'r', $target = NULL){
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
    if($method == 'c'){
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $this->form_validation->set_rules($this->brand_model->rules);
        if ($this->form_validation->run() == false) {
            $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
        }else{
            $id = $this->brand_model->insert([
                'user_id' => $token_data->id,
                'name' => $this->input->post('name'),
                'status' => 2
            ]);
            foreach ($_POST['categories'] as $cat) {
                $category = $this->category_brand_model->insert([
                    'brand_id' => $id,
                    'cat_id' => $cat['cat_id'],
                ]);
            }
            $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
        }
    }elseif ($method == 'r'){
        if(empty($target)){
            $data = $this->brand_model->order_by('id', 'DESC')->with_categories('fields: id, name')->where(['user_id' => $token_data->id, 'status' => 2])->get_all();
        }
        else{
            $data = $this->brand_model->with_categories('fields: id, name')->where('id', $target)->get_all();
        }
        $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }elseif($method == 'd'){
        //$data = $this->brand_model->where('id',$target)->delete();
    	$this->db->delete('brands', array('brand_id' => $target));
        $data = $this->db->delete('categories_brands', array('brand_id' => $target));
        $this->set_response_simple(($data == FALSE)?FALSE: $data, 'Deleted..', REST_Controller::HTTP_OK,TRUE);
    }elseif ($method == 'u'){
        $_POST = json_decode(file_get_contents("php://input"),TRUE);
        $this->form_validation->set_rules($this->brand_model->rules);
        if($this->form_validation->run()==false){
            $this->set_response_simple(validation_errors(),"Internal Server Error Occured", REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION,FALSE);
        }else{
            $data = $this->brand_model->update([
                "id" => $target,
                "name" => $this->input->post("name"),
            ],'id');
            $this->db->delete('categories_brands', array('brand_id' => $target));
            foreach ($_POST['categories'] as $cat) {
                $category = $this->category_brand_model->insert([
                    'brand_id' => $target,
                    'cat_id' => $cat['cat_id'],
                ],'id');
            }
        }
        $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Updated..!'.$_POST['categories'], REST_Controller::HTTP_ACCEPTED, TRUE);
    }
    }
}

