<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;
class Master extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('users_address_model');
        $this->load->model('address_model');
        $this->load->model('brand_model');
        $this->load->model('model_model');
        $this->load->model('warrenty_model');
        $this->load->model('ram_model');
        $this->load->model('storage_model');
        $this->load->model('battery_capacity_model');
        $this->load->model('size_model');
        $this->load->model('colour_model');
        $this->load->model('compatible_for_model');
        $this->load->model('camera_model');
        $this->load->model('country_model');
        $this->load->model('state_model');
        $this->load->model('district_model');
    }
    
    /**
     * @author Trupti
     * @desc To get list of sub categories and targeted category as well
     * @param string $target
     */
    public function brands_get($type = 1, $target = '') {
    	if(empty($target)){
    	    $data = $this->brand_model->order_by('name', 'ASC')->fields('id, name')->where('status', $type)->get_all();
    		if(! empty($data)){
    			for ($i = 0; $i < count($data) ; $i++){
    				$data[$i]['image'] = base_url().'uploads/ecom_brands_image/ecom_brands_'.$data[$i]['id'].'.jpg';
    			}
    		}
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    	    $data = $this->brand_model->fields('id, name')->with_models('fields:id, name')->where('id', $target)->get(); //|order_inside:name desc
    	    $this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function warrenties_get($target = '') {
    	if(empty($target)){
    		$data = $this->warrenty_model->fields('id, time')->get_all();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->warrenty_model->fields('id, time')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function rams_get($target = '') {
    	if(empty($target)){
    		$data = $this->ram_model->fields('id, size')->get_all();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->ram_model->fields('id, size')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function storages_get($target = '') {
    	if(empty($target)){
    		$data = $this->storage_model->fields('id, size')->get_all();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->storage_model->fields('id, size')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function battey_capacities_get($target = '') {
    	if(empty($target)){
    		$data = $this->battery_capacity_model->fields('id, capacity')->get_all();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->battery_capacity_model->fields('id, capacity')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function sizes_get($target = '') {
    	if(empty($target)){
    		$data = $this->size_model->fields('id, size')->get_all();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->size_model->fields('id, size')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function colours_get($target = '') {
    	if(empty($target)){
    		$data = $this->colour_model->fields('id, name, color_code')->get_all();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->colour_model->fields('id, name, color_code')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function models_get($target = '') {
        $brand_ids = (! empty($this->input->get('brand_ids')))? $this->input->get('brand_ids') : 1;
    	if(empty($target)){
    	    $data = $this->model_model->fields('id, name')->where('brand_id', explode(",", $brand_ids))->order_by('id', 'ASC')->order_by('brand_id', 'ASC')->get_all();
    	    $this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->model_model->fields('id, name')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function compatible_for_get($target = '') {
    	if(empty($target)){
    		$data = $this->compatible_for_model->fields('id, cat_id, name')->where('cat_id', $this->input->get('cat_id'))->get_all();
    		
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->compatible_for_model->fields('id, cat_id, name')->where(['id' => $target, 'cat_id' => $this->input->get('cat_id')])->get();
    		$this->set_response_simple(($data == FALSE)? NULL : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    
    public function camera_get($target = '') {
    	if(empty($target)){
    		$data = $this->camera_model->fields('id, pixels')->get_all();
    		
    		$this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}else{
    		$data = $this->camera_model->fields('id, pixels')->where('id', $target)->get();
    		$this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    	}
    }
    /**
     * User address
     *
     * To Manage address
     *
     * @author Trupti
     * @param string $type
     */
    public function user_address_post($type = 'r')
    {
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        if ($type == 'c') {
            $this->form_validation->set_rules($this->users_address_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $id = $this->users_address_model->insert([
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'pincode' => $this->input->post('pincode')
                ]);
                $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
        } elseif ($type == 'r') {
        	$data = $this->users_address_model->fields('id, user_id, name, phone, email, address, pincode, status')->where('user_id', $token_data->id)->get_all();
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        } elseif ($type == 's') {
            $data = $this->users_address_model->fields('id, user_id, name, phone, email, address, pincode, status')->get('id', $this->input->post('id'));
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->users_address_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $ll = $this->users_address_model->update([
                    'id' => $this->input->post('id'),
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'pincode' => $this->input->post('pincode')
                ], 'id');
                $this->set_response_simple($ll, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
            }
        } elseif ($type == 'd') {
            $ll = $this->users_address_model->delete([
                'id' => $this->input->post('id')
            ]);
            $this->set_response_simple($ll, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
        }elseif ($type == 'default'){
            $ll = $this->users_address_model->update([
                'id' => $this->input->post('id'),
                'status' => 1
            ], 'id');
            $this->set_response_simple($ll, 'Default address set..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    
   /**
     * @desc profile management
     * @author Mehar
     * 
     * @param string $type
     */
    public function profile_post($type = 'r'){
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
        $this->token = $token_data;
        if($type == 'r'){
        	$data = $this->user_model->fields('first_name, last_name, phone, email, store, wallet, unique_id')->with_groups('fields:id , name')->with_addresses('fields: id, address , address2, city, district, state, country, zip, phone', 'where: status= \'1\'')->where('id', $token_data->id)->get();
        	if(! empty($data['addresses'])){foreach ($data['addresses'] as $key => $address){
        	    $data['addresses'][$key]['country'] = $this->country_model->where('id', $address['country'])->get() ? $this->country_model->fields('id, name')->where('id', $address['country'])->get(): NULL;
        	    $data['addresses'][$key]['state'] = $this->state_model->where('id', $address['state'])->get()? $this->state_model->fields('id, country_id, name')->where('id', $address['state'])->get(): NUll;
        	    $data['addresses'][$key]['district'] = $this->district_model->where('id', $address['district'])->get()? $this->district_model->fields('id, country_id, state_id, name')->where('id', $address['district'])->get(): NULL;
        	}}
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif($type == 'u'){
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            $this->form_validation->set_rules($this->user_model->rules['profile']);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Validation Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                
                $id = $this->user_model->update([
                    'id' => $token_data->id,
                    'first_name' => $this->input->post('fname'),
                    'last_name' => $this->input->post('lname'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'store' => $this->input->post('store'),
                ], 'id');
                if($id){
                    if(! empty($this->input->post('address_id'))){
                        $this->address_model->update([
                            'id' => $this->input->post('address_id'),
                            'user_id' => $token_data->id,
                            'address' => (empty( $this->input->post('address')))? NULL : $this->input->post('address'),
                            'address2' => (empty( $this->input->post('address2')))? NULL : $this->input->post('address2'),
                            'city' => (empty( $this->input->post('city')))? NULL : $this->input->post('city'),
                            'district' => (empty( $this->input->post('district')))? NULL : $this->input->post('district'),
                            'state' => (empty( $this->input->post('state')))? NULL : $this->input->post('state'),
                            'country' => (empty( $this->input->post('country')))? 1 :$this->input->post('country'),
                            'zip' => (empty( $this->input->post('zip')))? NULL : $this->input->post('zip')
                        ], 'id');
                    }else{
                        $addresses = $this->address_model->where('user_id', $token_data->id)->get_all();
                        if(empty($addresses)){
                            $this->address_model->insert([
                                'user_id' => $token_data->id,
                                'address' => (empty( $this->input->post('address')))? NULL : $this->input->post('address'),
                                'address2' => (empty( $this->input->post('address2')))? NULL : $this->input->post('address2'),
                                'city' => (empty( $this->input->post('city')))? NULL : $this->input->post('city'),
                                'district' => (empty( $this->input->post('district')))? NULL : $this->input->post('district'),
                                'state' => (empty( $this->input->post('state')))? NULL : $this->input->post('state'),
                                'country' => (empty( $this->input->post('country')))? 1 : $this->input->post('country'),
                                'zip' => (empty( $this->input->post('zip')))? NULL : $this->input->post('zip')
                            ]);
                        }
                        
                    }
                }
                $this->set_response_simple($id, 'Profile Updated..!', REST_Controller::HTTP_OK, TRUE);
            }
        }elseif ($type == 'reset'){
            $this->form_validation->set_rules($this->user_model->rules['reset']);
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $identity = $token_data->userdetail->phone;
                $change = $this->ion_auth->change_password($identity, $this->input->post('opass'), $this->input->post('npass'));
                if ($change) {
                    $this->set_response_simple($change, 'Password Changed..!', REST_Controller::HTTP_OK, TRUE);
                } else {
                    $this->set_response_simple($change, 'Internal Error Occured..!', REST_Controller::HTTP_CONFLICT, FALSE);
                }
            }
        }elseif ($type == 'group_update'){
        	$_POST = json_decode(file_get_contents('php://input'), TRUE);
        	$groupData = $this->input->post('role');
        	if (isset($groupData) && ! empty($groupData)) {
        		$this->ion_auth->remove_from_group('', $token_data->id);
        		foreach ($groupData as $key =>$grp) {
        			$this->ion_auth->add_to_group($grp, $token_data->id);
        		}
        	} 
        	$this->set_response_simple(NULL, 'Group Changed..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
     public function brand_type_get($type = '') {
        if(empty($type)){
            $data = $this->brand_model->order_by('name', 'ASC')->fields('id, name')->where(['status' =>1])->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/ecom_brands_image/ecom_brands_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->brand_model->order_by('name', 'ASC')->fields('id, name')->where(['status' =>2])->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
}

