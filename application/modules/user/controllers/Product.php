<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;

class Product extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('product_model');
        $this->load->model('product_variant_model');
        $this->load->model('product_variant_value_model');
        $this->load->model('category_model');
        $this->load->model('sub_category_model');
        $this->load->model('brand_model');
        $this->load->model('model_model');
        $this->load->model('colour_model');
        $this->load->model('size_model');
        $this->load->model('product_image_model');
    }
    
    /**
     * @desc To manage product
     * @author Mehar
     * 
     * @param string $type
     */
    public function manage_product_post($type = 'c'){
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
        $this->product_model->user_id = $token_data->id;
        if($type == 'c'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->product_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
            	$sounds_like = $this->sounds_like($this->input->post('name'), $this->input->post('cat_id'), $this->input->post('sub_cat_id'), $this->input->post('brand_id'));
            	$product_code = generate_serial_no('DMMSPL', 2, rand(999, 9999));
                $product_id = $this->product_model->insert([
                	'user_id' => $token_data->id,
                    'custom_product_id' => ( empty($this->input->post('custom_product_id')))? NULL: $this->input->post('custom_product_id'),
                    'cat_id' => $this->input->post('cat_id'),
                	'sub_cat_id' => ( empty($this->input->post('sub_cat_id')))? 0: $this->input->post('sub_cat_id'),
                    'product_code' => $product_code,
                    'brand_id' => $this->input->post('brand_id'),
                	'model' => $this->input->post('model'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc'),
                    'screen_size' => ( empty($this->input->post('screen_size')))? NULL: $this->input->post('screen_size'),
                    'warrenty' => ( empty($this->input->post('warrenty')))? NULL: $this->input->post('warrenty'),
                    'ram' =>  ( empty($this->input->post('ram')))? NULL: $this->input->post('ram'),
                    'storage' => ( empty($this->input->post('storage')))? NULL: $this->input->post('storage'),
                	'battery_capacity' => ( empty($this->input->post('battery_capacity')))? NULL: $this->input->post('battery_capacity'),
                		
                		'front_cam' => ( empty($this->input->post('front_cam')))? NULL: $this->input->post('front_cam'),
                		'back_cam' => ( empty($this->input->post('back_cam')))? NULL: $this->input->post('back_cam'),
                		'network' => ( empty($this->input->post('network')))? NULL: $this->input->post('network'),
                		'condition' => ( empty($this->input->post('condition')))? NULL: $this->input->post('condition'),
                		'compatible_for' => ( empty($this->input->post('compatible_for')))? NULL: $this->input->post('compatible_for'),
                		'touch_screen' => ( empty($this->input->post('touch_screen')))? NULL: $this->input->post('touch_screen'),
                		'resolution' => ( empty($this->input->post('resolution')))? NULL: $this->input->post('resolution'),
                		'hdmi' => ( empty($this->input->post('hdmi')))? NULL: $this->input->post('hdmi'),
                		'usb' => ( empty($this->input->post('usb')))? NULL: $this->input->post('usb'),
                		'style' => ( empty($this->input->post('style')))? NULL: $this->input->post('style'),
                		'bluetooth' => ( empty($this->input->post('bluetooth')))? NULL: $this->input->post('bluetooth'),
                		'android' => ( empty($this->input->post('android')))? NULL: $this->input->post('android'),
                		
                		'is_having_camera' => ( empty($this->input->post('is_having_camera')))? NULL: $this->input->post('is_having_camera'),
                		'mixed_features' => ( empty($this->input->post('mixed_features')))? NULL: $this->input->post('mixed_features'),
                		'mixed_compatibility' => ( empty($this->input->post('mixed_compatibility')))? NULL: $this->input->post('mixed_compatibility'),
                		'mixed_extra_features' => ( empty($this->input->post('mixed_extra_features')))? NULL: $this->input->post('mixed_extra_features'),
                	'sounds_like' => $sounds_like
                		
                ]);
                $variant_value_id = [];
                if(! empty($product_id)){
                    $variants = $this->input->post('variants');
                    if(! empty($variants)){foreach ($variants as $variant){
                        $variant_id = $this->product_variant_model->insert([
                            'product_id' => $product_id,
                            'sku' => $variant['sku'],
                            'qty' => $variant['qty'],
                        	'moq' => $variant['moq'],
                            'price' => $variant['price'],
                        ]);
                        
                        array_push($variant_value_id, [
                            'product_id' => $product_id,
                            'variant_id' => $variant_id,
                            'model' => (! empty($variant['model']) && $variant['model'] != ' ')?$variant['model']: NULL,
                            'color' => (! empty($variant['color']) && $variant['color'] != ' ')?$variant['color']: NULL,
                            'size' => (! empty($variant['size']) && $variant['size'] != ' ')?$variant['size']: NULL,
                        ]);
                    }
                    $this->db->insert_batch('product_variant_values', $variant_value_id);
                    log_message('debug', $this->db->last_query());
                    }
                    
                    if (!file_exists('./uploads/product_image/')) {
                        mkdir('./uploads/product_image/', 0777, true);
                    }
                    if(! empty($this->input->post('image')) && is_array($this->input->post('image'))){
                        foreach ($this->input->post('image') as $key =>$val){
                            $image_id = $this->product_image_model->insert([
                                'product_id' => $product_id,
                                'ext' => 'jpg'
                            ]);
                            file_put_contents("./uploads/product_image/product_$image_id.jpg", base64_decode($val));
                        }
                    }else{
                         $image_id = $this->product_image_model->insert([
                             'product_id' => $product_id,
                             'ext' => 'jpg'
                         ]);
                         file_put_contents("./uploads/product_image/product_$image_id.jpg", base64_decode($this->input->post('image')));
                    }
                    $this->set_response_simple($product_id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
                }else{
                    $this->set_response_simple(NULL, 'Internal server Error', REST_Controller::HTTP_CONFLICT, FALSE);
                }
            }
        }elseif ($type == 'r'){
        	$data = $this->product_model
        		->fields('id, product_code, custom_product_id, name, desc, is_having_camera, mixed_features, screen_size, mixed_compatibility, battery_capacity, mixed_extra_features, front_cam, back_cam, model, network, condition, touch_screen, resolution, hdmi, usb, style, bluetooth, android, status')
	        	->order_by('id', 'DESC')
	        	->with_user('fields: id, unique_id')
        		->with_category('fields: id, name')
        		->with_sub_category('fields:id, name')
        		->with_brand('fields:id, name')
        		->with_model('fields:id, brand_id, name')
        		->with_warrenty('fields:id, time')
        		->with_ram('fields:id, size')
        		->with_storage('fields:id, size')
        		->with_variants('fields:id, sku, qty, price, moq')
        		->with_variant_values('fields:variant_id, model, color, size')
        		->with_compatible_for('fields: id, name')
        		->with_images('fields: id, ext')
        		->where('user_id', $token_data->id)
        		->get_all();
        		if(! empty($data)){
        		    for ($i = 0; $i < count($data) ; $i++){
        		        if(! empty($data[$i]['images'])){foreach ($data[$i]['images'] as $key => $image){
        		            $data[$i]['images'][$key]['image'] = base_url().'uploads/product_image/product_'.$image['id'].'.jpg';
        		        }}else {
        		            $data[$i]['images'] = [];
        		        }
        		    }
        		}
        	$this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        	
        }elseif ($type == 'image_delete'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->product_image_model->delete(['id' => $this->input->post('image_id')]);
            unlink("./uploads/product_image/product_".$this->input->post('image_id').".jpg");
            $this->set_response_simple(NULL, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
        }elseif ($type == 'd'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $product_id = $this->product_model->delete(['id' => $this->input->post('product_id')]);
            $this->set_response_simple($product_id, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
        }elseif ($type == 'u'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->product_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $product_code = $this->input->post('product_code');
                $sounds_like = $this->sounds_like($this->input->post('name'), $this->input->post('cat_id'), $this->input->post('sub_cat_id'), $this->input->post('brand_id'));
                $product_id = $this->input->post('product_id');
                $this->product_model->update([
                    'id' => $product_id,
                    'user_id' => $token_data->id,
                    //'custom_product_id' => ( empty($this->input->post('custom_product_id')))? NULL: $this->input->post('custom_product_id'),
                    //'cat_id' => $this->input->post('cat_id'),
                    //'sub_cat_id' => $this->input->post('sub_cat_id'),
                    //'brand_id' => $this->input->post('brand_id'),
                	//'model' => $this->input->post('model'),
                    'name' => $this->input->post('name'),
                    //'desc' => $this->input->post('desc'),
                	//'screen_size' => ( empty($this->input->post('screen_size')))? NULL: $this->input->post('screen_size'),
                    'warrenty' => ( empty($this->input->post('warrenty')))? NULL: $this->input->post('warrenty'),
                    'ram' =>  ( empty($this->input->post('ram')))? NULL: $this->input->post('ram'),
                    'storage' => ( empty($this->input->post('storage')))? NULL: $this->input->post('storage'),
                		//'battery_capacity' => ( empty($this->input->post('battery_capacity')))? NULL: $this->input->post('battery_capacity'),
                		//'front_cam' => ( empty($this->input->post('front_cam')))? NULL: $this->input->post('front_cam'),
                		//'back_cam' => ( empty($this->input->post('back_cam')))? NULL: $this->input->post('back_cam'),
                		//'network' => ( empty($this->input->post('network')))? NULL: $this->input->post('network'),
                		//'condition' => ( empty($this->input->post('condition')))? NULL: $this->input->post('condition'),
                		//'compatible_for' => ( empty($this->input->post('compatible_for')))? NULL: $this->input->post('compatible_for'),
                		//'touch_screen' => ( empty($this->input->post('touch_screen')))? NULL: $this->input->post('touch_screen'),
                		//'resolution' => ( empty($this->input->post('resolution')))? NULL: $this->input->post('resolution'),
                		//'hdmi' => ( empty($this->input->post('hdmi')))? NULL: $this->input->post('hdmi'),
                		//'usb' => ( empty($this->input->post('usb')))? NULL: $this->input->post('usb'),
                		//'style' => ( empty($this->input->post('style')))? NULL: $this->input->post('style'),
                		//'bluetooth' => ( empty($this->input->post('bluetooth')))? NULL: $this->input->post('bluetooth'),
                		//'android' => ( empty($this->input->post('android')))? NULL: $this->input->post('android'),
                		//'is_having_camera' => ( empty($this->input->post('is_having_camera')))? NULL: $this->input->post('is_having_camera'),
                		//'mixed_features' => ( empty($this->input->post('mixed_features')))? NULL: $this->input->post('mixed_features'),
                		//'mixed_compatibility' => ( empty($this->input->post('mixed_compatibility')))? NULL: $this->input->post('mixed_compatibility'),
                		//'mixed_extra_features' => ( empty($this->input->post('mixed_extra_features')))? NULL: $this->input->post('mixed_extra_features'),
                    'sounds_like' => $sounds_like
                ], 'id');
                
                $variants = $this->input->post('variants');
                foreach ($variants as $variant){
                    $var = $this->product_variant_model->where('sku', $variant['sku'])->get();
                    $variant_id = $this->product_variant_model->update([
                        'id' => $var['id'],
                        'qty' => $variant['qty'],
                        'price' => $variant['price'],
                    	'moq' => $variant['moq'],
                    ],'id');
                    
                    $variant_value_id = $this->product_variant_value_model->update([
                        'variant_id' => $var['id'],
                        'model' => (! empty($variant['model']))?$variant['model']: NULL,
                        'color' => (! empty($variant['color']))?$variant['color']: NULL,
                        'size' => (! empty($variant['size']))?$variant['size']: NULL,
                    ], 'variant_id');
                }
                if(! empty($_POST['images'])){foreach ($_POST['images'] as $key => $image){
                    if(! empty($image['id'])){
                        unlink("./uploads/product_image/product_".$image['id'].".jpg");
                        file_put_contents("./uploads/product_image/product_".$image['id'].".jpg", base64_decode($image['image']));
                    }else {
                        $image_id = $this->product_image_model->insert([
                            'product_id' => $product_id,
                            'ext' => 'jpg'
                        ]);
                        file_put_contents("./uploads/product_image/product_$image_id.jpg", base64_decode($image['image']));
                    }
                }}
                $this->set_response_simple(NULL, 'Product updated..!', REST_Controller::HTTP_ACCEPTED, TRUE);
            }
        }elseif ($type == 'change_status'){
        	$_POST = json_decode(file_get_contents("php://input"), TRUE);
        	$product_id = $this->product_model->update(['id' => $this->input->post('product_id'), 'status' => $this->input->post('status')], 'id');
        	$this->set_response_simple($product_id, 'Status changed..!', REST_Controller::HTTP_ACCEPTED, TRUE);
        }
      
    }
    
    public function sounds_like($name = NULL, $cat_id = NULL, $sub_cat_id = NULL, $brand_id = NULL){
    	$sounds_like = '';
    	if(! is_null($cat_id)){
    		$cat_name = $this->category_model->fields('name')->where('id', $cat_id)->get();
    		$sounds_like .= metaphone($cat_name['name']). ' ';
    	}
    	if (! is_null($sub_cat_id)){
    		$sub_cat_name = $this->sub_category_model->fields('name')->where('id', $sub_cat_id)->get();
    		$sounds_like .= metaphone($sub_cat_name['name']). ' ';
    	}
    	if (! is_null($brand_id)){
    		$brand_name = $this->brand_model->fields('name')->where('id', $brand_id)->get();
    		$sounds_like .= metaphone($brand_name['name']). ' ';
    	}
    	if (! is_null($name)) {foreach (explode(' ', $name) as $n){
    	    $sounds_like .= metaphone($n) . ' ';
    	}}
    	return $sounds_like;
    }
    
    /**
     * @author Mehar
     * @desc To get list of Products
     * @param string $limit
     * @param string $offset
     */
    public function products_get(){
        if(empty($_GET['product_id'])){
        	$data = $this->product_model->all((! empty($this->input->get('cat_id')))?$this->input->get('cat_id'):NULL, (! empty($this->input->get('sub_cat_id')))?$this->input->get('sub_cat_id'):NULL, (! empty($this->input->get('brand_id')))?$this->input->get('brand_id'):NULL, (! empty($this->input->get('q')))?$this->input->get('q'):NULL, (! empty($this->input->get('user_id')))?$this->input->get('user_id'):NULL);
             if(! empty($data['result'])){
                foreach  ($data['result'] as $key => $product){
                    $data['result'][$key]['variants'] = $this->product_variant_model->where('product_id', $product['id'])->get_all();
                    if(! empty($data['result'][$key]['variants'])){
                        $data['result'][$key]['price'] = $data['result'][$key]['variants'][array_search(min(array_column($data['result'][$key]['variants'], 'price')), array_column($data['result'][$key]['variants'], 'price'))]['price'];
                        $data['result'][$key]['max_price'] = $data['result'][$key]['variants'][array_search(max(array_column($data['result'][$key]['variants'], 'price')), array_column($data['result'][$key]['variants'], 'price'))]['price'];
                        $data['result'][$key]['moq'] = $data['result'][$key]['variants'][array_search(min(array_column($data['result'][$key]['variants'], 'moq')), array_column($data['result'][$key]['variants'], 'moq'))]['moq'];
                        $data['result'][$key]['qty'] = array_sum(array_column($data['result'][$key]['variants'], 'qty'));
                    }else{
                        $data['result'][$key]['price'] = 0;
                        $data['result'][$key]['max_price'] = 0;
                        $data['result'][$key]['moq'] = 0;
                        $data['result'][$key]['qty'] = 0;
                    }
                    if(! empty($data['result'][$key]['image_id'])){
                        $data['result'][$key]['image'] = base_url().'uploads/product_image/product_'.$data['result'][$key]['image_id'].'.jpg';
                    }else{
                        $data['result'][$key]['image'] = base_url().'uploads/product_image/product_not_available.jpg';
                    }
                }
            } 
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->product_model
            ->fields('id, product_code, custom_product_id, name, desc, is_having_camera, mixed_features, mixed_compatibility, screen_size, battery_capacity, mixed_extra_features, front_cam, back_cam, model, network, condition, touch_screen, resolution, hdmi, usb, style, bluetooth, android')
            ->order_by('id', 'DESC')
            ->with_user('fields: id, unique_id')
            ->with_category('fields: id, name')
            ->with_sub_category('fields:id, name')
            ->with_brand('fields:id, name')
            ->with_model('fields:id, brand_id, name')
            ->with_warrenty('fields:id, time')
            ->with_ram('fields:id, size')
            ->with_storage('fields:id, size')
            ->with_variants('fields:id, sku, qty, price, moq')
            ->with_variant_values('fields:variant_id, model, color, size')
            ->with_compatible_for('fields: id, name')
            ->with_images('fields: id, ext')
            ->where('id', $_GET['product_id'])
            ->get();
            $data['variant_values'] = $this->db->query("SELECT pvl.product_id, pvl.variant_id, pvl.model, pvl.color, pvl.size, m.brand_id FROM `products` as p LEFT JOIN product_variants as pv ON p.id = pv.product_id LEFT JOIN product_variant_values as pvl ON pv.id = pvl.variant_id join models as m ON pvl.model = m.id WHERE p.id = ".$_GET['product_id']." order by m.brand_id ASC")->result_array();
            if(! empty($data['variant_values'])){
            	foreach  ($data['variant_values'] as $key => $val){
            		$data['variant_values'][$key]['model'] = (empty($this->model_model->fields('id, brand_id, name')->where('id', $data['variant_values'][$key]['model'])->get()))? NULL: $this->model_model->fields('id, brand_id, name')->where('id', $data['variant_values'][$key]['model'])->get();
            		$data['variant_values'][$key]['color'] = (empty($this->colour_model->fields('id, color_code, name')->where('id', $data['variant_values'][$key]['color'])->get()))? NULL: $this->colour_model->fields('id, color_code, name')->where('id', $data['variant_values'][$key]['color'])->get();
            		$data['variant_values'][$key]['size'] = (empty($this->size_model->fields('id, size')->where('id', $data['variant_values'][$key]['size'])->get()))? NULL: $this->size_model->fields('id, size')->where('id', $data['variant_values'][$key]['size'])->get();
            	}
            }
            $data['price'] = $data['variants'][array_search(min(array_column($data['variants'], 'price')), array_column($data['variants'], 'price'))]['price'];
            $data['max_price'] = $data['variants'][array_search(max(array_column($data['variants'], 'price')), array_column($data['variants'], 'price'))]['price'];
            $data['moq'] = $data['variants'][array_search(min(array_column($data['variants'], 'moq')), array_column($data['variants'], 'moq'))]['moq'];
            $data['qty'] = array_sum(array_column($data['variants'], 'qty'));
            if(! empty($data['images'])){foreach ($data['images'] as $key => $image){
                $data['images'][$key]['image'] = base_url().'uploads/product_image/product_'.$image['id'].'.jpg';
            }}else {
                $data['images'] = NULL;
            }
            $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    public function order_form_post($type = 'c'){
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
        if($type == 'c'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $order_form = $this->input->post('order_form');
            $this ->db ->where('user_id', $token_data->id);
            $this ->db-> delete('order_form');
            foreach ($order_form as $key => $val){
               $this->db->insert('order_form', [
                   'user_id' => $token_data->id,
                   'product_id' => $val['product_id'],
                   'total' => $val['total']
               ]);
            }
            $this->set_response_simple($order_form, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif ($type == 'r'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $order_form = $this->db->get_where('order_form', ['user_id' => $token_data->id])->result_array();
             foreach ($order_form as $key => $val){
                $order_form[$key]['product'] = $this->product_model->fields('id, product_code, name')->where('id', $order_form[$key]['product_id'])->get();
                $product_images = $this->product_image_model->where('product_id', $order_form[$key]['product']['id'])->get();
                if(! empty($product_images)){
                    $order_form[$key]['image'] = base_url().'uploads/product_image/product_'.$product_images['id'].'.jpg?'.time();
                }else{
                    $order_form[$key]['image'] = base_url().'uploads/product_image/product_'.$order_form[$key]['product']['product_code'].'.jpg';
                }
            } 
            $this->set_response_simple($order_form, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
       
    }
}

