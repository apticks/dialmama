<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class Orders extends MY_REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('category_model');
        $this->load->model('sub_category_model');
        $this->load->model('brand_model');
        $this->load->model('cart_model');
        $this->load->model('cart_variant_model');
        $this->load->model('model_model');
        $this->load->model('colour_model');
        $this->load->model('size_model');
        $this->load->model('product_model');
        $this->load->model('product_variant_model');
        $this->load->model('product_variant_value_model');
        $this->load->model('users_address_model');
        $this->load->model('order_model');
        $this->load->model('order_details_model');
        $this->load->model('payment_model');
        $this->load->model('coupon_model');
        $this->load->model('product_image_model');
    }
    
    public function by_seller_get(){
        $token_data = $this->validate_token($this->input->get_request_header('TOKEN'));
        $id = $this->input->get('order_id');
        if(empty($id)){
            $orders = $this->order_model->all(
                $limit = empty($this->input->get('limit'))? NULL: $this->input->get('limit'), 
                $offset = empty($this->input->get('offset'))? NULL: $this->input->get('offset'), 
                $start_date = empty($this->input->get('start_date'))? NULL: $this->input->get('start_date'), 
                $end_date = empty($this->input->get('end_date'))? NULL: $this->input->get('end_date'), 
                $status = empty($this->input->get('status'))? NULL: $this->input->get('status'),
                "seller_orders",
                $token_data->id
                );
            if(! empty($orders)){ foreach ($orders as $key=> $order){
                $orders[$key]['total'] = $this->db->query("SELECT SUM(qty * price) as total FROM order_details where order_id = ".$order['id']." and vendor = ".$token_data->id." group by vendor;")->row()->total;
            }}else {
                $order = NULL;
            }
            $this->set_response_simple($orders, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else {
            $order = $this->order_model
            ->with_order_details('fields: id, order_id, product_id, variant_id, qty, price, discount, total, coupon_id, created_at, status', 'where: vendor='.$token_data->id)
            ->where('id', $id)->get();
            if(! empty($order['order_details'])){ foreach($order['order_details'] as $k => $product){
                $order['order_details'][$k]['product_details'] = $this->product_model
                ->fields('id, product_code, custom_product_id, name, desc, is_having_camera, mixed_features, mixed_compatibility, screen_size, battery_capacity, mixed_extra_features, front_cam, back_cam, model, network, condition, touch_screen, resolution, hdmi, usb, style, bluetooth, android')
                ->order_by('id', 'DESC')
                ->with_user('fields: id, unique_id')
                ->with_category('fields: id, name')
                ->with_sub_category('fields:id, name')
                ->with_brand('fields:id, name')
                ->with_model('fields:id, brand_id, name')
                ->with_warrenty('fields:id, time')
                ->with_ram('fields:id, size')
                ->with_storage('fields:id, size')
                ->with_compatible_for('fields: id, name')
                ->with_images('fields: id, ext')
                ->where('id', $product['product_id'])
                ->with_variants('fields:id, sku, qty, price, moq', 'where: id='.$product['variant_id'])
                ->with_variant_values('fields:variant_id, model, color, size', 'where: variant_id='.$product['variant_id'])
                ->get();
                //print_array($this->db->last_query());
                $order['order_details'][$k]['product_details']['variant_values'] = $this->db->query("SELECT pvl.product_id, pvl.variant_id, pvl.model, pvl.color, pvl.size, m.brand_id FROM `products` as p LEFT JOIN product_variants as pv ON p.id = pv.product_id LEFT JOIN product_variant_values as pvl ON pv.id = pvl.variant_id join models as m ON pvl.model = m.id WHERE pvl.variant_id = ".$product['variant_id']." order by m.brand_id ASC")->result_array();
                if(! empty($order['order_details'][$k]['product_details']['variant_values'])){
                    foreach  ($order['order_details'][$k]['product_details']['variant_values'] as $key => $val){
                        $order['order_details'][$k]['product_details']['variant_values'][$key]['model'] = (empty($this->model_model->fields('id, brand_id, name')->where('id',  $order['order_details'][$k]['product_details']['variant_values'][$key]['model'])->get()))? NULL: $this->model_model->fields('id, brand_id, name')->where('id',  $order['order_details'][$k]['product_details']['variant_values'][$key]['model'])->get();
                        $order['order_details'][$k]['product_details']['variant_values'][$key]['color'] = (empty($this->colour_model->fields('id, color_code, name')->where('id',  $order['order_details'][$k]['product_details']['variant_values'][$key]['color'])->get()))? NULL: $this->colour_model->fields('id, color_code, name')->where('id',  $order['order_details'][$k]['product_details']['variant_values'][$key]['color'])->get();
                        $order['order_details'][$k]['product_details']['variant_values'][$key]['size'] = (empty($this->size_model->fields('id, size')->where('id', $order['order_details'][$k]['product_details']['variant_values'][$key]['size'])->get()))? NULL: $this->size_model->fields('id, size')->where('id',  $order['order_details'][$k]['product_details']['variant_values'][$key]['size'])->get();
                    }
                }
                if(! empty($order['order_details'][$k]['product_details']['images'])){foreach ($order['order_details'][$k]['product_details']['images'] as $key => $image){
                    $order['order_details'][$k]['product_details']['images'][$key]['image'] = base_url().'uploads/product_image/product_'.$image['id'].'.jpg';
                }}else {
                    $order['order_details'][$k]['product_details']['images'] = NULL;
                }
            }}
            $this->set_response_simple((empty($order))? NULL:$order, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    
}
