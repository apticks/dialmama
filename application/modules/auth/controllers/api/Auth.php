<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class Auth extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('otp_model');
        $this->load->model('group_model');
        $this->load->helper('date');
    }

    /**
     *
     * @author Mehar
     *         Login Api
     */
    public function login_post()
    {
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $this->form_validation->set_rules($this->user_model->rules['login']);
        if ($this->form_validation->run() == FALSE) {
            $this->set_response(validation_errors());
        } else {
            $identity = strtoupper(html_escape($this->input->post('identity')));
            $password = html_escape($this->input->post('password'));
            $login_one = $this->ion_auth->login($identity, $password);
            if (! $login_one) {
                $this->ion_auth_model->identity_column = 'unique_id';
                $this->config->set_item('identity', 'unique_id');
                $login_two = $this->ion_auth->login($identity, $password);
                if ($login_two) {
                    $user_data = $this->user_model->fields('username,email,phone')
                        ->where('id', $this->ion_auth->get_user_id())
                        ->get();
                    $timestamp = now();
                    $token = array(
                        "id" => $this->ion_auth->get_user_id(),
                        "userdetail" => $user_data,
                        "time" => $timestamp
                    );
                    $jwt = JWT::encode($token, $this->config->item('jwt_key'));
                    $this->set_response_simple([
                        "token" => $jwt
                    ], 'Login SuccessFully.!', http_response_code(), TRUE);
                } else {
                    $this->set_response_simple($this->ion_auth->errors(), 'Failed', http_response_code(), FALSE);
                }
            } else {
                $user_data = $this->user_model->fields('username,email,phone')
                    ->where('id', $this->ion_auth->get_user_id())
                    ->get();
                $timestamp = now();
                $token = array(
                    "id" => $this->ion_auth->get_user_id(),
                    "userdetail" => $user_data,
                    "time" => $timestamp
                );
                $jwt = JWT::encode($token, $this->config->item('jwt_key'));
                $this->set_response_simple([
                    "token" => $jwt
                ], 'Login SuccessFully.!', http_response_code(), TRUE);
            }
        }
    }
    
    /**
     * @desc Forgot password Recovery
     * @param string emial
     * @author Mehar
     */
    public function forgot_password_post(){
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $this->form_validation->set_rules('identity', 'Identity', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->set_response(validation_errors());
        } else {
            $identity_column = $this->config->item('identity', 'ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();
            if (empty($identity) || $identity == null) {
                if ($this->config->item('identity', 'ion_auth') != 'email') {
                    $this->set_response_simple(NULL, 'Identity not found', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                } else {
                    $this->set_response_simple(NULL, 'Email not found', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                }
            }else{
                $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
                if ($forgotten) {
                    $this->set_response_simple(NULL, $this->ion_auth->messages(), REST_Controller::HTTP_OK, TRUE);
                } else {
                    $this->set_response_simple(NULL, $this->ion_auth->errors(), REST_Controller::HTTP_NO_CONTENT, FALSE);
                }
            }
        }
    }

    public function verify_post()
    {
        $this->set_response_simple([
            "token" => $this->validate_token($this->input->get_request_header('TOKEN'))
        ], 'verify', http_response_code(), TRUE);
    }
    
    public function create_user_post(){
        $group = $this->group_model->where('id', $this->input->post('group_id'))->get();
        if(! empty($group)){
        $unique_id = generate_serial_no($group['code'], 4, $group['last_id']);
        $this->group_model->update([
            'last_id' => $group['last_id'] + 1
        ], $group['id']);
        $email = strtolower($this->input->post('email'));
        $identity = ($this->config->item('identity', 'ion_auth') === 'email') ? $email : $unique_id;
        $additional_data = array(
            'first_name' => $this->input->post('name'),
            'unique_id' => $unique_id,
            'active' => 1
        );
        $group_id[] = $group['id'];
        $user_id = $this->ion_auth->register($identity, (empty($this->input->post('password')))? '1234': $this->input->post('password'),$email, $additional_data, $group_id);
        if($user_id){
            $user_unique_id = $this->user_model->fields('unique_id')->where('id', $user_id)->get();
            $this->set_response_simple($user_unique_id, $this->ion_auth->messages(), REST_Controller::HTTP_OK, TRUE);
        }else{
            $this->set_response_simple($user_id, $this->ion_auth->errors(), REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
        }
        }
    }
    
    public function roles_get(){
        $data = $this->group_model->fields('id, name')->where('status', 1)->get_all();
        $this->set_response_simple($data, "List of roles", REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     * @desc To Generate Otp
     * @author author Mehar
     */
    public function otp_gen_post() {
    	$_POST = json_decode(file_get_contents("php://input"), TRUE);
    	$this->form_validation->set_rules($this->user_model->rules['otp']);
    	if($this->form_validation->run() ==  FALSE){
    		$this->set_response_simple(NULL, explode('.', validation_errors())[0], REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
    	}else{
    		$mobile = $this->input->post('mobile');
    		$check_mobile = $this->user_model->where('phone', $mobile)->as_array()->get();
    		
    		$otp = '';
    		if($check_mobile != FALSE){
    			$otp = rand(154564, 564646);
    			//if($this->Sms_model->send_sms())
    			$user_exist_in_otp = $this->otp_model->where('user_id', $check_mobile['id'])->as_array()->get();
    			
    			if(! empty($user_exist_in_otp)){
    				$this->otp_model->update([
    						'user_id' => $check_mobile['id'],
    						'otp' => $otp,
    						'is_expired' => 0,
    						'updated_at' => date('y-m-d h:i:s')
    				], 'user_id');
    			}else{
    				$this->otp_model->insert(['user_id' => $check_mobile['id'], 'otp' => $otp]);
    			}
    		}else{
    				$last_id = $this->config->item('last_id', 'ion_auth');
    				$unique_id = generate_serial_no($this->config->item('user_id_prefix', 'ion_auth'), 4, $last_id);
    				$this->db->where('id', 1);
    				$this->db->update('user_id',['last_id' => $last_id + 1]);
    				$mobile = strtolower($this->input->post('mobile'));
    				$user_id = $this->user_model->insert(['phone' => $mobile, 'active' => 1, 'unique_id' => $unique_id]);
    				$this->db->insert('users_groups', ['user_id' => $user_id, 'group_id' => 3]);
	    			if(! empty($user_id)){
	    				$otp = rand(154564, 564646);
	    				$this->otp_model->insert(['user_id' => (empty($user_id))? NULL : $user_id, 'otp' => $otp]);
	    			} 
    		}
    		
    		if(! empty($otp)){
    		    $response = $this->send_by_msg91('OTP : '.$otp.' is your Dial Mama verification code. Please do not share it with anyone. Thank Q', $mobile, 1307161768640667069);
    			$this->set_response_simple($otp, 'Otp Generated', REST_Controller::HTTP_OK, TRUE);
    		}else{
    			$this->set_response_simple(NULL, 'Internal server error', REST_Controller::HTTP_CONFLICT, FALSE);
    		}
    		
    	}
    }
    
    public function new_user_post($identity = NULL, $password = NULL, $mobile = NULL, $additional_data = NULL){
    	return $this->ion_auth->register($identity, (empty($password))? '123456': $password, $mobile, $additional_data, [3]);
    }
    
    public function verify_otp_post(){
    	$_POST = json_decode(file_get_contents("php://input"), TRUE);
    	$this->form_validation->set_rules($this->user_model->rules['otp']);
    	$this->form_validation->set_rules('otp', 'OTP', 'required');
    	if($this->form_validation->run() ==  FALSE){
    		$this->set_response_simple(NULL, explode('.', validation_errors())[0], REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
    	}else{
    		$mobile = $this->input->post('mobile');
    		$otp = $this->input->post('otp');
    		$check_mobile = $this->user_model->where('phone',$mobile)->as_array()->get();
    		$is_verified = $this->otp_model->verify_otp($check_mobile, $otp);
    		if(! empty($is_verified)){
    			$this->otp_model->update([
    					'user_id' => $check_mobile['id'],
    					'otp' => $otp,
    					'is_expired' => 1,
    					'updated_at' => date('y-m-d h:i:s')
    			], 'user_id');
    			$timestamp = now();
    			$token = array(
    					"id" => $check_mobile['id'],
    					"userdetail" => $check_mobile,
    					"time" => $timestamp
    			);
    			$jwt = JWT::encode($token, $this->config->item('jwt_key'));
    			$data = ['token' => $jwt];
    			$this->set_response_simple($data, 'verified..!', REST_Controller::HTTP_OK, TRUE);
    		}else{
    			$this->set_response_simple(NULL, 'Invalid otp..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
    		}
    		
    	}
}
}
