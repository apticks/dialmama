<?php
include_once(BASEPATH.'../vendor/autoload.php'); 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Master extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in())
            redirect('auth/login');
        
        $this->load->model('state_model');
        $this->load->model('district_model');
        $this->load->model('brand_model');
        $this->load->model('model_model');
        $this->load->model('warrenty_model');
        $this->load->model('ram_model');
        $this->load->model('storage_model');
        $this->load->model('battery_capacity_model');
        $this->load->model('size_model');
        $this->load->model('colour_model');
        $this->load->library('pagination');
    }

    
    /**
     * States crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function state($type = 'r')
    {
       /*  if (! $this->ion_auth_acl->has_permission('state'))
            redirect('admin'); */
        
        if ($type == 'c') {
            $this->form_validation->set_rules($this->state_model->rules);
            if ($this->form_validation->run() == FALSE) {
               $this->state('r');
            } else {
                $id = $this->state_model->insert([
                    'name' => $this->input->post('name'),
                ]);
                redirect('state/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'States';
            $this->data['content'] = 'master/state';
            $this->data['states'] = $this->state_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->state_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
                $this->state_model->update([
                    'id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                ], 'id','name');
                redirect('state/r', 'refresh');
            }
        } elseif ($type == 'd') {
            $this->state_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit State';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'state';
            $this->data['state'] = $this->state_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }

    /**
     * Districts crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function district($type = 'r')
    {
       /*  if (! $this->ion_auth_acl->has_permission('district'))
            redirect('admin'); */
        
        if ($type == 'c') {
            $this->form_validation->set_rules($this->district_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->district('r');
            } else {
                $id = $this->district_model->insert([
                    'state_id' => $this->input->post('state_id'),
                    'name' => $this->input->post('name'),
                ]);
                redirect('district/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'District';
            $this->data['content'] = 'master/district';
            $this->data['states'] = $this->state_model->get_all();
            $this->data['districts'] = $this->district_model->order_by('id', 'DESC')->limit(10)->get_all();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->district_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
                $this->district_model->update([
                    'id' => $this->input->post('id'),
                    'state_id' => $this->input->post('state_id'),
                    'name' => $this->input->post('name'),
                ], 'id');
                redirect('district/r', 'refresh');
            }
        } elseif ($type == 'd') {
            $this->district_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit District';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'district';
            $this->data['states'] = $this->state_model->get_all();
            $this->data['district'] = $this->district_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    /**
     * Model crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function model($type = 'r', $rowno = 0)
    {
        /*  if (! $this->ion_auth_acl->has_permission('district'))
         redirect('admin'); */
        
        if ($type == 'c') {
            $this->form_validation->set_rules($this->model_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->model('r');
            } else {
            	$id = $this->model_model->insert([
                    'brand_id' => $this->input->post('brand_id'),
                    'name' => $this->input->post('name'),
                ]);
                redirect('model/r/0', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Model';
            $this->data['content'] = 'master/model';
          //  $this->data['brands'] = $this->brand_model->order_by('id', 'DESC')->get_all();
              $search_text = "" ; $brand_id = 0; $noofrows = 10;
            if(isset($_POST['submit'])){
                $search_text = $this->input->post('q');
                $brand_id = $this->input->post('brand_id');
                $noofrows = $this->input->post('noofrows');
                $this->session->set_userdata(array("q" => $search_text,'brand_id' => $brand_id, 'noofrows' => $noofrows));
            }else{
                if(  $this->session->userdata('q') != NULL|| $this->session->userdata('brand_id') != NULL || $this->session->userdata('noofrows') != NULL){
                    $search_text = $this->session->userdata('q');
                    $brand_id = $this->session->userdata('brand_id');
                    $noofrows = $this->session->userdata('noofrows');
                }
            }
            $rowperpage = $noofrows? $noofrows: 10;
            if($rowno != 0){
                $rowno = ($rowno-1) * $rowperpage;
            } 
           //   $users_record = $this->model_model->order_by('id', 'DESC')->limit($rowperpage, $rowno)->get_all();
           $allcount = $this->model_model->users_count($search_text, $brand_id);
           $this->data['models'] = $this->model_model->get_models($rowperpage, $rowno,$search_text,$brand_id);
      //  print_array($this->data['models']);exit();
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='page-item active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tagl_close'] = "</li>";
            $config['base_url'] = base_url().'model/r';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $allcount;
            $config['per_page'] = $rowperpage;
            
            // Initialize
            $this->pagination->initialize($config);
            
            $this->data['pagination'] = $this->pagination->create_links();
           // $this->data['models'] = $models_record;
              foreach ($this->data['models'] as $key => $model){
                $this->data['models'][$key]['brand'] = $this->brand_model->fields('id, name, status')->where('id', $model['brand_id'])->get();
            }
            $this->data['q'] = $search_text;
            $this->data['brand_id'] = $brand_id;
            $this->data['row'] = $rowno;
            $this->data['noofrows'] = $rowperpage;

            $this->data['brands'] = $this->brand_model->order_by('name', 'asc')->where('status', 1)->get_all();
            $this->data['mf_brands'] = $this->brand_model->order_by('name', 'asc')->where('status', 2)->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'u') {
        	$this->form_validation->set_rules($this->model_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->model('edit');
            } else {
            	$this->model_model->update([
                    'id' => $this->input->post('id'),
                    'brand_id' => $this->input->post('brand_id'),
                    'name' => $this->input->post('name'),
                ], 'id');
                redirect('model/r/0', 'refresh');
            }
        } elseif ($type == 'd') {
        	$this->model_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Model';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'model';
            $this->data['brands'] = $this->brand_model->order_by('name', 'asc')->where('status', 1)->get_all();
            $this->data['mf_brands'] = $this->brand_model->order_by('name', 'asc')->where('status', 2)->get_all();
            $this->data['model'] = $this->model_model->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    /**
     * Model crud Excel
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function excel_model($type = 'r', $rowno = 0){
        if ($type == 'r') {
            $this->data['title'] = 'Model';
            $this->data['content'] = 'master/model_excel';
            //  $this->data['brands'] = $this->brand_model->order_by('id', 'DESC')->get_all();
            $search_text = "" ; $brand_id = 0; $brand_type =0; $noofrows = 10;
            if(isset($_POST['submit'])){
                $search_text = $this->input->post('q');
                $brand_id = $this->input->post('brand_id');
                $brand_type = $this->input->post('brand_type');
                $noofrows = $this->input->post('noofrows');
                $this->session->set_userdata(array("q" => $search_text,'brand_id' => $brand_id, 'brand_type' => $brand_type, 'noofrows' => $noofrows));
            }else{
                if(  $this->session->userdata('q') != NULL || $this->session->userdata('brand_id') != NULL || $this->session->userdata('brand_type') != NULL || $this->session->userdata('noofrows') != NULL){
                    $search_text = $this->session->userdata('q');
                    $brand_id = $this->session->userdata('brand_id');
                    $brand_type = $this->session->userdata('brand_type');
                    $noofrows = $this->session->userdata('noofrows');
                }
            }
            $rowperpage = $noofrows? $noofrows: 10;
            if($rowno != 0){
                $rowno = ($rowno-1) * $rowperpage;
            }
            //   $users_record = $this->model_model->order_by('id', 'DESC')->limit($rowperpage, $rowno)->get_all();
            $allcount = $this->model_model->users_count($search_text, $brand_id);
            $this->data['models'] = $this->model_model->get_models($rowperpage, $rowno,$search_text,$brand_id);
            //  print_array($this->data['models']);exit();
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='page-item active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tagl_close'] = "</li>";
            $config['base_url'] = base_url().'admin/master/excel_model/r';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $allcount;
            $config['per_page'] = $rowperpage;
            
            // Initialize
            $this->pagination->initialize($config);
            
            $this->data['pagination'] = $this->pagination->create_links();
            // $this->data['models'] = $models_record;
            foreach ($this->data['models'] as $key => $model){
                $this->data['models'][$key]['brand'] = $this->brand_model->fields('id, name, status')->where('id', $model['brand_id'])->get();
            }
            $this->data['q'] = $search_text;
            $this->data['brand_id'] = $brand_id; 
            $this->data['brand_type'] = $brand_type;
            $this->data['row'] = $rowno;
            $this->data['noofrows'] = $rowperpage;
            
            $this->data['brands'] = $this->brand_model->order_by('name', 'asc')->where('status', $brand_type)->get_all();
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'export'){
            $brand_id = $this->input->post('brand_id');
            $brand_type = $this->input->post('brand_type');
            $brand =  $this->brand_model->where('id', $brand_id)->get();
            if($brand_type == 1)
                $fileName = $brand['name'].'_models_normal_brands_'.time().'.xlsx';
            else
                $fileName = $brand['name'].'_models_manufacturer _brands_'.time().'.xlsx';
            
            $modelsData = $this->model_model->where('brand_id', $brand_id)->order_by('name', 'asc')->get_all();
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'id');
            $sheet->setCellValue('B1', 'brand_id');
            $sheet->setCellValue('C1', 'brand_name');
            $sheet->setCellValue('D1', 'model_name');
            $sheet->setCellValue('E1', 'status');
            $sheet->setCellValue('F1', 'listing_order');
            $rows = 2;
            if(! empty($modelsData)){foreach ($modelsData as $val){
                $sheet->setCellValue('A' . $rows, $val['id']);
                $sheet->setCellValue('B' . $rows, $val['brand_id']);
                $sheet->setCellValue('C' . $rows, $brand['name']);
                $sheet->setCellValue('D' . $rows, $val['name']);
                $sheet->setCellValue('E' . $rows, $val['status']);
                $sheet->setCellValue('F' . $rows, $val['listing_order']);
                $rows++;
            }}else{
                $sheet->setCellValue('A' . $rows, "");
                $sheet->setCellValue('B' . $rows,"");
                $sheet->setCellValue('C' . $rows, "");
                $sheet->setCellValue('D' . $rows, "");
                $sheet->setCellValue('E' . $rows, "");
                $sheet->setCellValue('F' . $rows, "");
            }
            $writer = new Xlsx($spreadsheet);
            if (!file_exists(BASEPATH."../uploads/model_excels/")) {
                mkdir(BASEPATH."../uploads/model_excels/", 0777, true);
            }
            $writer->save(BASEPATH."../uploads/model_excels/".$fileName);
            header("Content-Type: application/vnd.ms-excel");
            redirect(base_url()."/uploads/model_excels/".$fileName);
        }elseif ($type == 'bulk_upload'){
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '0');
            // If file uploaded
            if(!empty($_FILES['models_xlsx']['name'])) {
                // get file extension
                $extension = pathinfo($_FILES['models_xlsx']['name'], PATHINFO_EXTENSION);
                
                if($extension == 'csv'){
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } elseif($extension == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                // file path
                $spreadsheet = $reader->load($_FILES['models_xlsx']['tmp_name']);
                $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                
                // array Count
                $arrayCount = count($allDataInSheet);
                $flag = 0;
                $createArray = array('id', 'brand_id', 'brand_name', 'model_name', 'status', 'listing_order');
                $makeArray = array('id' => 'id', 'brand_id' => 'brand_id', 'brand_name' => 'brand_name', 'model_name' => 'model_name', 'status' => 'status', 'listing_order' => 'listing_order');
                $SheetDataKey = array();
                
                foreach ($allDataInSheet as $dataInSheet) {
                    foreach ($dataInSheet as $key => $value) {
                        if (in_array(trim($value), $createArray)) {
                            $value = preg_replace('/\s+/', '', $value);
                            $SheetDataKey[trim($value)] = $key;
                        }
                    }
                }
                //print_array($SheetDataKey);
                $dataDiff = array_diff_key($makeArray, $SheetDataKey);
                if (empty($dataDiff)) {
                    $flag = 1;
                }
                // match excel sheet column
                if ($flag == 1) { $k = 0;
                for ($i = 2; $i <= $arrayCount; $i++) {
                    
                    $id = $SheetDataKey['id'];
                    $brand_id = $SheetDataKey['brand_id'];
                    $brand_name = $SheetDataKey['brand_name'];
                    $model_name = $SheetDataKey['model_name'];
                    $status = $SheetDataKey['status'];
                    $listing_order = $SheetDataKey['listing_order'];
                    
                    $id = filter_var(trim($allDataInSheet[$i][$id]), FILTER_SANITIZE_STRING);
                    $brand_id = filter_var(trim($allDataInSheet[$i][$brand_id]), FILTER_SANITIZE_STRING);
                    $brand_name = filter_var(trim($allDataInSheet[$i][$brand_name]), FILTER_SANITIZE_STRING);
                    $model_name = filter_var(trim($allDataInSheet[$i][$model_name]), FILTER_SANITIZE_STRING);
                    $status = filter_var(trim($allDataInSheet[$i][$status]), FILTER_SANITIZE_EMAIL);
                    $listing_order = filter_var(trim($allDataInSheet[$i][$listing_order]), FILTER_SANITIZE_STRING);
                    if(! empty($brand_id)  &&  ! empty($model_name)){
                        $data = [
                            'id' => $id,
                            'brand_id' => $brand_id,
                            'name' => $model_name,
                            'status' => $status,
                            'listing_order' => $listing_order
                        ];
                        if(! empty($id)){
                            $db_res = $this->model_model->update($data, 'id');
                        }else{
                            unset($data['id']);
                            $db_res = $this->model_model->insert($data);
                        }
                        $this->session->set_flashdata('upload_status', ["success" =>  "Vendors successfully imported..!"]);
                    }else{
                        $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)"]);
                        $this->data['vendor'] = array('id' => $id, 'brand_id' => $brand_id, 'brand_name' => $brand_name, 'model_name' => $model_name, 'status' => $status, 'listing_order' => $listing_order);
                    }
                }
                } else {
                    $this->session->set_flashdata('upload_status', ["error" => "Please import correct file, did not match excel sheet column"]);
                }
                redirect(base_url()."excel_model/r/0");
            }else{
                echo "Please upload the excel file";
                redirect(base_url()."excel_model/r/0");
            }
        }
    }
    
    /**
     * Warrenty crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function warrenty($type = 'r')
    {
        /*  if (! $this->ion_auth_acl->has_permission('district'))
         redirect('admin'); */
        
        if ($type == 'c') {
        	$this->form_validation->set_rules($this->warrenty_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->warrenty('r');
            } else {
            	$id = $this->warrenty_model->insert([
                    'time' => $this->input->post('duration'),
                ]);
                redirect('warrenty/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Warrenties';
            $this->data['content'] = 'master/warrenty';
            $this->data['warrenties'] = $this->warrenty_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'u') {
        	$this->form_validation->set_rules($this->warrenty_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->warrenty('edit');
            } else {
            	$this->warrenty_model->update([
                    'id' => $this->input->post('id'),
                    'time' => $this->input->post('duration'),
                ], 'id');
                redirect('warrenty/r', 'refresh');
            }
        } elseif ($type == 'd') {
        	$this->warrenty_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Warrenty';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'warrenty';
            $this->data['warrenty'] = $this->warrenty_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    /**
     * Ram crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function ram($type = 'r')
    {
        /*  if (! $this->ion_auth_acl->has_permission('district'))
         redirect('admin'); */
        
        if ($type == 'c') {
        	$this->form_validation->set_rules($this->ram_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->ram('r');
            } else {
            	$id = $this->ram_model->insert([
                    'size' => $this->input->post('size'),
                ]);
                redirect('ram/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Rams';
            $this->data['content'] = 'master/ram';
            $this->data['rams'] = $this->ram_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
        	$this->form_validation->set_rules($this->ram_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->ram('edit');
            } else {
            	$this->ram_model->update([
                    'id' => $this->input->post('id'),
                    'size' => $this->input->post('size'),
                ], 'id');
                redirect('ram/r', 'refresh');
            }
        } elseif ($type == 'd') {
        	$this->ram_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Ram';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'ram';
            $this->data['ram'] = $this->ram_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    /**
     * Storage crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function storage($type = 'r')
    {
        /*  if (! $this->ion_auth_acl->has_permission('district'))
         redirect('admin'); */
        
        if ($type == 'c') {
        	$this->form_validation->set_rules($this->storage_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->storage('r');
            } else {
            	$id = $this->storage_model->insert([
                    'size' => $this->input->post('size'),
                ]);
                redirect('storage/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Storages';
            $this->data['content'] = 'master/storage';
            $this->data['storages'] = $this->storage_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
        	$this->form_validation->set_rules($this->storage_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
            	$this->storage_model->update([
                    'id' => $this->input->post('id'),
                    'size' => $this->input->post('size'),
                ], 'id');
                redirect('storage/r', 'refresh');
            }
        } elseif ($type == 'd') {
        	$this->storage_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Storage';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'storage';
            $this->data['storage'] = $this->storage_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    /**
     * Colors crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function colour($type = 'r')
    {
        /*  if (! $this->ion_auth_acl->has_permission('district'))
         redirect('admin'); */
        
        if ($type == 'c') {
        	$this->form_validation->set_rules($this->colour_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->colour('r');
            } else {
            	$id = $this->colour_model->insert([
                    'color_code' => $this->input->post('color_code'),
                    'name' => $this->input->post('name'),
                ]);
                redirect('colour/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Colours';
            $this->data['content'] = 'master/colour';
            $this->data['colours'] = $this->colour_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'u') {
        	$this->form_validation->set_rules($this->colour_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->colour('edit');
            } else {
            	$this->colour_model->update([
                    'id' => $this->input->post('id'),
                    'color_code' => $this->input->post('color_code'),
                    'name' => $this->input->post('name'),
                ], 'id');
                redirect('colour/r', 'refresh');
            }
        } elseif ($type == 'd') {
        	$this->colour_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Colour';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'colour';
            $this->data['colour'] = $this->colour_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    /**
     * Batteries crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function batteries($type = 'r')
    {
        /*  if (! $this->ion_auth_acl->has_permission('district'))
         redirect('admin'); */
        
        if ($type == 'c') {
        	$this->form_validation->set_rules($this->battery_capacity_model->rules);
            if ($this->form_validation->run() == FALSE) {
            	$this->batteries('r');
            } else {
            	$id = $this->battery_capacity_model->insert([
                    'capacity' => $this->input->post('capacity')
                ]);
                redirect('batteries/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Battery Capacities';
            $this->data['content'] = 'master/battery';
            $this->data['batteries'] = $this->battery_capacity_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'u') {
        	$this->form_validation->set_rules($this->battery_capacity_model->rules);
            if ($this->form_validation->run() == FALSE) {
            	$this->batbatteries('edit');
            } else {
            	$this->battery_capacity_model->update([
                    'id' => $this->input->post('id'),
                    'capacity' => $this->input->post('capacity'),
                ], 'id');
                redirect('batteries/r', 'refresh');
            }
        } elseif ($type == 'd') {
        	$this->battery_capacity_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit District';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'batteries';
            $this->data['battery'] = $this->battery_capacity_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    /**
     * Sizes crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function size($type = 'r')
    {
        /*  if (! $this->ion_auth_acl->has_permission('district'))
         redirect('admin'); */
        
        if ($type == 'c') {
        	$this->form_validation->set_rules($this->size_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->size('r');
            } else {
            	$id = $this->size_model->insert([
                    'size' => $this->input->post('size'),
                ]);
                redirect('size/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Sizes';
            $this->data['content'] = 'master/size';
            $this->data['sizes'] = $this->size_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
        	$this->form_validation->set_rules($this->size_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->size('edit');
            } else {
            	$this->size_model->update([
                    'id' => $this->input->post('id'),
                    'size' => $this->input->post('size'),
                ], 'id');
                redirect('size/r', 'refresh');
            }
        } elseif ($type == 'd') {
        	$this->size_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Size';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'size';
            $this->data['size'] = $this->size_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
  
    
}

