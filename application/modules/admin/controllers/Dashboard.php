<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Dashboard extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        
        if (! $this->ion_auth->logged_in())
            redirect('auth/login');
        
        $this->load->model('state_model');
        $this->load->model('otp_model');
        $this->load->model('district_model');
        $this->load->model('delivery_partner_address_model');
    }

    public function index()
    {
        $this->data['users_groups']           =   $this->ion_auth->get_users_groups()->result();
        $this->data['users_permissions']      =   $this->ion_auth_acl->build_acl();
        $this->data['otp'] = $this->otp_model->get(1)['otp'];
        $this->data['title'] = 'Dashboard';
        $this->data['content'] = 'admin/dashboard';
        $this->_render_page($this->template, $this->data);
    }
    
    public function  meeseva_addresses($type = 'r'){
        if($type == 'r'){
            $this->data['title'] = 'Meeseav Addresses list';
            $this->data['content'] = 'admin/meeseva/meeseva_addresses_list';
            $this->data['meeseva_address'] = $this->delivery_partner_address_model->get_all();
            $this->_render_page($this->template, $this->data); 
        }elseif ($type == 'c'){
            $this->form_validation->set_rules($this->delivery_partner_address_model->rules['create']);
            if ($this->form_validation->run() == FALSE) {
                $this->data['title'] = 'Meeseav Addresses list';
                $this->data['content'] = 'admin/meeseva/add_meeseva_address';
                $this->_render_page($this->template, $this->data); 
            } else {
                $is_existed = $this->delivery_partner_address_model->where('pincode', $this->input->post('pincode'))->get();
                if(! $is_existed){
                    $this->delivery_partner_address_model->insert([
                        'owner_name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'pincode' => $this->input->post('pincode'),
                        'address' => $this->input->post('address'),
                        'landmark' => $this->input->post('landmark'),
                        'state' => $this->input->post('state'),
                        'district' => $this->input->post('district'),
                        'created_user_id' => $this->ion_auth->get_user_id(),
                        'city' => $this->input->post('city'),
                    ]);
                }
                redirect('meeseva_addresses/r', 'refresh');
            }
        }elseif ($type == 'u'){
            $this->form_validation->set_rules($this->delivery_partner_address_model->rules['create']);
            if ($this->form_validation->run() == FALSE) {
                $this->data['title'] = 'Meeseav Addresses list';
                $this->data['content'] = 'admin/meeseva/update_meeseva_address';
                $this->data['meeseva_address'] = $this->delivery_partner_address_model->get($this->input->post('id'));
                $this->_render_page($this->template, $this->data);
            } else {
                $this->delivery_partner_address_model->update([
                    'id' => $this->input->post('id'),
                    'owner_name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'pincode' => $this->input->post('pincode'),
                    'address' => $this->input->post('address'),
                    'landmark' => $this->input->post('landmark'),
                    'state' => $this->input->post('state'),
                    'district' => $this->input->post('district'),
                    'updated_user_id' => $this->ion_auth->get_user_id(),
                    'city' => $this->input->post('city'),
                ], 'id');
                redirect('meeseva_addresses/r', 'refresh');
            }
        }elseif ($type == 'd'){
            $this->delivery_partner_address_model->delete(['id' => $this->input->post('id')]);
        }
        
    }
}
?>