<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

/**
 *
 * @author Mehar
 *         Admin module
 */
class Operations extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');
		
            $this->load->model('order_model');
	        $this->load->model('order_details_model');
	        $this->load->model('user_model');
	        $this->load->model('wallet_transaction_model');
	        $this->load->model('coupon_model');
	        $this->load->model('category_model');
	        $this->load->model('product_variant_value_model');
	        $this->load->model('colour_model');
	        $this->load->model('model_model');
	        $this->load->model('brand_model');
	        $this->load->model('sub_category_model');
	        $this->load->model('category_model');
	        $this->load->model('ram_model');
	        $this->load->model('storage_model');
	        $this->load->model('compatible_for_model');
	        $this->load->model('delivery_partner_address_model');
    }

    public function orders($type = 'r'){
    	if($type == 'r'){
    		$this->data['title'] = 'orders';
    		$this->data['content'] = 'admin/operations/orders_list';
    		if(isset( $_POST['upload']) ){
    			$this->data['orders'] = $this->order_model->all($_POST['start_date'],(isset($_POST['end_date']))? $_POST['end_date']:NULL, (isset($_POST['status']))? $_POST['status']:NULL);
    			foreach($this->data['orders'] as $key => $val){
    				$this->data['orders'][$key]['coupon'] = $this->db->query("SELECT * FROM `order_coupons` INNER JOIN coupons ON coupons.id = order_coupons.coupon_id WHERE order_coupons.order_id = ".$val['id']." AND coupons.owner != 'DIALMAMA'")->result_array();
    			}
    			$this->session->set_flashdata('order_filters', ['start_date' => $_POST['start_date'], 'end_date' => (isset($_POST['end_date']))? $_POST['end_date']:NULL, 'status' => (isset($_POST['status']))? $_POST['status']:NULL]);
    		}else{
    			$this->data['orders'] = $this->order_model->order_by('id', 'DESC')->get_all();
    			foreach($this->data['orders'] as $key => $val){
    				$this->data['orders'][$key]['coupon'] = $this->db->query("SELECT * FROM `order_coupons` INNER JOIN coupons ON coupons.id = order_coupons.coupon_id WHERE order_coupons.order_id = ".$val['id']." AND coupons.owner != 'DIALMAMA'")->result_array();
    			}
    		}
    		$this->_render_page($this->template, $this->data);
    	}elseif ($type == 'info'){
    		$this->data['title'] = 'orders info';
    		$this->data['content'] = 'admin/operations/orders_info';
    		$this->data['order'] = $this->order_model->with_meeseva_address('fields: id, channel_id, channel_name, owner_name, email, phone, address, landmark, state, district, city, pincode, created_user_id, updated_user_id, created_at, updated_at, deleted_at, status')->with_address('fields: user_id, phone, email, name, address, pincode, status')->where('id', $_GET['id'])->get();
    		$this->data['order_details'] = $this->order_details_model->order_by('id', 'DESC')->with_product('fields:id, custom_product_id, product_code, name, desc, ram, storage, condition, compatible_for, cat_id, sub_cat_id, brand_id, status')->with_vendor('fields:id, unique_id, email, phone, first_name')->with_variant('fields:id, sku, price, qty, moq')->where('order_id', $_GET['id'])->get_all();
    		foreach($this->data['order_details'] as $key => $val){
    		    $this->data['order_details'][$key]['variant_values'] = $this->product_variant_value_model->where("variant_id ", $val['variant']['id'])->get();
    		}
    		foreach($this->data['order_details'] as $key => $val){
    		    if(! empty($val['product']['cat_id'])){
    		        $category = $this->category_model->where('id', $val['product']['cat_id'])->get();
    		        $this->data['order_details'][$key]['product']['category'] = empty($category)? 'NA' : $category['name'];
    		    }else{
    		        $this->data['order_details'][$key]['product']['category'] = 'NA';
    		    }
    		    
    		    if(! empty($val['product']['sub_cat_id'])){
    		        $subcategory = $this->sub_category_model->where('id', $val['product']['sub_cat_id'])->get();
    		        $this->data['order_details'][$key]['product']['sub_category'] = empty($subcategory)? 'NA' :  $subcategory['name'];
    		    }else {
    		        $this->data['order_details'][$key]['product']['sub_category'] = 'NA';
    		    }
    			
    			
    		    if(! empty($val['product']['brand_id'])){
    			    $mfg_brand = $this->brand_model->where('id', $val['product']['brand_id'])->get();
    			    $this->data['order_details'][$key]['product']['mfg_brand'] = empty($mfg_brand)? 'NA' : $mfg_brand['name'];
    			}else {
    			    $this->data['order_details'][$key]['product']['mfg_brand'] = 'NA';
    			}
    			
    			
    			if(! empty($val['product']['ram'])){
    			    $ram = $this->ram_model->where('id', $val['product']['ram'])->get();
    			    $this->data['order_details'][$key]['product']['ram'] = empty($ram)? 'NA' : $ram['size'];
    			}else {
    			    $this->data['order_details'][$key]['product']['ram'] = 'NA';
    			}
    			
    			
    			if(! empty($val['product']['storage'])){
    			    $storage = $this->storage_model->where('id', $val['product']['storage'])->get();
    			    $this->data['order_details'][$key]['product']['storage'] = empty($storage)? 'NA' : $storage['size'];
    			}else {
    			    $this->data['order_details'][$key]['product']['storage'] = 'NA';
    			}
    			
    			
    			if(! empty($val['product']['compatible_for'])){
    			    $compatibility = $this->compatible_for_model->where('id', $val['product']['compatible_for'])->get();
    			    $this->data['order_details'][$key]['product']['compatibility'] = empty($compatibility)? 'NA' : $compatibility['name'];
    			}else {
    			    $this->data['order_details'][$key]['product']['compatibility'] = 'NA';
    			}
    			
    			
    		}
    		foreach($this->data['order_details'] as $key => $val){
    		    if(! empty($this->data['order_details'][$key]['variant_values']['color'])){
    		        $color = $this->colour_model->where('id', $this->data['order_details'][$key]['variant_values']['color'])->get();
    		        $this->data['order_details'][$key]['variant_values']['color'] = empty($color)? 'NA' : $color['name'];
    		    }else {
    		        $this->data['order_details'][$key]['variant_values']['color'] = 'NA';
    		    }
    		    
    		    
    		    if(! empty($this->data['order_details'][$key]['variant_values']['model'])){
    		        $model = $this->model_model->where('id', $this->data['order_details'][$key]['variant_values']['model'])->get();
    		        $this->data['order_details'][$key]['variant_values']['model'] = empty($model)? 'NA' : $model['name'];
    		    }else {
    		        $this->data['order_details'][$key]['variant_values']['model'] = 'NA';
    		    }
    			
    			
    		    if(! empty($model['brand_id'])){
    			    $brand = $this->brand_model->where('id', $model['brand_id'])->get();
    			    $this->data['order_details'][$key]['variant_values']['brand'] = empty($brand)? 'NA' : $brand['name'];
    			}else {
    			    $this->data['order_details'][$key]['variant_values']['brand'] = 'NA';
    			}
    			
    		}
    		$this->_render_page($this->template, $this->data);
    	}elseif ($type == 'tracking_info_update'){
    		$this->order_model->update([
    			'id' => $this->input->post('id'),
    			'track_id' => $this->input->post('track_id'),
    			'status' => $this->input->post('status'),
    		], 'id');
    		redirect('orders/info?id='.$this->input->post('id'));
    	}elseif ($type == 'order_details'){
    	    $this->data['title'] = 'orders details';
    	    $this->data['content'] = 'admin/operations/order_details';
    	    $this->data['order'] = $this->order_model
    	    ->with_address('fields: user_id, phone, email, name, address, pincode, status')
    	    ->with_meeseva_address('fields: id, channel_id, channel_name, owner_name, email, phone, address, landmark, state, district, city, pincode, created_user_id, updated_user_id, created_at, updated_at, deleted_at, status')
    	    ->where('id', $_GET['id'])->get();
    	    $this->data['order_details'] = $this->order_details_model->order_by('id', 'DESC')
    	    ->with_product('fields:id, custom_product_id, product_code, name, desc, ram, storage, condition, compatible_for, cat_id, sub_cat_id, brand_id, status')
    	    ->with_vendor('fields:id, unique_id, email, phone, first_name')
    	    ->with_variant('fields:id, sku, price, qty, moq')
    	    ->where('order_id', $_GET['id'])->get_all();
    	    //print_array($this->data['order']);
    	    foreach($this->data['order_details'] as $key => $val){
    	        $this->data['order_details'][$key]['variant_values'] = $this->product_variant_value_model->where("variant_id ", $val['variant']['id'])->get();
    	    }
    	    foreach($this->data['order_details'] as $key => $val){
    	        if(! empty($val['product']['cat_id'])){
    	            $category = $this->category_model->where('id', $val['product']['cat_id'])->get();
    	            $this->data['order_details'][$key]['product']['category'] = empty($category)? 'NA' : $category['name'];
    	        }else{
    	            $this->data['order_details'][$key]['product']['category'] = 'NA';
    	        }
    	        
    	        if(! empty($val['product']['sub_cat_id'])){
    	            $subcategory = $this->sub_category_model->where('id', $val['product']['sub_cat_id'])->get();
    	            $this->data['order_details'][$key]['product']['sub_category'] = empty($subcategory)? 'NA' :  $subcategory['name'];
    	        }else {
    	            $this->data['order_details'][$key]['product']['sub_category'] = 'NA';
    	        }
    	        
    	        
    	        if(! empty($val['product']['brand_id'])){
    	            $mfg_brand = $this->brand_model->where('id', $val['product']['brand_id'])->get();
    	            $this->data['order_details'][$key]['product']['mfg_brand'] = empty($mfg_brand)? 'NA' : $mfg_brand['name'];
    	        }else {
    	            $this->data['order_details'][$key]['product']['mfg_brand'] = 'NA';
    	        }
    	        
    	        
    	        if(! empty($val['product']['ram'])){
    	            $ram = $this->ram_model->where('id', $val['product']['ram'])->get();
    	            $this->data['order_details'][$key]['product']['ram'] = empty($ram)? 'NA' : $ram['size'];
    	        }else {
    	            $this->data['order_details'][$key]['product']['ram'] = 'NA';
    	        }
    	        
    	        
    	        if(! empty($val['product']['storage'])){
    	            $storage = $this->storage_model->where('id', $val['product']['storage'])->get();
    	            $this->data['order_details'][$key]['product']['storage'] = empty($storage)? 'NA' : $storage['size'];
    	        }else {
    	            $this->data['order_details'][$key]['product']['storage'] = 'NA';
    	        }
    	        
    	        
    	        if(! empty($val['product']['compatible_for'])){
    	            $compatibility = $this->compatible_for_model->where('id', $val['product']['compatible_for'])->get();
    	            $this->data['order_details'][$key]['product']['compatibility'] = empty($compatibility)? 'NA' : $compatibility['name'];
    	        }else {
    	            $this->data['order_details'][$key]['product']['compatibility'] = 'NA';
    	        }
    	        
    	        
    	    }
    	    foreach($this->data['order_details'] as $key => $val){
    	        if(! empty($this->data['order_details'][$key]['variant_values']['color'])){
    	            $color = $this->colour_model->where('id', $this->data['order_details'][$key]['variant_values']['color'])->get();
    	            $this->data['order_details'][$key]['variant_values']['color'] = empty($color)? 'NA' : $color['name'];
    	        }else {
    	            $this->data['order_details'][$key]['variant_values']['color'] = 'NA';
    	        }
    	        
    	        
    	        if(! empty($this->data['order_details'][$key]['variant_values']['model'])){
    	            $model = $this->model_model->where('id', $this->data['order_details'][$key]['variant_values']['model'])->get();
    	            $this->data['order_details'][$key]['variant_values']['model'] = empty($model)? 'NA' : $model['name'];
    	        }else {
    	            $this->data['order_details'][$key]['variant_values']['model'] = 'NA';
    	        }
    	        
    	        
    	        if(! empty($model['brand_id'])){
    	            $brand = $this->brand_model->where('id', $model['brand_id'])->get();
    	            $this->data['order_details'][$key]['variant_values']['brand'] = empty($brand)? 'NA' : $brand['name'];
    	        }else {
    	            $this->data['order_details'][$key]['variant_values']['brand'] = 'NA';
    	        }
    	        
    	    }
    	    $this->_render_page($this->template, $this->data);
    	}elseif ($type == 'change_order_item_status'){
    		$is_checked = $this->input->post('is_checked');
    		$user = $this->user_model->where('id', $this->input->post('vendor_id'))->get();
    		if($is_checked == 'true'){
    			$this->order_details_model->update([
    					'id' => $this->input->post('order_item_id'),
    					'status' => 3
    			], 'id');
    			$changed_amount = $user['wallet'] + floatval($this->input->post('amount'));
    			$this->wallet_transaction_model->insert([
    				'type' => 'credit',
    				'user_id' => $this->input->post('vendor_id'),
    				'cash' => $this->input->post('amount'),
    				'txn_id' => time() . mt_rand(),
    				'order_no' => $this->input->post('order_no'),
    				'order_item_code' => $this->input->post('order_item_code'),
    				'order_item_id' => $this->input->post('order_item_id'),
    				'status' => 0
    			]);
    			$this->user_model->update([
    				'id' => $this->input->post('vendor_id'),
    				'wallet' => $changed_amount
    			], 'id');
    		}elseif ($is_checked != 'true'){
    			$this->order_details_model->update([
    					'id' => $this->input->post('order_item_id'),
    					'status' => 2
    			], 'id');
    			$this->db->where('order_item_id', $this->input->post('order_item_id'));
    			$this->db->delete('wallet_transactions');
    			$changed_amount = $user['wallet'] - floatval($this->input->post('amount'));
    			$this->user_model->update([
    					'id' => $this->input->post('vendor_id'),
    					'wallet' => $changed_amount
    			], 'id');
    		}
    		echo $user['wallet'];
    	}
    }
    public function coupons($type = 'r'){
    	if($type == 'c'){
    		$this->form_validation->set_rules($this->coupon_model->rules);
    		if ($this->form_validation->run() == FALSE) {
    			$this->data['title'] = 'News Add';
    			$this->data['content'] = 'admin/operations/add_coupon';
    			$this->data['categories'] = $this->category_model->order_by('id', 'DESC')->where('status', 1)->get_all();
    			$this->_render_page($this->template, $this->data);
    		} else {
				$this->coupon_model->insert([
						'title' => $this->input->post('title'),
						'owner' => (empty($this->input->post('owner')))? "DIALMAMA":$this->input->post('owner'),
						'code' => $this->input->post('code'),
						'cat_id' => (empty($this->input->post('cat_id')))? NULL:$this->input->post('cat_id'),
				        'amount' => (empty($this->input->post('amount')))? 0:$this->input->post('amount'),
				        'per_cent' => (empty($this->input->post('per_cent')))? 0:$this->input->post('per_cent'),
						'validity' => (empty($this->input->post('validity')))? NULL:$this->input->post('validity'),
				]);   
				redirect('coupons/r', 'refresh');
    		}
    	}elseif ($type == 'r'){
    		$this->data['title'] = 'Coupon';
    		$this->data['content'] = 'admin/operations/coupon_list';
    		$this->data['coupons'] = $this->coupon_model->order_by('id', 'DESC')->get_all();
    		$this->data['categories'] = $this->category_model->order_by('id', 'DESC')->where('status', 1)->get_all();
    		$this->_render_page($this->template, $this->data);
    	}elseif($type == 'u'){
    		$this->coupon_model->update([
    				'id' => $this->input->post('id'),
    				'title' => $this->input->post('title'),
    				'owner' => (empty($this->input->post('owner')))? "DIALMAMA":$this->input->post('owner'),
    				'code' => $this->input->post('code'),
    				'cat_id' => (empty($this->input->post('cat_id')))? NULL:$this->input->post('cat_id'),
    		        'per_cent' => (empty($this->input->post('per_cent')))? 0:$this->input->post('per_cent'),
    				'amount' => $this->input->post('amount'),
    				'validity' => (empty($this->input->post('validity')))? NULL:$this->input->post('validity'),
    		], 'id');
    		redirect('coupons/r', 'refresh');
    	}elseif ($type == 'd'){
    		$this->coupon_model->delete(['id' => $this->input->post('id')]);
    	}elseif ($type == 'edit'){
    		$this->data['title'] = 'Edit Coupon';
    		$this->data['content'] = 'admin/operations/edit';
    		$this->data['type'] = 'state';
    		$this->data['coupon'] = $this->coupon_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
    		$this->data['categories'] = $this->category_model->order_by('id', 'DESC')->where('status', 1)->get_all();
    		$this->_render_page($this->template, $this->data);
    	}
    }
}