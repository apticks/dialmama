<!--Add Category And its list-->
<div id="printableArea">
	<div class="card">
		<div class="card-header">
			<h4>Order Details</h4>
			<input type="button" onclick="printDiv('printableArea')" value="Print" />
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-4">
					<p><span>Date: </span> <b> <?php echo date('d-M-Y', strtotime($order['created_at']));?></b></p>
					<p><span>Order ID: </span> <b><?php echo $order['order_no'];?></b></p>
				</div>
				<div class="col-4"></div>
				<div class="col-4">
					<p><span>Payment mode: </span> 
    					<b><?php if($order['payment_method_id'] == 1){
    						echo 'COD';
    					}elseif ($order['payment_method_id'] == 2){
    						echo 'PAYU MONEY';
    					}else{
    						echo 'INSTAMOJO';
    					}?></b>
					</p>
				</div>
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-bordered" 
					style="width: 100%;">
					<thead>
						<tr>
							<th>Product Id</th>
							<th>Name</th>
							<th>Category</th>
							<th>MFG Brand</th>
							<th>Brand</th>
							<th>Model</th>
							<th>Color</th>
							<th>Price</th>
							<th>QTY</th>
							<th>Total</th>
							<th>Discount</th>
							<th>Net Amount</th>
							<th>Vendor Id</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php if(! empty($order_details)):?>
						<?php
						$product_ids = [];
						foreach ($order_details as $ord):
						$rowspan = array_count_values(array_column(array_column($order_details, 'product'), 'product_code'));
						  ?>
							<tr>
								<?php if(! in_array($ord['product']['product_code'], $product_ids)):?>
    								<td rowspan="<?php echo $rowspan[$ord['product']['product_code']];?>">
    									<a href="<?php echo base_url()?>ecom_product/edit?id=<?php echo (! empty($ord['product']['id']))? $ord['product']['id']: '';?>"><?php echo (! empty($ord['product']['product_code']))? $ord['product']['product_code']: '';?></a>
    								</td>
								<?php endif;?>
								<?php if(! in_array($ord['product']['product_code'], $product_ids)):?>
									<td rowspan="<?php echo $rowspan[$ord['product']['product_code']];?>"><?php echo (! empty($ord['product']['name']) && ! in_array($ord['product']['product_code'], $product_ids))? $ord['product']['name']: '';?></td>
								<?php endif;?>
								<?php if(! in_array($ord['product']['product_code'], $product_ids)):?>
									<td rowspan="<?php echo $rowspan[$ord['product']['product_code']];?>"><?php echo $ord['product']['category'];?></td>
								<?php endif;?>
								<td><?php echo $ord['product']['mfg_brand'];?></td>
								<td><?php echo $ord['variant_values']['brand'];?></td>
								<td><?php echo $ord['variant_values']['model'];?></td>
								<td><?php echo $ord['variant_values']['color'];?></td>
								<td><?php echo $ord['price'];?></td>
								<td><?php echo $ord['qty'];?></td>
								<td><?php echo floatval($ord['price']) * intval($ord['qty']);?></td>
								<td><?php echo $ord['discount'];?></td>
								<td><?php echo (floatval($ord['price']) * intval($ord['qty'])) - floatval($ord['discount']);?></td>
								<td><?php echo (! empty($ord['product']['custom_product_id']))? $ord['product']['custom_product_id']: '';?></td>
								<td><b><?php 
								if($ord['status'] == 1){
									echo "Received";
								}elseif ($ord['status'] == 2){
									echo "Accepted";
								}elseif ($ord['status'] == 0){
									echo "Cancelled";
								}elseif ($ord['status'] == 3){
									echo "Arrived at hub";
								}else {
								    echo "NA";
								}
								?></b>
								</td>
								<td>
									<input type="checkbox" <?php echo ($ord['status'] == 3) ? 'checked' : '';?> order_item_id="<?php echo $ord['id']?>" amount="<?php echo $ord['price']?>"  vendor_id="<?php echo $ord['vendor']['id']?>"  order_no="<?php echo $order['order_no']?>" order_item_code="<?php echo $ord['order_item_code'];?>" class="change_order_item_status" data-on="Arrivied" data-off="Waiting" data-toggle="toggle" data-onstyle="success" data-offstyle="primary"> 
								</td>
							</tr>
						<?php
						if(! in_array($ord['product']['product_code'], $product_ids)){
						    array_push($product_ids, $ord['product']['product_code']);
						}
						endforeach;?>
					<?php else :?>
					<tr>
							<th colspan='8'><h3>
									<center>No Orders</center>
								</h3></th>
						</tr>
					<?php endif;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<div class="card">
	<div class="card-header">
		<h4>Shipping details</h4>
	</div>
	<div class="card-body">
    	<div class="row justify-content-center">
    		<?php if($order['payment_method_id'] == 1  && ! empty($order['meeseva_address'])):?>
    			<div class="col-6 admin-address">
                      <div class="container" style="border-bottom:1px solid black">
                        <h4>Customer</h4>
                      </div>
                        <hr>
                      <ul class="container admin-address_details">
                        <li><p><span class="glyphicon glyphicon-envelope one" data-feather="user" style="width:50px;">Name</span><?php echo empty($order['customer_name'])? 'NA': $order['customer_name'];?></p></li>
                        <li><p><span class="glyphicon glyphicon-earphone one" data-feather="phone" style="width:50px;">Mobile</span><?php echo empty($order['mobile_number'])? 'NA': $order['mobile_number'];?></p></li>
                      </ul>
    			</div>
    			<div class="col-6 admin-address">
                      <div class="container" style="border-bottom:1px solid black">
                        <h4><?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['channel_name'];?></h4>
                      </div>
                        <hr>
                      <ul class="container admin-address_details">
                        <li><p><b>Mobile   :</b>+91 <?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['phone'];?></p></li>
                        <li><p><b>Address  :</b><?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['address'];?></p></li>
                        <li><p><b>Landmark :</b><?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['landmark'];?></p>
                        <li><p><b>City     :</b><?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['city'];?></p>
                        <li><p><b>District :</b><?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['district'];?></p>
                        <li><p><b>State    :</b><?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['state'];?></p>
                        <li><p><b>Pincode  :</b><?php echo empty($order['meeseva_address'])? 'NA': $order['meeseva_address']['pincode'];?></p>
                      </ul>
    			</div>
    		<?php endif;?>
    		<?php if($order['payment_method_id'] == 2  && ! empty($order['address'])):?>
    			<div class="col-6 admin-address">
                      <div class="container" style="border-bottom:1px solid black">
                        <h2><?php echo empty($order['address'])? 'NA': $order['address']['name'];?></h2>
                      </div>
                        <hr>
                      <ul class="container admin-address_details">
                        <li><p><span class="glyphicon glyphicon-earphone one" data-feather="smartphone" style="width:50px;">Mobile</span>+91 <?php echo empty($order['address'])? 'NA': $order['address']['phone'];?></p></li>
                        <li><p><span class="glyphicon glyphicon-envelope one" data-feather="mail" style="width:50px;">Email</span><?php echo empty($order['address'])? 'NA': $order['address']['email'];?></p></li>
                        <li><p><span class="glyphicon glyphicon-map-marker one" data-feather="home" style="width:50px;">Address</span><?php echo empty($order['address'])? 'NA': $order['address']['address'];?></p></li>
                        <li><p><span class="glyphicon glyphicon-new-window one" data-feather="map-pin" style="width:50px;">Pincode</span><?php echo empty($order['address'])? 'NA': $order['address']['pincode'];?></p>
                      </ul>
    			</div>
    		<?php endif;?>
    	</div>
    	<div class="container">
        	<form  novalidate="" action="<?php echo base_url('orders/tracking_info_update');?>" method="post" enctype="multipart/form-data">
        		<div class="row">
        			<div class="form-group col-5">
        			<label>Order tracking id</label>
        			 <input type="text" name="track_id" required="" value="<?php echo $order['track_id'];?>" class="form-control" placeholder="Track id">
        			 <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
        		</div>
        		<div class="form-group col-5">
        			<label>Order status</label> 
        			<select class="form-control" name="status">
        				<option value="null" selected disabled>--select--</option>
        				<option value="0" <?php echo ( $order['status'] == 0 )? 'selected': '';?>>Cancelled</option>
        				<option value="1" <?php echo ( $order['status'] == 1 )? 'selected': '';?>>Placed</option>
        				<option value="2" <?php echo ( $order['status'] == 2 )? 'selected': '';?>>Shipped</option>
        				<option value="3" <?php echo ( $order['status'] == 3 )? 'selected': '';?>>Delivered</option>
        			</select>
        		</div>
        		
        		<div class="form-group col-2">
        			<button type="submit" name="upload" id="upload" value="Apply"
        				class="btn btn-primary mt-27 ">Submit</button>
        		</div>
        		</div>
        	</form>
    	</div>
	</div>
</div>
</div>
<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
	