<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="col-10">List of Coupons</h4>
					<a class="btn btn-outline-dark btn-lg col-2" href="<?php echo base_url('coupons/c')?>"><i class="fa fa-plus" aria-hidden="true"></i> Add Coupon</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Title</th>
									<th>Coupon</th>
									<th>Owner</th>
									<th>Pincode</th>
									<th>Mobile</th>
									<th>Category</th>
									<th>Amount</th>
									<th>Discount (in %)</th>
									<th>Validity</th>
									<th>Created At</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($coupons)):?>
    							<?php  $sno = 1; foreach ($coupons as $coupon): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $coupon['title'];?></td>
									<td><?php echo $coupon['code'];?></td>
									<td><?php echo $coupon['owner'];?></td>
									<td><?php echo $coupon['pincode'];?></td>
									<td><?php echo $coupon['mobile'];?></td>
									<td><?php foreach ($categories as $category):?>
    									<?php echo ($category['id'] == $coupon['cat_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
									<td><?php echo $coupon['amount'];?></td>
									<td><?php echo $coupon['per_cent'];?></td>
									<td><?php echo $coupon['validity'];?></td>
									<td><?php echo $coupon['created_at'];?></td>
									<td><a
										href="<?php echo base_url()?>coupons/edit?id=<?php echo $coupon['id']; ?>"
										class=" mr-2  " type="category"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $coupon['id'] ?>, 'coupons/d')">
											<i class="far fa-trash-alt"></i>
									</a></td>
								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='9'><h3>
											<center>Coupons Not Available</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>