<div class="card">
	<div class="card-header">Add Coupon</div>
	<div class="card-body">
		<div class="row">
        <div class="col col-sm col-md" >
        	<form action="<?php echo base_url('coupons/c')?>" method="post" >
                  <div class="form-group row">
                    <label for="title" class="col-4 col-form-label">Coupon Title</label> 
                    <div class="col-8">
                      <input id="title" name="title" placeholder="Coupon Title" type="text" class="form-control" >
                      <?php echo form_error('title', '<div style="color:red">', '</div>');?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="title" class="col-4 col-form-label">Coupon Owner</label> 
                    <div class="col-8">
                      <input id="title" name="owner" placeholder="Coupon Owner" type="text" class="form-control" >
                      <?php echo form_error('owner', '<div style="color:red">', '</div>');?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="category" class="col-4 col-form-label">News Category</label> 
                    <div class="col-8">
                      <select id="category" name="cat_id" class="custom-select" >
                        <option value="0" selected>--Select--</option>
                        <?php foreach ($categories as $category){?>
                        	<option value="<?php echo $category['id']?>"><?php echo $category['name']?></option>
                        <?php }?>
                      </select>
                      <?php echo form_error('cat_id', '<div style="color:red">', '</div>');?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="date" class="col-4 col-form-label">Validity</label> 
                    <div class="col-8">
                      <input id="news-date" name="validity" placeholder="yyyy-mm-dd" type="text" class="form-control" >
                      <?php echo form_error('validity', '<div style="color:red">', '</div>');?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="title" class="col-4 col-form-label">Code</label> 
                    <div class="col-8">
                      <input id="title" name="code" placeholder="Code" type="text" class="form-control" >
                      <?php echo form_error('code', '<div style="color:red">', '</div>');?>
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="title" class="col-4 col-form-label">Discount (in %)</label> 
                    <div class="col-8">
                      <input id="title" name="per_cent" placeholder="in %" type="text" class="form-control" >
                      <?php echo form_error('per_cent', '<div style="color:red">', '</div>');?>
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="title" class="col-4 col-form-label">Amount</label> 
                    <div class="col-8">
                      <input id="title" name="amount" placeholder="Amount" type="text" class="form-control" >
                      <?php echo form_error('amount', '<div style="color:red">', '</div>');?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="offset-4 col-8">
                      <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
        </div>
        </form>
    </div>
	</div>
</div>