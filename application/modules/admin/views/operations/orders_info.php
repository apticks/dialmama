<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Orders</h4>
		<form  novalidate="" action="<?php echo base_url('orders/tracking_info_update');?>" method="post" enctype="multipart/form-data">
			<div class="card-header" >
				<div class="row justify-content-center">
					<div class="col-6 admin-address">
                          <div class="container" style="border-bottom:1px solid black">
                            <h2><?php echo $order['address']['name'];?></h2>
                          </div>
                            <hr>
                          <ul class="container admin-address_details">
                            <li><p><span class="glyphicon glyphicon-earphone one" data-feather="smartphone" style="width:50px;">Mobile</span>+91 <?php echo $order['address']['phone'];?></p></li>
                            <li><p><span class="glyphicon glyphicon-envelope one" data-feather="mail" style="width:50px;">Email</span><?php echo $order['address']['email'];?></p></li>
                            <li><p><span class="glyphicon glyphicon-map-marker one" data-feather="home" style="width:50px;">Address</span><?php echo $order['address']['address'];?></p></li>
                            <li><p><span class="glyphicon glyphicon-new-window one" data-feather="map-pin" style="width:50px;">Pincode</span><?php echo $order['address']['pincode'];?></p>
                          </ul>
					</div>
				</div>
				<div class="form-row justify-content-center">
					<div class="form-group col-md-4">
						<label>Order tracking id</label>
						 <input type="text" name="track_id" required="" value="<?php echo $order['track_id'];?>" class="form-control" placeholder="Track id">
						 <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
					</div>
					<div class="form-group col-md-3">
						<label>Order status</label> 
						<select class="form-control" name="status">
							<option value="null" selected disabled>--select--</option>
							<option value="0" <?php echo ( $order['status'] == 0 )? 'selected': '';?>>Cancelled</option>
							<option value="1" <?php echo ( $order['status'] == 1 )? 'selected': '';?>>Placed</option>
							<option value="2" <?php echo ( $order['status'] == 2 )? 'selected': '';?>>Shipped</option>
							<option value="3" <?php echo ( $order['status'] == 3 )? 'selected': '';?>>Delivered</option>
						</select>
					</div>
					
					<div class="form-group col-md-1">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Orders</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Order Id</th>
									<th>Category</th>
									<th>Subcategory</th>
									<th>Product</th>
									<th>MFG Brand</th>
									<th>Brand</th>
									<th>Model No</th>
									<th>QTY</th>
									<th>Color</th>
									<th>Ram</th>
									<th>Storage</th>
									<th>Condition</th>
									<th>Compatibility</th>
									<th>Price</th>
									<th>Discount</th>
									<th>Final price</th>
									<th>Vendor</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($order_details)):?>
    							<?php  $sno = 1; foreach ($order_details as $ord): ?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $ord['order_item_code'];?></td>
    									<td><?php echo $ord['product']['category'];?></td>
    									<td><?php echo $ord['product']['sub_category'];?></td>
    									<td><?php echo $ord['product']['name']."( ".$ord['product']['custom_product_id']." )";?></td>
    									<td><?php echo $ord['product']['mfg_brand'];?></td>
    									<td><?php echo $ord['variant_values']['brand'];?></td>
    									<td><?php echo $ord['variant_values']['model'];?></td>
    									<td><?php echo $ord['qty'];?></td>
    									<td><?php echo $ord['variant_values']['color'];?></td>
    									<td><?php echo $ord['product']['ram'];?></td>
										<td><?php echo $ord['product']['storage'];?></td>
										<td><?php 
										if($ord['product']['condition'] == 1){
										    echo "Like new";
										}elseif ($ord['product']['condition'] == 2){
										    echo "Including Accessories";
										}elseif ($ord['product']['condition'] == 3){
										    echo "Only phone";
										}elseif ($ord['product']['condition'] == 4){
										    echo "Box Opened";
										}elseif ($ord['product']['condition'] == 5){
										    echo "A Grade";
										}elseif ($ord['product']['condition'] == 6){
										    echo "B Grade";
										}elseif ($ord['product']['condition'] == 7){
										    echo "Brand Warranty";
										}elseif ($ord['product']['condition'] == 8){
										    echo "Seller Warranty";
										}else {
										    echo "NA";
										}
										?></td>
    									<td><?php echo $ord['product']['compatibility'];?></td>
    									<td><?php echo $ord['price'];?></td>
    									<td><?php echo $ord['discount'];?></td>
										<td><?php echo $ord['total'];?></td>
    									<td><?php echo "Call: ".$ord['vendor']['phone']."( ".$ord['vendor']['unique_id']." )";?></td>
    									<td><b><?php 
										if($ord['status'] == 1){
											echo "Received";
										}elseif ($ord['status'] == 2){
											echo "Accepted";
										}elseif ($ord['status'] == 0){
											echo "Cancelled";
										}elseif ($ord['status'] == 3){
											echo "Arrived at hub";
										}else {
										    echo "NA";
										}
    									?></b>
    									</td>
    									<td>
    										<input type="checkbox" <?php echo ($ord['status'] == 3) ? 'checked' : '';?> order_item_id="<?php echo $ord['id']?>" amount="<?php echo $ord['price']?>"  vendor_id="<?php echo $ord['vendor']['id']?>"  order_no="<?php echo $order['order_no']?>" order_item_code="<?php echo $ord['order_item_code'];?>" class="change_order_item_status" data-on="Arrivied" data-off="Waiting" data-toggle="toggle" data-onstyle="success" data-offstyle="primary"> 
    									</td>
									</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='8'><h3>
											<center>No Orders</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
