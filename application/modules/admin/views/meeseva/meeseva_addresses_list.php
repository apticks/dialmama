<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>Meeseva Addresses</h4>
					<a class="btn btn-outline-primary float-right" href="<?php echo base_url('meeseva_addresses/c')?>">Add address</a>
				</div>
							<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Pincode</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Address</th>
									<th>Landmark</th>
									<th>State</th>
									<th>District</th>
									<th>City</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($meeseva_address)):?>
    							<?php  $sno = 1; foreach ($meeseva_address as $address): ?>
    								<tr>
									<td><?php echo $address['pincode'];?></td>
									<td><?php echo $address['owner_name'];?></td>
									<td><?php echo $address['email'].' & '.$address['phone'];?></td>
									<td><?php echo $address['address'];?></td>
									<td><?php echo $address['landmark'];?></td>
									<td><?php echo $address['state'];?></td>
									<td><?php echo $address['district'];?></td>
									<td><?php echo $address['city'];?></td>
									<td><a
										href="<?php echo base_url()?>meeseva_addresses/u?id=<?php echo $address['id']; ?>"
										class=" mr-2  " type="ecom_brands"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $address['id'] ?>, 'meeseva_addresses/d')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
								<tr>
									<th colspan='10'>
										<h3><center>No Pincodes</center></h3>
									</th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
					
				</div>
			</div>


		</div>