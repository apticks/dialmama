<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--Add User And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Meeseva address</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('meeseva_addresses/u'); ?>" method="post">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Name</label> <input type="text" name="name"
							class="form-control" required="" value="<?php echo $meeseva_address['owner_name']?>">
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>Email</label> <input type="text" name="email"
							class="form-control" required="" value="<?php echo $meeseva_address['email']?>">
						<?php echo form_error('email','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>Phone</label> <input type="text" name="phone"
							class="form-control" required="" value="<?php echo $meeseva_address['phone']?>">
						<?php echo form_error('phone','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>Pincode</label> <input type="text" name="pincode"
							class="form-control" required="" value="<?php echo $meeseva_address['pincode']?>">
						<?php echo form_error('pincode','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>Address</label> <input type="text" name="address"
							class="form-control" required="" value="<?php echo $meeseva_address['address']?>">
						<?php echo form_error('address','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>Landmark</label> <input type="text" name="landmark"
							class="form-control" required="" value="<?php echo $meeseva_address['landmark']?>">
						<?php echo form_error('landmark','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>State</label> <input type="text" name="state"
							class="form-control" required="" value="<?php echo $meeseva_address['state']?>">
						<?php echo form_error('state','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>District</label> <input type="text" name="district"
							class="form-control" required="" value="<?php echo $meeseva_address['district']?>">
						<?php echo form_error('district','<div style="color:red">','</div>')?>
					</div>
					
					<div class="form-group col-md-6">
						<label>City</label> <input type="text" name="city"
							class="form-control" required="" value="<?php echo $meeseva_address['city']?>">
						<?php echo form_error('city','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-12">
					<input type="hidden" name="id" class="form-control" required="" value="<?php echo $meeseva_address['id']?>">
						<button class="btn btn-primary mt-27 " id="btnSubmit">Submit</button>
					</div>
				</div>


			</div>
		</form>
	</div>
</div>
