<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
    		<div class="card-header">
    			<h4 class="ven"> Model Filter</h4>
        		 <form class="" novalidate="" action="<?php echo base_url('excel_model/r/0');?>" method="post" enctype="multipart/form-data">
        		 	<div class="row">
    					<div class="form-group col-md-3">
    						<label>Brand Type</label>
    						<!-- <input type="file" class="form-control" required="">-->
    						<select class="form-control" name="brand_type" id="brand_type" required="" value="<?php echo (! empty($brand_type))? $brand_type: 0;?>" onchange="brand_type_changed()">
    								<option value="0" <?php echo (empty($brand_type))? "selected" : ""?>selected disabled>--select--</option>
    								<option value="1" <?php echo (1 == $brand_type)? "selected" : ""?>>Normal brands</option>
    								<option value="2" <?php echo (2 == $brand_type)? "selected" : ""?>>Manufacturer brands</option>
    						</select>
    					</div>
    					
    					<div class="form-group col-3"> 
                         <label for="menu">Brands</label>
                             <select calss="form-control" name="brand_id" id="brand_id" <?php  echo (! empty($brand_id))? '': 'disabled';?> value="<?php echo $brand_id;?>" onchange="excel_brand_changed()" class="form-control">
                                <option value="0">All</option>
                                <?php foreach ($brands as $g):?>
                                <option value="<?php echo $g['id'];?>" <?php echo ($g['id'] == $brand_id)? "selected" : ""?>><?php echo $g['name'].' (ID: '.$g['id'].')';?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group col-2">
    						<label for="noofrows">rows</label>
    						<input type="text" id="noofrows" name="noofrows" placeholder="rows" value="<?php echo $noofrows;?>" class="form-control">
    					</div>
    					 <div class="form-group col-1">
    						<button type="submit" name="submit" id="upload" value="Apply" class="btn btn-primary mt-27 "><i class="fa fa-search">Search</i></button>
    					</div>
					</div>
				
        		</form>
        		<div class="row">
        			<div class="form-group col-2">
        				<form class="needs-validation h-100 justify-content-center align-items-center" novalidate="" action="<?php echo base_url('excel_model/export/0');?>" method="post" enctype="multipart/form-data">
        					<input type="hidden" name="brand_id" id="export_brand_id" placeholder="Brands" value="<?php echo $brand_id;?>" class="form-control">
        					<input type="hidden" name="brand_type" id="export_brand_type"  placeholder="Brands"  value="<?php echo $brand_type;?>" class="form-control">
        					<button type="submit" name="export" id="export_button" class="btn btn-success">Export models xlsx</button>
        				</form>
        			</div>
        			<div class="form-group col-2">
                		<form class="needs-validation h-100 justify-content-center align-items-center" novalidate="" action="<?php echo base_url('excel_model/r/0');?>" method="post" enctype="multipart/form-data">
            				<input type="hidden" name="q" placeholder="Search" value="" class="form-control">
            				<input type="hidden" name="brand_id" placeholder="Brands" value="0" class="form-control">
            				<input type="hidden" name="brand_type" placeholder="Brands" value="0" class="form-control">
            				<button type="submit" name="submit" id="upload" value="Apply" class="btn btn-danger">Clear</button>
            			</form>
        			</div>
        			
    			</div>
    			
			</div>
		</div>
</div>
<div class="row">
    <div class="card-body">
    	<div class="card">
    		<div class="card-header">
				<h4>Models bulk upload</h4>
			</div>
			<div class="card-body">
            	<form class="" novalidate="" action="<?php echo base_url('excel_model/bulk_upload/0');?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
            		<div class="row">
                		<div class="form-group col-3">
            				<label for="models_xlsx">Choose excel file</label>
            				<input type="file" id="models_xlsx" name="models_xlsx" placeholder="rows" class="form-control">
            			</div>
            			 <div class="form-group col-3">
            				<button type="submit" name="submit" id="upload_excel" value="Apply" class="btn btn-success  mt-27">Uplad excel</button>
            			</div>
        			</div>
    			</form>
			</div>
    	</div>
    </div>
</div>
<div class="row">
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Models</h4>
				</div>
			<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExportNoPagination"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Model Name</th>
									<th>Brand</th>
									<th>Brand Type</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($models)):?>
    							<?php  $sno = 1; foreach ($models as $model): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $model['name'];?></td>
								<!-- 	<td><?php foreach ($brands as $brand):?>
    										<?php //echo ($brand['id'] == $model['brand_id'])? $brand['name']:'';?>
    									<?php endforeach;?></td> -->
    									<td><?php echo (! empty($model['brand']['name']))? $model['brand']['name'] : 'NA' ;?></td>
    									<td><?php echo ($model['brand']['status'] == 1)? 'Normal Brand' : 'Manufacturer Brand' ;?></td>
                                   
									<td><a
										href="<?php echo base_url()?>model/edit/0?id=<?php echo $model['id']; ?>"
										class=" mr-2  " type="ecom_brands"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $model['id'] ?>, 'model/d/0')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>No Models Available!</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
						<!-- Paginate -->
    				<div class="row  justify-content-center">
    					<div class="col-12" style='margin-top: 10px;'>
                           <?= $pagination; ?>
                        </div>
    				</div>
					</div>
					
				</div>
			</div>


		</div>
</div>
