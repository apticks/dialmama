<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Model</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('model/c/0');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Model Name</label> <input type="text" name="name"
							required="" value="<?php echo set_value('name')?>"
							class="form-control" placeholder="Model Name">
						<div class="invalid-feedback">Please Provide Model Number.!</div>
						<?php echo form_error('name', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Brand</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="brand_id" required="" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($brands as $brand):?>
    								<option value="<?php echo $brand['id'];?>"><?php echo $brand['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">Please select Brand.!</div>
					</div>
					<div class="form-group col-md-4">
						<label>Manufacturer Brand</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="brand_id" required="" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($mf_brands as $brand):?>
    								<option value="<?php echo $brand['id'];?>"><?php echo $brand['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">Please select Brand.!</div>
					</div>
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>
		<br>
	<div class="col-12">
    		<div class="card-header">
    			<h4 class="ven"> Model Filter</h4>
        		 <form class="" novalidate="" action="<?php echo base_url('model/r/0');?>" method="post" enctype="multipart/form-data">
        		 	<div class="row">
        				<div class="form-group col-3">
        					<label for="q">Model Name</label>
    						<input type="text" name="q" id="q" placeholder="Name" value="<?php echo $q;?>" class="form-control">
    					</div>
    					
    					   <div class="form-group col-3"> 
                         <label for="menu">Brands</label>
                             <select calss="form-control" name="brand_id" id="brand" value="<?php echo $brand_id;?>" class="form-control">
                                <option value="0">All</option>
                                <?php foreach ($brands as $g):?>
                                <option value="<?php echo $g['id'];?>" <?php echo ($g['id'] == $brand_id)? "selected" : ""?>><?php echo $g['name'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group col-3">
    						<label for="noofrows">rows</label>
    						<input type="text" id="noofrows" name="noofrows" placeholder="rows" value="<?php echo $noofrows;?>" class="form-control">
    					</div>
    					 <div class="form-group col-3">
    						<button type="submit" name="submit" id="upload" value="Apply" class="btn btn-primary mt-27 "><i class="fa fa-search">Search</i></button>
    					</div>
					</div>
				
        		</form>
        		<form class="needs-validation h-100 justify-content-center align-items-center" novalidate="" action="<?php //echo base_url('vendors_filter/0');?>" method="post" enctype="multipart/form-data">
    				<input type="hidden" name="q" placeholder="Search" value="" class="form-control">
    				<input type="hidden" name="brand_id" placeholder="Brands" value="0" class="form-control">
    				<button type="submit" name="submit" id="upload" value="Apply" class="btn btn-danger mt-3">Clear</button>
    			</form>
			</div>
		</div>
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Models</h4>
				</div>
							<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExportNoPagination"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Model Name</th>
									<th>Brand</th>
									<th>Brand Type</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($models)):?>
    							<?php  $sno = 1; foreach ($models as $model): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $model['name'];?></td>
								<!-- 	<td><?php foreach ($brands as $brand):?>
    										<?php //echo ($brand['id'] == $model['brand_id'])? $brand['name']:'';?>
    									<?php endforeach;?></td> -->
    									<td><?php echo (! empty($model['brand']['name']))? $model['brand']['name'] : 'NA' ;?></td>
    									<td><?php echo ($model['brand']['status'] == 1)? 'Normal Brand' : 'Manufacturer Brand' ;?></td>
                                   
									<td><a
										href="<?php echo base_url()?>model/edit/0?id=<?php echo $model['id']; ?>"
										class=" mr-2  " type="ecom_brands"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $model['id'] ?>, 'model/d/0')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='4'><h3>
											<center>No Models</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
						<!-- Paginate -->
    				<div class="row  justify-content-center">
    					<div class="col-12" style='margin-top: 10px;'>
                           <?= $pagination; ?>
                        </div>
    				</div>
					</div>
					
				</div>
			</div>


		</div>

	</div>
</div>
