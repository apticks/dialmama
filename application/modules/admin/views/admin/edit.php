
 <!--Edit Daily Members -->
    <div class="row">
    <div class="col-12">
        <h4>Edit Daily Members</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('members_update');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">

                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label>Name of Member</label>
                        <input type="text" name="name" class="form-control" required="" value="<?php echo $members['name'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $members['id'] ; ?>">
					 <div class="form-group col-md-4">
                        <label>Email of Member</label>
                        <input type="text" name="email" class="form-control" required="" value="<?php echo $members['email'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Mobile number of Member</label>
                        <input type="text" name="mobile" class="form-control" required="" value="<?php echo $members['mobile'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                     <div class="form-group col-md-4">
                        <label>Aadhar number of Member</label>
                        <input type="text" name="aadhar_no" class="form-control" required="" value="<?php echo $members['aadhar_no'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                      <div class="form-group col-md-4">
                        <label>Pan number of Member</label>
                        <input type="text" name="pan" class="form-control" required="" value="<?php echo $members['pan'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                      <div class="form-group col-md-4">
                        <label>Date of Birth</label>
                        <input type="text" name="member_dob" class="form-control" required="" value="<?php echo $members['member_dob'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
					 <div class="form-group col-md-4">
                        <label>Nominee Name</label>
                        <input type="text" name="nominee_name" class="form-control" required="" value="<?php echo $members['nominee_name'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                     <div class="form-group col-md-4">
                        <label>Nominee Aadhar</label>
                        <input type="text" name="nominee_aadhar" class="form-control" required="" value="<?php echo $members['nominee_aadhar'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                     <div class="form-group col-md-4">
                        <label>Nominee DOB</label>
                        <input type="text" name="nominee_dob" class="form-control" required="" value="<?php echo $members['nominee_dob'];?>">
                        <div class="invalid-feedback">Enter Valid Name?</div>
                    </div>
                   <div class="form-group col-md-4">
                                                <label>State</label>
                                               
                                                <select class="form-control" id='state' onchange="state_changed()" name="state_id" required="">
                                                    <option value="0" selected disabled>--select--</option>

                                                    <?php foreach ($states as $state):?>
                                                        <option value="<?php echo $state['id'];?>" <?php echo ($state['id'] == $members['state_id'])? 'selected': '';?>><?php echo $state['name']?></option>
                                                        <?php echo $state['name']?>
                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <div class="invalid-feedback">Select valid state?</div>
                                            </div>
                                             <div class="form-group col-md-5">
                                                <label>District</label>
                                                <select id="district" class="form-control" name="district_id" required="">
                                                    <option value="0" selected disabled>--select--</option>
                                                    <?php foreach ($districts as $district): ?>
                                                        <?php if ($district['state_id'] == $members['state_id']):?>
                                                           <option value="<?php echo $district['id'];?>" <?php echo ($district['id'] == $members['district_id'])? 'selected': '';?>><?php echo $district['name']?></option>
														<?php echo $district['name']?>
                                                            </option>
                                                        <?php endif;?>
                                                            <?php endforeach;?>
                                                </select>
                                                <div class="invalid-feedback">Belongs to the District?</div>
                                            </div>
                                            
                    <div class="form-group col-md-12">
                        <!--                             <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-primary mt-27 ">Update</button> -->
                        <button class="btn btn-primary mt-27 ">Update</button>

                    </div>
                </div>
            </div>

        </form>
    </div>
</div>


