<?php

class Camera_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'camera';
        $this->primary_key = 'id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    
    
    private function _config() {
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
    }
    
    
    private function _relations(){
    	
    }
    
    private function _form(){
    	$this->rules = array(
    			array(
    					'field' => 'name',
    					'label' => 'Name',
    					'rules' => 'trim|required',
    					'errors' => array(
    							'required' => 'You must provide a %s.'
    					)
    			),
    			array(
    					'field' => 'color_code',
    					'label' => 'Colour Code',
    					'rules' => 'trim|required',
    					'errors' => array(
    							'required' => 'You must provide a %s.'
    					)
    			),
    	);
    }
}

