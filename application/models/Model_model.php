<?php

class Model_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'models';
        $this->primary_key = 'id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    
    
    private function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    
    private function _relations(){
       // $this->has_one = array('Brand_model', 'id', 'brand_id');
        $this->has_many['brands'] = array(
            'foreign_model' => 'Brand_model',
            'foreign_table' => 'brands',
            'local_key' => 'brand_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
    }
    
    private function _form(){
    	$this->rules = array(
    			array(
    					'field' => 'name',
    					'label' => 'Name',
    					'rules' => 'trim|required',
    					'errors' => array(
    							'required' => 'You must provide a %s.'
    					)
    			),
    			array(
    					'field' => 'brnad_id',
    					'lable' => 'Brand Id',
    					'rules' => 'trim|requiered',
    					'errors' => array(
    							'required' => 'You must provide a %s.'
    					)
    			)
    	);
    }
    public function get_models($limit = NULL, $offset = NULL,$search_text = NULL, $brand_id = NULL){
         $this->_query_users( $search_text,$brand_id );
        $this->db->order_by('`models`.name', 'ASC');
        $this->db->limit($limit, $offset);
        $rs     = $this->db->get($this->table);
        //print_array($this->db->last_query());
        return   $rs->result_array();
    }
   public function users_count( $search_text = NULL, $brand_id = NULL){
        $this->_query_users($search_text,  $brand_id);
        return $this->db->count_all_results($this->table);
    }
    private function _query_users($search_text = NULL,$brand_id = NULL){
         $primary_key = '`' . $this->primary_key . '`';
        $table       = '`' . $this->table . '`';
        
        $str_select_vendor = '';
        foreach (array('created_at', 'updated_at', 'deleted_at', 'id', 'name', 'brand_id','status') as $v)
        {
            $str_select_vendor .= "$table.`$v`,";
        }
        
        $this->db->select($str_select_vendor);
        
        if ( ! empty($search_text))
        {
            $this->db->or_like($table . '.`name`', $search_text);
           
        }
         if (! empty($brand_id)) {
            $this->db->where("$table.brand_id=", $brand_id);
        }
        $this->db->where("$table.deleted_at", NULL);
        return $this;
    }
    
}

