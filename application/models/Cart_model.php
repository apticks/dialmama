<?php

class Cart_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="cart";
            $this->primary_key="id";
            $this->config();
            $this->forms();
            $this->relations();
    }
   
    public function config(){
        $this->timestamps = false;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       $this->has_one['product'] = array('product_model', 'id', 'product_id');
       $this->has_many['cart_variants'] = array(
       		'foreign_model' => 'Cart_variant_model',
       		'foreign_table' => 'cart_variants',
       		'foreign_key' => 'cart_id',
       		'local_key' => 'id',
       		'get_relate' => FALSE
       );
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'product_id',
                'label' => 'Product Id',
                'rules' => 'required',
            ),
         );
    }
}

