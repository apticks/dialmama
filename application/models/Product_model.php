<?php
class Product_model extends MY_Model {
	public $rules;
	public function __construct() {
		parent::__construct ();
		$this->table = 'products';
		$this->primary_key = 'id';
		$this->foreign_key = 'product_id';
		
		$this->before_create[] = '_add_created_by';
		$this->before_update[] = '_add_updated_by';
		
		$this->_config ();
		$this->_form ();
		$this->_relations ();
	}
	private function _config() {
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		$this->delete_cache_on_save = TRUE;
	}
	
	protected function _add_created_by($data)
	{
	    $data['created_user_id'] = $this->ion_auth->get_user_id()? $this->ion_auth->get_user_id(): $this->user_id; //add user_id
	    return $data;
	}
	
	protected function _add_updated_by($data)
	{
	    $data['updated_user_id'] = $this->ion_auth->get_user_id()? $this->ion_auth->get_user_id(): $this->user_id; //add user_id
	    return $data;
	}
	
	private function _relations() {
		$this->has_one['compatible_for'] = array('Compatible_for_model', 'id', 'compatible_for');
		$this->has_one['front_camera'] = array('Camera_model', 'id', 'front_cam');
		$this->has_one['back_camera'] = array('Camera_model', 'id', 'back_cam');
		
		$this->has_one['user'] = array('User_model', 'id', 'user_id');
		$this->has_one['category'] = array('Category_model', 'id', 'cat_id');
		$this->has_one['sub_category'] = array('Sub_category_model', 'id', 'sub_cat_id');
		$this->has_one['brand'] = array('Brand_model', 'id', 'brand_id');
		$this->has_one['model'] = array('Model_model', 'id', 'model');
		$this->has_one['warrenty'] = array('Warrenty_model', 'id', 'warrenty');
		$this->has_one['ram'] = array('Ram_model', 'id', 'ram');
		$this->has_one['storage'] = array('Storage_model', 'id', 'storage');
		/* $this->has_one['battery_capacity'] = array('Battery_capacity_model', 'id', 'battery_capacity');
		$this->has_one['screen_size'] = array('Size_model', 'id', 'screen_size'); */
		$this->has_many['variants'] = array(
				'foreign_model' => 'Product_variant_model',
				'foreign_table' => 'product_variants',
				'local_key' => 'id',
				'foreign_key' => 'product_id',
				'get_relate' => FALSE
		);
		
		$this->has_many['images'] = array(
		    'foreign_model' => 'Product_image_model',
		    'foreign_table' => 'product_images',
		    'local_key' => 'id',
		    'foreign_key' => 'product_id',
		    'get_relate' => FALSE
		);
		
		$this->has_many['variant_values'] = array(
				'foreign_model' => 'Product_variant_value_model',
				'foreign_table' => 'product_variant_values',
				'local_key' => 'id',
				'foreign_key' => 'product_id',
				'get_relate' => FALSE
		);
	}
	private function _form() {
		$this->rules = array (
				 array (
						'lable' => 'Product Name',
						'field' => 'name',
						'rules' => 'required',
				), 
		);
		
		
	}
	
	
	public function all($cat_id = NULL, $sub_cat_id = NULL, $brand_id = NULL, $search = NULL, $user_id = NULL)
	{
		$this->_query_all($cat_id, $sub_cat_id, $brand_id, $search, $user_id);
        $this->db->group_by('products.id');
        $this->db->order_by('products.id', 'DESC');
        $rs     = $this->db->get($this->table);
        $result = $rs->result_array();
	    
	    $this->db->reset_query();
	   
	    $this->_query_all($cat_id, $sub_cat_id, $brand_id, $search, $user_id);
	    $count = $this->db->count_all_results($this->table);
	    
	    return  array(
	        'result' => $result,
	        'count'  => $count
	    );
	}
	
	private function _query_all($cat_id = NULL, $sub_cat_id = NULL, $brand_id = NULL, $search = NULL, $user_id = NULL)
	{
	    
	    $this->load->model(array('sub_category_model', 'user_model', 'category_model', 'brand_model', 'product_image_model'));
	    
	    $ecom_sub_category_table       = '`' . $this->sub_category_model->table . '`';
	    $ecom_sub_category_primary_key = '`' . $this->sub_category_model->primary_key . '`';
	    $ecom_sub_category_foreign_key = '`' . $this->sub_category_model->foreign_key . '`';
	    
	    $brand_table       = '`' . $this->brand_model->table . '`';
	    $brand_primary_key = '`' . $this->brand_model->primary_key . '`';
	    $brand_foreign_key = '`' . $this->brand_model->foreign_key . '`';
	    
	    $category_table       = '`' . $this->category_model->table . '`';
	    $category_primary_key = '`' . $this->category_model->primary_key . '`';
	    $category_foreign_key = '`' . $this->category_model->foreign_key . '`';
	    
	    $user_table       = '`' . $this->user_model->table . '`';
	    $user_primary_key = '`' . $this->user_model->primary_key . '`';
	    $user_foreign_key = '`' . $this->user_model->foreign_key . '`';
	    
	    $product_image_table       = '`' . $this->product_image_model->table . '`';
	    $product_image_primary_key = '`' . $this->product_image_model->primary_key . '`';
	    
	    $primary_key = '`' . $this->primary_key . '`';
	    $foreign_key = '`' . $this->foreign_key . '`';
	    $table       = '`' . $this->table . '`';
	    
	    $str_select_product = '';
	    foreach (array( 'id', 'user_id', 'name', 'product_code', 'custom_product_id', 'cat_id', 'sub_cat_id', 'brand_id', 'status') as $v)
	    {
	        $str_select_product .= "$table.`$v`,";
	    }
	    
	    $this->db->select($str_select_product."`product_images`.`id` as image_id, `product_images`.`ext`");
	    $this->db->join($ecom_sub_category_table, "$ecom_sub_category_table.$primary_key=$table.$ecom_sub_category_foreign_key", 'left');
	    $this->db->join($category_table, "$category_table.$primary_key=$table.$category_foreign_key", 'left');
	    $this->db->join($brand_table, "$brand_table.$brand_primary_key=$table.$brand_foreign_key", 'left');
	    $this->db->join($user_table, "$user_table.$user_primary_key=$table.$user_foreign_key", 'left');
	    $this->db->join($product_image_table, "$product_image_table.$foreign_key=$table.$primary_key", 'left');
	    
	    if ($cat_id)
	    {
	        $this->db->where("$table.$category_foreign_key", $cat_id);
	    }
	    
	    if ($brand_id)
	    {
	        $this->db->where("$table.$brand_foreign_key", $brand_id);
	    }
	    
	    if ($user_id)
	    {
	    	$this->db->where("$table.$user_foreign_key", $user_id);
	    }
	    
	    if ($sub_cat_id)
	    {
	        $this->db->where("$table.$ecom_sub_category_foreign_key", $sub_cat_id);
	    }
	    
	    if (! is_null($search)) {foreach (explode(' ', $search) as $s){
	        $this->db->or_like($table . '.`sounds_like`', metaphone($s));
	    }}
	    $this->db->where("$table.status", 1);
	    $this->db->where("$table.deleted_at", null);
	    $this->db->where("$user_table.status", 1);
	    return $this;
	}
	
}

class Products_list_row
{
    public $id;
    public $name;
    public $product_code;
    public $custom_product_id;
    public $cat_id;
    public $sub_cat_id;
    public $brand_id;
}
