<?php

class Cart_variant_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="cart_variants";
            $this->primary_key="id";
            $this->config();
            $this->forms();
            $this->relations();
    }
   
    public function config(){
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       $this->has_one['product'] = array('product_model', 'id', 'product_id');
       $this->has_one['product_variant'] = array('Product_variant_model', 'id', 'variant_id');
       $this->has_one['product_variant_values'] = array('Product_variant_value_model', 'variant_id', 'variant_id');
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'product_id',
                'label' => 'Product Id',
                'rules' => 'required',
            ),
         );
    }
}

