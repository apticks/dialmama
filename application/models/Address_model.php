<?php

class Address_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'address';
        $this->primary_key = 'id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    
    private function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
    }
    
    private function _relations(){
        $this->has_one = array('User_model', 'id', 'user_id');
        $this->has_one = array('Country_model', 'id', 'country');
        $this->has_one = array('State_model', 'id', 'state');
        $this->has_one = array('District_model', 'id', 'distrcit');
    }
    
    private function _form(){
        
    }
}

