<?php

class Order_details_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'order_details';
        $this->primary_key = 'id';
        $this->foreign_key = 'order_details_id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    public function _config() {
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
    	$this->has_one['product'] = array('Product_model', 'id', 'product_id');
    	$this->has_one['vendor'] = array('User_model', 'id', 'vendor');
    	$this->has_one['variant'] = array('Product_variant_model', 'id', 'variant_id');
    }
    
   
    
    public function _form(){
        $this->rules = array(
            array(
                'field' => 'name',
                'lable' => 'Name',
                'rules' => 'trim|required|min_length[5]',
                'errors' => array(
                    'required' => 'You must provide a %s.',
                    'min_length' => 'you need to give minimum 5 characters'
                )
            ),
            array(
                'field' => 'desc',
                'lable' => 'Description',
                'rules' => 'trim|required|max_length[200]',
                'errors' => array(
                    'required' => 'You must provide a %s.'
                )
            )
        );
    }
}

