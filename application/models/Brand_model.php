<?php

class Brand_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="brands";
            $this->primary_key="id";
            $this->foreign_key="brand_id";
            
            $this->config();
            $this->forms();
            $this->relations();
    }
    
    public function config(){
        $this->timestamps = true;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
    	$this->has_many['models'] = array(
    			'foreign_model' => 'Model_model',
    			'foreign_table' => 'models',
    			'local_key' => 'id',
    			'foreign_key' => 'brand_id',
    			'get_relate' => FALSE
    	);
    	$this->has_many_pivot['categories'] = array(
    	    'foreign_model' => 'category_brand_model',
    	    'foreign_table' => 'categories',
    	    'pivot_table' => 'categories_brands',
    	    'local_key' => 'id',
    	    'pivot_local_key' => 'brand_id',
    	    'foreign_key' => 'id',
    	    'pivot_foreign_key' => 'cat_id',
    	    'get_relate' => FALSE
    	); 
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            
        );
    }
}

