<?php
class Product_image_model extends MY_Model {
	public $rules;
	public function __construct() {
		parent::__construct ();
		$this->table = 'product_images';
		$this->primary_key = 'id';

		$this->_config ();
		$this->_form ();
		$this->_relations ();
	}
	private function _config() {
		$this->timestamps = FALSE;
		$this->soft_deletes = FALSE;
	}
	private function _relations() {
		$this->has_one['product'] = array('Product_model', 'id', 'product_id');
	}
	private function _form() {
		
	}
}

