<?php

class Member_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'members';
        $this->primary_key = 'id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
        
    }
    
   public function _form(){
        $this->rules = array(
            array(
                'field' => 'ref_id',
                'lable' => 'Referal Id',
                'rules' => 'min_length[8]|max_length[10]|callback_check_referance',
                'errors' => array(
                    'min_length' => 'you need to give minimum 8 characters',
                    'check_referance' => 'Referal id is not valid'
                )
                ),
            array(
                'field' => 'email',
                'lable' => 'Email',
                'rules' => 'trim|required|valid_email|callback_check_email'
            ),
            );
        
    }
}

