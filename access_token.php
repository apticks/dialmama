<?php

class Instamojo
{
    private $client_id;
    private $client_secret;
    private $url = "https://api.instamojo.com/oauth2/token/";
    private $env = "production";

    public function __construct($client_id, $client_secret)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
    }

    public function getToken() {
        if (substr( $this->client_id, 0, 5 ) === "test_") {
            $this->url = "https://test.instamojo.com/oauth2/token/";
            $this->env = "test";
        }
        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, rawurldecode(http_build_query(array(
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'grant_type' => 'client_credentials'
        ))));
        $json = json_decode(curl_exec($curl));
        if(curl_error($curl))
        {
            echo 'error:' . curl_error($curl);
        }
        if (isset($json->error)) {
            return "Error: " . $json->error;
            throw new \Exception("Error: " . $json->error);
        }
        $this->token = $json;
        return $this->env . $json->access_token;
    }
}

//$instamojo = new Instamojo("akw7KwsD38YYKwKqtq8ToSeBNMfVanS1Csrgr8N6", "aAHLXUbk2MNdV9xl3Ux0IUXWo5HjExxU98N5XJHIFNh60ZkKgqqFYKxffWflyYSmmhyZrEQqVUp05IeIGuPpuJe2xnTGMVVsYLiCS08awNROIJcyOCe1Qb5fUL7CzObf");
$instamojo = new Instamojo("CJ4odY7y60xf5r3XYJIFVTGh14kHmremJAbTsVLD", "A02G1KfO1MmXjboF8jDGvfUIo3EVJU8RpTDZ6tPWtqmbVNafYGc5Q1tWf6jDXazkiKkCuExVOxvgCSixGXT39lUt1jM91jKMpeWwB3NbCAcnMXo8VpXnDlIWpjV6i4bv");

echo $instamojo->getToken();
?>